package au.com.tech

import groovy.transform.CompileStatic

@CompileStatic
class RingBuffer<T> implements Iterable<T> {
    List<T> a = new ArrayList<T>()

    RingBuffer() {
    }

    RingBuffer(Collection<T> c) {
        a.addAll(c)
    }

    void add(T o) {
        a.add(o)
    }

    @Override
    RingBufferIterator<T> iterator() {
        return new RingBufferIterator<T>(a)
    }

    /**
     * Return iterator starting after o
     * @param o
     * @return
     */
    RingBufferIterator<T> iterator(T o) {
        RingBufferIterator<T> r = new RingBufferIterator<T>(a)
        r.fastForward(o)
        return r
    }
}

@CompileStatic
class RingBufferIterator<T> implements Iterator<T> {
    List<T> a
    Iterator<T> i

    RingBufferIterator(List<T> v) {
        this.a = v
        this.i = v.iterator()
    }

    boolean fastForward(T o) {
        if (a.contains(o)) {
            while (i.hasNext()) {
                T n = i.next()
                if (n == o) {
                    break
                }
            }
            return true
        } else {
            return false
        }
    }

    @Override
    boolean hasNext() {
        return a.size() > 0
    }

    @Override
    T next() {
        if (i.hasNext()) {
            return i.next()
        } else {
            i = a.iterator()
            return i.next()
        }
    }
}
