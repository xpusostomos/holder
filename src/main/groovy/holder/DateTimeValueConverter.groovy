package holder

import grails.databinding.converters.ValueConverter
import org.springframework.core.annotation.Order
import org.springframework.core.Ordered

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

@Order(value=Ordered.HIGHEST_PRECEDENCE)
class DateTimeValueConverter implements ValueConverter  {
    static String stdFormat = DateValueConverter.stdFormat + " HH:mm"
    static DateTimeFormatter stdFormatter = DateTimeFormatter.ofPattern(stdFormat)
    static DateValueConverter dateConverter = new DateValueConverter()

    @Override
    boolean canConvert(value) {
        try {
            if (value instanceof String) {
                LocalDateTime.parse(value, stdFormatter)
                return true
            }
        } catch (DateTimeParseException x) {
        }
        return dateConverter.canConvert(value)
    }

    @Override
    def convert(value) {
        try {
            LocalDateTime.parse(value, stdFormatter)
        } catch (DateTimeParseException x) {
            dateConverter.convert(value).atTime(12, 0, 0)
        }
    }

    @Override
    Class<?> getTargetType() {
        LocalDateTime
    }

    static String format(LocalDateTime dt) {
        dt?.format(stdFormatter)
    }
}
