package holder

import grails.databinding.BindUsing
import grails.gorm.validation.Constrained
import grails.validation.Validateable

class EmailCommand implements Validateable {
    boolean alsoSendToBillingEmail = false
    boolean useBCC = true
    String to
    String subject
    String body
//    Set<AttachmentCommand> attachments
    @BindUsing({ obj, source ->
        return Album.paramsToAlbum(source['attachments'])
    })
    Album attachments
    List<PersonCandidate> candidates = new ArrayList<>()

    static constraints = {
//        alsoSendToBillingEmail
        to nullable: true
        subject maxSize: 78
        body widget: 'textarea'
        attachments nullable: true
    }

    static Map<String, Constrained> getConstrainedProperties() {
        return EmailCommand.constraintsMap
    }
}

class PersonCandidate {
    Person person
    boolean checked
}
