package holder

import grails.compiler.GrailsCompileStatic
import org.springframework.security.authentication.InternalAuthenticationServiceException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.web.authentication.session.NullAuthenticatedSessionStrategy
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@GrailsCompileStatic
class TokenAuthenticationFilter extends AbstractAuthenticationProcessingFilter  {
    String DEFAULT_TOKEN = '_tk'
    String DEFAULT_USERNAME = 'username'

    SessionAuthenticationStrategy sessionStrategy = new NullAuthenticatedSessionStrategy();
    TokenAuthenticationFilter() {
        this("/**")
    }

    TokenAuthenticationFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl)
    }

    @Override
    Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter(DEFAULT_USERNAME)
        String token = request.getParameter(DEFAULT_TOKEN)
        if (token) {
            TokenAuthenticationToken tat = new TokenAuthenticationToken(username, token)
            return this.getAuthenticationManager().authenticate(tat)
        } else {
            return null
        }
    }

    @Override
    void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        // You could delete this if you want it to apply to all URLs
        if (!requiresAuthentication(request, response)) {
            chain.doFilter(request, response);

            return;
        }

        Authentication authResult;
        try {
            authResult = attemptAuthentication(request, response);
            if (authResult == null) {
                // Unlike the super class, we consider a null response an opportunity for the user
                // to use the usual spring login screen, so we call change.doFilter before returning
                chain.doFilter(request, response);
                return;
            }
            sessionStrategy.onAuthentication(authResult, request, response);
        }
        catch (InternalAuthenticationServiceException failed) {
            logger.error(
                    "An internal error occurred while trying to authenticate the user.",
                    failed);
            unsuccessfulAuthentication(request, response, failed);
            return;
        }
        catch (AuthenticationException failed) {
            // Authentication failed, wrong credentials
            // Here the user gets 401, but you could instead redirect
            // to the login screen if you called chain.doFilter
            unsuccessfulAuthentication(request, response, failed);
            return;
        }

        // We need to successfully authenticate before calling the chain
        successfulAuthentication(request, response, chain, authResult);
        // Here by calling the chain, we ensure that we continue onto the destination URL
        chain.doFilter(request, response);
    }
}
