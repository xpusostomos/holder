package holder
import grails.validation.Validateable

class LoginCommand implements Validateable {
    String email
    String password
    Person user
    boolean get = false
    String message

    static constraints = {
        email validator: { val, obj ->
            if (obj.findUser(val) == null) {
                ['holder.LoginCommand.email.noMatch']
            }
        }
        password validator: { val, obj ->
            if (obj.findUser(obj.email)) {
                String error = obj.findUser(obj.email).checkCredentials(val)
                if (error) {
                    [ error ]
                }
            }
        }
        message nullable: true
    }

    /**
     * Cache the user object
     * @param email
     * @return
     */
    Person findUser(String email) {
        if (this.user == null || this.user.email != email) {
            this.user = Person.findByEmail(email)
        }
        this.user
    }

    String vClass(def bean, String field) {
        bean.hasErrors() && !bean.get ? (bean.errors.getFieldError(field) ? 'is-invalid' : 'is-valid') : ''
    }
}
