package holder

import grails.validation.Validateable

import java.time.LocalDate

class DepreciationCommand implements Validateable {
    LocalDate from
    LocalDate to
}
