package holder

import grails.databinding.converters.ValueConverter
import org.apache.commons.lang3.time.DurationFormatUtils
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order

import java.time.Duration
import java.time.format.DateTimeParseException

@Order(value=Ordered.HIGHEST_PRECEDENCE)
class DurationValueConverter implements ValueConverter  {
    @Override
    boolean canConvert(value) {
        try {
            if (value instanceof String) {
//                Integer.parseInt(value)
                Duration.parse(value)
                return true
            }
        } catch (DateTimeParseException x) {
//            x.printStackTrace()
        }
        return false
    }

    @Override
    def convert(value) {
//        new Duration(Integer.parseInt(value) * 24 * 60 * 60, 0)
        Duration.parse(value)
    }

    @Override
    Class<?> getTargetType() {
        Duration
    }

    static String format(Duration dt) {
//        dt.toDays().toString()
//        DurationFormatUtils.formatDuration(dt, DurationFormatUtils.ISO_EXTENDED_FORMAT_PATTERN)
        StringBuilder sb = new StringBuilder('P')
        if (dt.toDaysPart() != 0) {
            sb.append(dt.toDaysPart())
            sb.append('D')
        }
        boolean doneT = false
        if (dt.toHoursPart() != 0) {
            sb.append('T')
            doneT = true
            sb.append(dt.toHoursPart())
            sb.append('H')
        }
        if (dt.toMinutesPart()) {
            if (!doneT) {
                sb.append('T')
                doneT = true
            }
            sb.append(dt.toMinutesPart())
            sb.append('M')
        }
        if (dt.toSecondsPart() != 0 || dt.toMillisPart() != 0) {
            if (!doneT) {
                sb.append('T')
            }
            sb.append(dt.toSecondsPart())
            if (dt.toMillisPart() != 0) {
                sb.append('.')
                sb.append(dt.toMillisPart())
            }
            sb.append('S')
        }
        sb.toString()
    }
}
