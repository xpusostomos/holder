package holder

import grails.compiler.GrailsCompileStatic
import grails.gorm.transactions.Transactional
import grails.plugin.springsecurity.userdetails.GormUserDetailsService
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException

//@GrailsCompileStatic
class TokenAuthenticationProvider implements AuthenticationProvider {
    GormUserDetailsService userDetailsService

    @Transactional
    @Override
    Authentication authenticate(Authentication authentication) throws AuthenticationException {
        TokenAuthenticationToken authenticationToken = (TokenAuthenticationToken) authentication

        Token.withTransaction {
            Token token = Token.findByToken(authenticationToken.credentials)

            if (token) {
                TokenAuthenticationToken newToken = token.checkAuthentication((String)authenticationToken.principal, (String)authenticationToken.credentials)
                return newToken
            } else {
                return null
//                throw new PreAuthenticatedCredentialsNotFoundException('cannot find token')
            }
        }
    }

    @Override
    boolean supports(Class<?> authentication) {
        return authentication.equals(TokenAuthenticationToken.class)
    }
}
