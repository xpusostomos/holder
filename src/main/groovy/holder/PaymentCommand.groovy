package holder

import grails.validation.Validateable

import java.time.LocalDate

class PaymentCommand implements Validateable {
    LocalDate date = LocalDate.now()
    BigDecimal amount
    String description
    String notes
    Person person
    AccountEntryMethodEnum method

    static constraints = {
        notes nullable: true, widget: 'textarea'
    }
}
