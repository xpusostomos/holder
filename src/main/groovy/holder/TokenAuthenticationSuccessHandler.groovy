package holder

import grails.compiler.GrailsCompileStatic
import org.springframework.security.web.authentication.AuthenticationSuccessHandler
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;

@GrailsCompileStatic
class TokenAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    void onAuthenticationSuccess(HttpServletRequest request,
                                 HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {
        // NO-OP
    }
}
