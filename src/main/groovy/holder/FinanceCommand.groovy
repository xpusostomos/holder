package holder
import grails.validation.Validateable
import java.time.LocalDate

class FinanceCommand implements Validateable {
    LocalDate fromDate
    LocalDate toDate
    boolean byYear = false
    boolean calendarYear = false
    Room premises

    static constraints = {
        premises nullable: true
    }
}
