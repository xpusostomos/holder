package holder

import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority

class TokenAuthenticationToken extends AbstractAuthenticationToken {
    Token token
    Object principal
    String credentials

    TokenAuthenticationToken(Token token) {
        super(token.user.authorities.collect { new SimpleGrantedAuthority(it.authority)})
        this.token = token
        this.principal = token.user
        super.setAuthenticated(true)
    }

    TokenAuthenticationToken(String principal, String token) {
        super([])
        this.principal = principal
        this.credentials =  token
    }

//    @Override
//    Collection<? extends GrantedAuthority> getAuthorities() {
//        return token.user.authorities.collect { new SimpleGrantedAuthority(it.authority)}
//    }

    @Override
    String getCredentials() {
        return this.credentials
    }
//
//    @Override
//    Object getDetails() {
//        return token.urlPattern
//    }

    @Override
    Object getPrincipal() {
        return this.principal
    }

    @Override
    void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException(
                    "Cannot set this token to trusted - use constructor which takes a GrantedAuthority list instead");
        }

        super.setAuthenticated(false);
    }

    //TODO look at super class and rewrite
    @Override
    String getName() {
        return token.user.name
    }
}
