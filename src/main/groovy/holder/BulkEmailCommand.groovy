package holder

import grails.databinding.BindUsing
import grails.validation.Validateable

class BulkEmailCommand implements Validateable {
    String templateName
    Room room
    String tag
    String toAddress
    String ccAddress
    String bccAddress
    boolean alsoSendToBillingEmail = false
    boolean useBCC = true
    String to
    String subject
    String body
//    Set<AttachmentCommand> attachments
    @BindUsing({ obj, source ->
        return Album.paramsToAlbum(source['attachments'])
    })
    Album attachments

    static constraints = {
//        alsoSendToBillingEmail
        templateName nullable: true
        room nullable: true
        tag nullable: true
        toAddress nullable: true
        ccAddress nullable: true
        bccAddress nullable: true
        subject maxSize: 78
        body nullable: true, widget: 'textarea'
        attachments nullable: true
    }

}
