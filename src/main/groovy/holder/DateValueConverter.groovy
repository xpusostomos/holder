package holder

import grails.databinding.converters.ValueConverter
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

@Order(value=Ordered.HIGHEST_PRECEDENCE)
class DateValueConverter implements ValueConverter  {
    static String stdFormat = "dd-LLL-yyyy"
    static DateTimeFormatter stdFormatter = DateTimeFormatter.ofPattern(stdFormat)

    @Override
    boolean canConvert(value) {
        try {
            if (value instanceof String) {
                LocalDate.parse(value, stdFormatter)
                return true
            }
        } catch (DateTimeParseException x) {
        }
        return false
    }

    @Override
    def convert(value) {
        LocalDate.parse(value, stdFormatter)
    }

    @Override
    Class<?> getTargetType() {
        LocalDate
    }

    static String format(LocalDate dt) {
        dt?.format(stdFormatter)
    }
}
