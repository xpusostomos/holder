package holder

import grails.validation.Validateable

import java.time.LocalDate

enum BulkEntryType {
    EXPENSE, INCOME, ADHOC
}

class BulkEntryCommand implements Validateable {
    BulkEntryType type = BulkEntryType.EXPENSE
    Account singleAccount
    List<Account> legalSingleAccounts
    List<Account> legalMultipleAccounts
    boolean debitSingle = false
    LocalDate periodBegin
    LocalDate periodEnd
    List<BulkEntrySubCommand> entries = new ArrayList<BulkEntrySubCommand>()

    static constraints = {
        singleAccount blank: false
        periodBegin nullable: true
        periodEnd nullable: true
    }

    BulkEntryCommand() {
        initEntries()
    }

    void initEntries() {
        entries = new ArrayList<>()
        10.times {
            entries.add(new BulkEntrySubCommand())
        }
    }

    void addTen() {
        10.times {
            entries.add(new BulkEntrySubCommand())
        }
    }

    void beforeValidate() {
        if (type == BulkEntryType.EXPENSE) {
            legalSingleAccounts = Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(11100, 11200, [sort: 'id'])
            legalSingleAccounts.add(Account.get(30500))
            legalMultipleAccounts = [
                    *holder.Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(10000, 20000, [sort: 'id']).findAll{ !it.name.contains('Depreciation') },
                    *holder.Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(60000, 70000, [sort: 'id']).findAll{ !it.name.contains('Depreciation') }
            ]
            debitSingle = false
        } else if (type == BulkEntryType.INCOME) {
            legalSingleAccounts = Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(11100, 11200, [sort: 'id'])
            legalMultipleAccounts = holder.Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(40000, 50000, [sort: 'id'])
            debitSingle = true
        } else {
            legalSingleAccounts = Account.findAll([sort: 'id'])
            legalMultipleAccounts = holder.Account.findAll([sort: 'id'])
        }
        entries.each {
            if (it.hasEntry()) {
                it.validate()
            }
        }
//        for (int i = 0; i < entries.size(); i++) {
//            BulkEntrySubCommand sc = entries[i]
//            sc.errors = null
//            if (sc.hasEntry()) {
//                if (!sc.date) {
//                    sc.errors = "Missing date"
//                } else if (!sc.description) {
//                    sc.errors = "Missing description"
//                } else if (!sc.account) {
//                    sc.errors = "Missing account"
//                } else if (!sc.premises) {
//                    sc.errors = "Missing premises"
//                } else if (!sc.amount) {
//                    sc.errors = "Missing amount"
//                }
//            }
//        }
//        return entries.find { it.errors} == null
    }
}

class BulkEntrySubCommand implements Validateable {
    LocalDate date
    String description
    Account account
    Room premises
    BigDecimal amount
    Boolean provisional = false
    BigDecimal usefulLife
    DepreciationMethod depreciationMethod
    String notes
    List<AccountEntry> possibleDups = new ArrayList<>()
//    String errors

    static constraints = {
        date blank: false
        description blank: false
        account blank: false
        premises blank: false
        amount blank: false
        provisional nullable: false
        usefulLife nullable: true
        depreciationMethod nullable: true
        notes nullable: true
    }

    boolean hasEntry() {
        return date || description || account || premises || amount || notes
    }

    void beforeValidate() {
        if (date || amount) {
//            possibleDups = AccountEntry.findAllByDateOrAmount(date, amount, [sort: [date: 'asc', amount: 'asc']])
            possibleDups = AccountEntry.findAllByAmount(amount, [sort: [date: 'asc']])
        } else {
            possibleDups.clear()
        }
    }
}
