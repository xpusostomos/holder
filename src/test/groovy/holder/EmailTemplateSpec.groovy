package holder

import grails.testing.gorm.DataTest
import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Shared
import grails.test.hibernate.HibernateSpec


class EmailTemplateSpec extends Specification
        implements DataTest, DomainUnitTest<EmailTemplate> {
//    @Shared EmailTemplate et

    def setupSpec() {
        mockDomain EmailTemplate
        mockDomain User
        mockDomain Role
        mockDomain UserRole
        mockDomain Config
        mockDomain Room
        mockDomain Account
    }

    def setup() {

//        String a = "[a]{b|c}.*d^?\\"
//        String b = a.replaceAll('([]\\[\\\\^$.|?*+(){}])', '\\\\$1')
//        String c = a.replace('([{}|])', '-$1')
        BootStrap.createDefaultLogin()
        Room g1 = Room.findByName('G1')
        Room g = Room.findByName('G')
        EmailTemplate et = new EmailTemplate(
                name: 'SIGN',
                body: 'sign',
                subject: 'SIGN-foo',
                tags: '|foo|',
                room: null
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'sign',
                subject: 'REM-foo',
                tags: '|foo|',
                room: null
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'rem',
                subject: 'REM-foo-G1',
                tags: '|foo|',
                room: g1
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'rem',
                subject: 'REM-bar|baz-G1',
                tags: '|bar|baz|',
                room: g1
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'rem',
                subject: 'REM-broom-G',
                tags: '|broom|',
                room: g
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'rem',
                subject: 'REM-G',
                tags: '|broom|',
                room: g
        )
        et.save()
        et = new EmailTemplate(
                name: 'REM',
                body: 'sign',
                subject: 'REM-plain',
                tags: null,
                room: null
        )
        et.save()
    }

    def cleanup() {
    }

    void "test email template search"() {
        System.out.println("HERE")
        expect:"fix me"
        Room g1 = Room.findByName('G1')
        Room g = Room.findByName('G')
        Room e1 = Room.findByName('E1')
        EmailTemplate.list().size() > 0
        Room.list().size() > 0
//        EmailTemplate.findByTags('\\|bar\\|')?.subject == 'REM-bar|baz-G' // works
//        EmailTemplate.findByTagsLike('\\|bar\\|')?.subject == 'REM-bar|baz-G' // query returns null
//        EmailTemplate.findByTagsLike('%\\|bar\\|%')?.subject == 'REM-bar|baz-G' // query returns wrong object
        EmailTemplate.findTemplate('REM', ['foo'], g1)?.subject == 'REM-foo-G1'
        EmailTemplate.findTemplate('SIGN', ['foo'], null)?.subject == 'SIGN-foo'
        EmailTemplate.findTemplate('REM', ['bar'], g1)?.subject == 'REM-plain'
        EmailTemplate.findTemplate('REM', ['baz', 'bar'], g1)?.subject == 'REM-bar|baz-G1'
        EmailTemplate.findTemplate('REM', ['broom'], g1)?.subject == 'REM-broom-G'
        EmailTemplate.findTemplate('REM', ['broom'], g1)?.subject == 'REM-broom-G'
        EmailTemplate.findTemplate('REM', ['foo'], e1)?.subject == 'REM-foo'
        EmailTemplate.findTemplate('REM', ['blaze'], e1)?.subject == 'REM-plain'
    }
}
