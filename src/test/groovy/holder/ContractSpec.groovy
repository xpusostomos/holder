package holder

import grails.plugin.springsecurity.SpringSecurityService
import grails.testing.gorm.DataTest
import grails.testing.gorm.DomainUnitTest
import grails.testing.services.ServiceUnitTest
import grails.util.Holders
import org.grails.datastore.mapping.engine.event.ValidationEvent
import spock.lang.Specification

import java.time.LocalDate
import java.time.LocalDateTime

class ContractSpec extends Specification implements DomainUnitTest<Contract>, ServiceUnitTest<RecordUserAuditService>, DataTest {

    void  setupSpec() {
       mockDomains Config, Person
    }

    def setup() {
        SpringSecurityService.metaClass.getCurrentUser = { return new User(id:1) }
        Holders.grailsApplication.mainContext.beanFactory.registerSingleton('springSecurityService', new SpringSecurityService())
        Config config = new Config(
                name: ConfigNameEnum.STD_PERIOD_END_DATE.toString(),
                date: LocalDate.of(2020, 12, 27)
        )
        ValidationEvent event = new ValidationEvent(datastore, config)
        service.onValidate(event)
        config.save()
    }

    def cleanup() {
    }

    void "test contract"() {
        given:
        Person p = new Person(
        )
        Contract c = new Contract(
                person: p,
                checkIn: LocalDateTime.of(2022, 1, 24, 12, 0, 0),
                checkOut: LocalDateTime.of(2022, 2, 20, 12, 0, 0)
        )

        expect:
        c.nextPeriodNumberOfDays(null, null) == 28

        when:
        c.checkoutDayIsPaid = false
        then:"number of days exclusive"
        c.nextPeriodNumberOfDays(null, null) == 27
    }
}
