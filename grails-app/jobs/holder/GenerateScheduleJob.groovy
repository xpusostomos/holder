package holder

import grails.core.GrailsApplication

class GenerateScheduleJob {
    GrailsApplication grailsApplication
    ScheduleService scheduleService

    static triggers = {
        cron name: 'generateSchedule', startDelay: 10000, cronExpression: grailsApplication.config.getProperty('generateScheduleJobCron')
    }

    def execute() {
        if (Config.getBool(ConfigNameEnum.GEN_SCHEDULE_ENABLED)) {
            Task.findAll().forEach {
                it.generateSchedule().each {
                    it.save()
                    scheduleService.save(it)
                }
            }
        }
    }
}
