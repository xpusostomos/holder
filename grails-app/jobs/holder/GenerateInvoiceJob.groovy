package holder

import grails.core.GrailsApplication

class GenerateInvoiceJob {
    GrailsApplication grailsApplication
    AccountEntryService accountEntryService
    PersonService personService

    static triggers = {
        cron name: 'generateInvoice', startDelay: 10000, cronExpression: grailsApplication.config.getProperty('generateInvoiceJobCron')
    }

    def execute() {
        try {
            if (Config.getBool(ConfigNameEnum.GEN_INVOICE_ENABLED)) {
                Contract.findAllByAgreedAndClosedAndPredecessor(true, false, null).forEach {
                    List<AccountEntry> aes = it.generateAccountEntriesWithinReminderPeriodOrAtLeastTheFirstOne()
                    aes.each { ae ->
                        it.person.addToAccountEntries(ae)
                        it.addToAccountEntries(ae)
                        ae.save()
                        accountEntryService.save(ae)
                    }
                    // This isn't quite right if a person were to have multiple contracts
                    if (aes.size() > 0 && it.person.amountOwing.isAtLeast(Config.getDecimal(ConfigNameEnum.MIN_REMIND_AMT))) {
                        personService.generatePaymentDueEmail(it.person)
                    }
                }
            }
        } catch (Exception x) {
            x.printStackTrace()
        }
    }
}
