package holder

import grails.core.GrailsApplication

class RemindScheduleJob {
    GrailsApplication grailsApplication
    ScheduleService scheduleService

    static triggers = {
        cron name: 'remindSchedule', startDelay: 10000, cronExpression: grailsApplication.config.getProperty('remindScheduleJobCron')
    }

    def execute() {
        if (Config.getBool(ConfigNameEnum.GEN_REMINDER_ENABLED)) {
            Schedule.findAllByCompleteAndCancelledAndSendMail(false, false, true).forEach {
                scheduleService.checkReminderAndSendEmail(it)
            }
        }
    }
}
