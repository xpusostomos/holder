<%@ page import="holder.MailIndex; grails.plugin.asyncmail.AsynchronousMailMessage" %>
<table>
    <thead>
    <tr>
        <th colspan="5">Emails</th>
    </tr>
    <tr>
        <th>Date</th><th>Status</th><th>Subject</th><th>To</th><th>Text</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${MailIndex.findAllByEntityIdAndEntityName(entityId, entityName, [sort: 'dateCreated'])}" var="mi">
        <g:set var="mail" value="${mi.mail}"/>
        <tr>
            <td><f:display type="date" bean="${mail}" property="createDate"/></td>
            <td>${mail.status}</td>
            <td>${mail.subject}</td>
            <td>To: ${mail.to} ${mail.cc.size() > 0 ? "CC: $mail.cc" : ''}${mail.bcc.size() > 0 ? "BCC: $mail.bcc" : ''}</td>
            <td><pre>${mail.text}</pre></td>
        </tr>
    </g:each>
    </tbody>
</table>
