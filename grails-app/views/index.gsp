<%@ page import="holder.User; holder.User; holder.Person" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Turner Rooms for Rent</title>
</head>
<body>
<h1 style="text-align:center;font-size: xx-large;">Turner Rooms for Rent</h1>
%{--<img href="/assets/images/house/front-of-house.jpg"/>--}%
%{--<img src="${resource(dir: "images/house", file:"front-of-house.jpg")}"/>--}%
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>

<p style="text-align:center;">
<asset:image src="house/front-of-house.jpg" alt="Front of house"/>
</p>
<g:if test="${availableRooms.isEmpty()}">
    <div style="justify-content: center;display: flex;">
    Sorry, there are no rooms available for rent at the current time. Check back regularly for updates
    </div>
</g:if>
<g:else>
    <div style="justify-content: center;display: flex;">
        <fieldset class="buttons">
            <g:link controller="apply">Click here to apply to see or rent the room</g:link>
        </fieldset>
    </div>
    <table>
        <thead>
            <tr><th colspan="4">Rooms Available</th></tr>
            <tr>
                <th>Room</th>
                <th>Date Available</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
        <g:each in="${availableRooms}" var="room">
            <tr>
                <td><f:display bean="${room}" property="name"/></td>
                <td><f:display bean="${room}" property="dateAvailable"/></td>
                <td><g:link action="showRoom" id="${room.id}"><f:display bean="${room}" property="notes"/></g:link></td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <p style="text-align:center;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d19149.630382776435!2d149.11739669392117!3d-35.272963801766004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzXCsDE2JzAxLjciUyAxNDnCsDA3JzM3LjMiRQ!5e0!3m2!1sen!2sau!4v1617591775614!5m2!1sen!2sau" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </p>
    <div style="justify-content: center;display: flex;">
        <fieldset class="buttons">
            <g:link controller="apply">Click here to apply to see or rent the room</g:link>
        </fieldset>
    </div>
</g:else>
<hr>
<div style="justify-content: center;display: flex;">
<g:if test="${User.principal}">
    <a href="mailto:${User.principal.email}">Email for enquiries</a>
</g:if>
</div>
%{--<g:formatDate date="${ java.time.LocalDate.now()}"/>--}%
%{--<g:formatDate date="${ new java.util.Date()}"/>--}%

</html>
