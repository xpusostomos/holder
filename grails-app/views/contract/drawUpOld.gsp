<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 20/03/2021
  Time: 5:34 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Draw up Contract</title>
</head>

<body>
<g:uploadForm>
    <g:hiddenField name="booking.id" value="${booking.id}"/>
    <g:hiddenField name="contract.booking.id" value="${contract.booking.id}"/>
    <g:render template="/contract/edit" model="[title: 'CONTRACTS']"/>
    <g:actionSubmit class="btn btn-primary" action="drawUp2" value="Next"/>

</g:uploadForm>
</body>
</html>
