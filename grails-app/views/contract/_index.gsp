<table class="table table-striped table-bordered">
    <thead class="thead-light">
    <tr><th colspan="8">${title}</th></tr>
    <tr>
        <th>ID</th>
        <th>Person</th>
        <th>Date From</th>
        <th>Date To</th>
        <th>Move Out Date</th>
        <th>Rent</th>
        <th>Bond</th>
        <th>Booking</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${contractList}">
        <tr>
%{--            <td><g:link controller="contract" action="${it.person ?'edit' : 'drawUp'}" id="${it.id}">${it.id}</g:link></td>--}%
            <td><g:link controller="contract" action="edit" id="${it.id}">${it.id}</g:link></td>
            <td><g:link controller="person" action="edit" id="it.person.id">${it?.person?.name}</g:link></td>
            <td>${it.checkIn}</td>
            <td>${it.checkOut}</td>
            <td>${it.moveOut}</td>
            <td>${it.rent}</td>
            <td>${it.bond}</td>
            <td><g:link controller="booking" action="edit" id="${it.booking?.id}">${it.booking?.id}</g:link></td>
        </tr>
    </g:each>
    </tbody>
</table>
