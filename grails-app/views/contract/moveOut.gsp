<%@ page import="holder.DateTimeValueConverter" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Move Out</title>
</head>

<body>
<g:form action="saveMoveOut" id="${contract.id}">
    <f:field bean="contract" property="noticeGiven" label="Notice Given"/>
    <f:field bean="contract" property="moveOut" label="Move Out Date"/>
    <f:field bean="contract" property="payUntil" label="Pay Until Date"/>
    <div class="fieldcontain">
        <label for="override">Override contract violation</label>
        <g:checkBox name="override" value="${false}"/>
    </div>
    <g:submitButton name="Submit" value="Submit"/>
</g:form>
</body>
</html>
