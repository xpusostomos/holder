<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'contract.label', default: 'Contract')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-contract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="indexShortTerm"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-contract" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.contract}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.contract}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:each in="${this.contract.accountEntries}" var="ae">
                <g:hasErrors bean="${ae}">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${ae}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                </g:hasErrors>
            </g:each>
            <g:form action="updateShortTerm" id="${contract.id}" method="PUT">
                <g:hiddenField name="version" value="${contract?.version}" />
                <fieldset class="form">
                    <f:field bean="contract" property="agent" value="${contract.agent}"/>
                    <f:field bean="contract" property="room" value="${contract.room}" label="Property"/>
                    <f:field bean="contract" property="person" value="${contract.person}" label="Payer"/>
                    <f:field bean="contract" property="checkIn" value="${contract.checkIn.toLocalDate()}"  wrapper="localDate"/>
                    <f:field bean="contract" property="checkOut" value="${contract.checkOut.toLocalDate()}"  wrapper="localDate"/>
                    <f:field bean="contract" property="rent" value="${contract.rent}"/>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="update" class="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
