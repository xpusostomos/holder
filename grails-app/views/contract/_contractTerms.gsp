<%@ page import="holder.SexEnum; holder.User" %>
<ol>
    <li>This contract is between ${booking.name} (the lodger)
    and ${User.principal.name} (the landlord).
    </li>
    <li>The room will be held for you once you've paid security bond of $${contract.bond}</li>
    <li>Lease Period: From <g:formatDate date="${contract.checkIn}"/> to <g:formatDate date="${contract.checkOut}"/></li>
    <li>Rent is to be paid 4 weekly in cash in advance. The first payment as laid out below should be paid before moving in.
        <table style="width:500px;">
        <tr><th>Date</th><th>Days</th><th>Amount</th></tr>
        <g:each in="${fees}" var="fee">
            <tr><td>${fee.periodBegin}</td><td>${fee.days}</td><td>${fee.amount}</td></tr>
        </g:each>
    </table>
    </li>
    <li>Notification period to move out is 2 weeks.</li>
    <li>Penalty for moving out within the lease period: loss of bond
    plus you must pay rent for 2 weeks after you give notice.</li>
    <li>Moving out after the lease period: You must pay rent for 2 weeks after you give notice.</li>
    <li>If you wish to move out on the contract end date, you must give notice. If you don't give notice
    it will be assumed you are continuing on, in which case you should continue to pay the regular
    rent every ${contract.rentPeriod} days.</li>
    <li>Price INCLUDES broadband internet connectivity, electricity, heating, water. You must
    supply your own phone and food.</li>
    <li>Leave the room clean and tidy when you move out.</li>
    <li>You should clean up your mess in the kitchen and bathroom as you go.</li>
    <li>You will be responsible for cleaning one of the following each week (one will be
    selected with landlord at time of signing):
    • Kitchen
    • Vacuum upstairs + staircase
    • Clean bathroom
    • Clean washroom / toilet
    </li>
    <g:if test="${!contract.carSpot}">
        <li>You may not park a car on the property. If you wish to park a car, contact the landlord concerning the fee.</li>
    </g:if>
    <g:else>
        <li>You may park a car on the property on space ${contract.carSpot.fullName}.
    </g:else>
    <li>The room is for one ${booking.sex == holder.SexEnum.COUPLE ? 'couple' : 'person'} only.</li>
    <li>No Smokers please.</li>
    <li>Rent is payable when you are on holidays at the full rate. If you will be on holidays on
    the date rent is due, it is your responsibility to pay before you leave.</li>
    <li>No sub-leasing of the room.</li>
    <li>You are responsible for the safety and insurance of your own personal effects. Neither
    the owner, lessor, nor anyone else in the household is to be responsible for any losses, or
    damages incurred including those of personal effects or personal injury, whether or not
    due to negligence.</li>
    <li>Name of Lodger:</li>
    <li>Signed:</li>
    <li>Inventory: (Anything not required, can cross out and return item).
    Bed, bookshelf (x2), table, chair, lamp.
    1x 32” TV with remote control.</li>


</ol>
