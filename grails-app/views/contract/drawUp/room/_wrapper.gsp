<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
%{--<g:select name="${property}"--}%
%{--          id="${property}"--}%
%{--          from="${holder.Room.allByDate()}"--}%
%{--          noSelection="${['':'']}"--}%
%{--          optionKey="${{$it.id}}"--}%
%{--          optionValue="${{"$it.name ($it.availableDesc)"}}"--}%
%{--          value="">--}%
    <select name="${property}" id="${property}" ${required ? 'required' : ''}>
        <option value=""></option>
        <g:each in="${holder.Room.allAvailableOnKnownDate()}" var="room">
            <option value="${room.id}" ${value == room.id ? 'selected' : ''}>${room.name} (${room.availableDesc})</option>
        </g:each>
        <g:each in="${holder.Room.allNotAvailableOnKnownDate()}" var="room">
            <option value="${room.id}" ${value == room.id ? 'selected' : ''}>${room.name} (UNAVAILABLE)</option>
        </g:each>
    </select>
</div>
