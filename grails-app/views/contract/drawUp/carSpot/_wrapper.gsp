<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:select name="${property}"
              id="${property}"
              from="${holder.Room.findByRoomType(holder.RoomTypeEnum.CARSPOT)}"
              noSelection="${['':'']}"
              optionKey="id"
              optionValue="${{"$it.name ($it.availableDesc)"}}"
              value="${value}">
%{--    required="${false}">--}%
%{--        <g:if test="${!required}">--}%
%{--            <option value=""></option>--}%
%{--        </g:if>--}%
%{--        <g:each in="${holder.Room.findByRoomType(holder.RoomTypeEnum.CARSPOT)}" var="room">--}%
%{--            <option value="${room.id}">${room.name} (${room.availableDesc})</option>--}%
%{--        </g:each>--}%
%{--        <g:each in="${holder.Room.allNotAvailableOnKnownDate()}" var="room">--}%
%{--            <option value="${room.id}">${room.name} (UNAVAILABLE)</option>--}%
%{--        </g:each>--}%
    </g:select>
</div>
