<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 20/03/2021
  Time: 5:34 pm
--%>

<%@ page import="holder.DateTimeValueConverter" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <meta name="layout" content="main"/>
  <title>Draw up Contract</title>
</head>

<body>
<g:uploadForm action="send">
  <g:hiddenField name="contract.id" value="${contract.id}"/>
  <g:hiddenField name="contract.booking.id" value="${contract.booking.id}"/>
%{--  <g:hiddenField name="contract.booking.id" value="${contract.booking.id}"/>--}%
  <g:hiddenField name="contract.checkIn" value="${DateTimeValueConverter.format(contract.checkIn)}"/>
  <g:hiddenField name="contract.checkOut" value="${DateTimeValueConverter.format(contract.checkOut)}"/>
  <g:hiddenField name="contract.rent" value="${contract.rent}"/>
  <g:hiddenField name="contract.bond" value="${contract.bond}"/>
  <g:hiddenField name="contract.rentPeriod" value="${contract.rentPeriod}"/>
  <g:hiddenField name="contract.useStdPayDates" value="${contract.useStdPayDates}"/>
  <g:hiddenField name="contract.notes" value="${contract.notes}"/>
  <g:hiddenField name="contract.room.id" value="${contract.room.id}"/>
  <h1>Preview</h1>
  <span class="border border-primary">
  ${raw(contract.contractTerms)}
  </span>
  <div class="form-row">
    <label for="contract.contractTerms">Contract Terms</label>
    <g:textArea class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('contractTerms') ? 'is-invalid' : 'is-valid'}" name="contract.contractTerms"  rows="15" cols="100">
${contract.contractTerms}
    </g:textArea>

    <div class="invalid-feedback"><g:fieldError field="contractTerms" bean="${contract}"/></div>

  </div>
  <fieldset class="buttons">
    <g:actionSubmit class="view" action="preview" value="${message(code: 'default.button.preview.label', default: 'Preview')}"/>
    <g:actionSubmit class="save" action="saveToResume" value="${message(code: 'default.button.save.label', default: 'Save')}"/>
    <g:actionSubmit class="email" action="send" value="${message(code: 'default.button.send.label', default: 'Send to tenant')}"/>
  </fieldset>

</g:uploadForm>
</body>
</html>
