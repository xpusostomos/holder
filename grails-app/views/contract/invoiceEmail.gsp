Hi,

A reminder that rent is due:

Click here to see your rental calendar: <g:createLink controller="${contract.person.home.controller}" action="${contract.person.home.action}" id="${contract.person.home.id}" absolute="${true}"/>

There is a black box in the entry with envelopes.
If you wish, you can put your rent in the envelope (please don't fold cash) with your name,
put it in the box and email me, and I will pick it up.

Or if you prefer, send me an email when you are home, and I will pick it up from you from your room.

---
Chris
