<%@ page import="holder.PersonType; holder.Person" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'contract.label', default: 'Contract')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
<g:set var="springSecurityService" bean="springSecurityService"/>
<g:set var="person" value="${Person.findByUser(springSecurityService.currentUser)}"/>
    <body>
        <a href="#list-contract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                </sec:ifAllGranted>
                <li><g:link class="create" action="createShortTerm" params="[all: all]"><g:message code="default.newShortTerm.label" args="[entityName]" default="New Short Term Contract"/></g:link></li>
            </ul>
        </div>
        <div id="list-contract" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <table>
                <thead>
                <tr><th>ID</th><th>Name</th><th>Room</th><th>Rent</th><th>Check In</th><th>Check Out</th></tr>
                </thead>
                <tbody>
                <g:each in="${contractList}" var="v">
                <tr>
                    <td><g:link action="showShortTerm" id="${v.id}">${v.id}</g:link></td>
                    <td>${v.name}</td>
                    <td>${v.room}</td>
                    <td>${v.rent}</td>
                    <td>${v.checkIn?.toLocalDate()}</td>
                    <td>${v.checkOut?.toLocalDate()}</td>
                </tr>
                </g:each>
                </tbody>
            </table>
%{--            <f:table collection="${contractList}" properties="id,name,room,rent,checkIn,checkOut" except="[]"/>--}%

            <div class="pagination">
                <g:paginate total="${contractCount ?: 0}" />
            </div>
        </div>
    <g:if test="${person?.type == PersonType.AGENT}">
        <g:render template="/person/ledger" model="[title: "ACCOUNT ENTRIES FOR $person.name", person: person, openingBalance: openingBalance, accountEntries: accountEntries]"/>
    </g:if>
    </body>
</html>
