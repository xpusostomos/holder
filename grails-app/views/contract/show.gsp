<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'contract.label', default: 'Contract')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-contract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="createShortTerm"><g:message code="default.newShortTerm.label" args="[entityName]" default="New Short Term Contract"/></g:link></li>
            </ul>
        </div>
        <div id="show-contract" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="contract" />
            <g:form resource="${this.contract}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.contract}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <g:if test="${!this.contract.agreed}">
                        <g:link class="resume" action="resume" resource="${this.contract}"><g:message code="default.button.resume.label" default="Resume Editing" /></g:link>
                        <g:link class="resume" action="manualSignup" resource="${this.contract}"><g:message code="default.button.manualSignup.label" default="Manual Signup" /></g:link>
                    </g:if>
                    <g:link class="resume" action="manualCancel" resource="${this.contract}"><g:message code="default.button.manualCancel.label" default="Manual Cancel" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    <g:link class="create" action="extendContract" id="${contract.id}"><g:message code="default.extend.label" args="[entityName]" default="Extend Contract"/></g:link>
                    <g:link class="redraw" action="redraw" id="${contract.id}"><g:message code="default.extend.label" args="[entityName]" default="Redraw Contract"/></g:link>
                    <g:link class="moveIn" action="moveOut" id="${contract.id}"><g:message code="default.extend.label" args="[entityName]" default="Move In"/></g:link>
                    <g:link class="moveOut" action="moveOut" id="${contract.id}"><g:message code="default.extend.label" args="[entityName]" default="Move Out"/></g:link>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
