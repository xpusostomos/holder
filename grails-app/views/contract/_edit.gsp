<%@ page import="holder.DateTimeValueConverter; holder.Contract" %>

<g:hiddenField name="contract.id" value="${contract.id}"/>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="contract.checkIn">Date From</label>
            <g:field class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('checkIn') ? 'is-invalid' : 'is-valid'}" type="text" name="contract.checkIn" onload="this.datetimepicker()" value="${contract.checkIn.format(DateTimeValueConverter.stdFormatter)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkIn" bean="${contract}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="contract.checkOut">Date To</label>
            <g:field class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('checkOut') ? 'is-invalid' : 'is-valid'}" type="text" name="contract.checkOut" value="${contract.checkOut.format(DateTimeValueConverter.stdFormatter)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkOut" bean="${contract}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="contract.rent">Rent</label>
            <g:field class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('rent') ? 'is-invalid' : 'is-valid'}" type="number" name="contract.rent" value="${contract.rent}" step="1" min="0"/>
            <div class="invalid-feedback"><g:fieldError field="rent" bean="${contract}"/></div>
        </div>
        <div class="form-group col-md-4">
            <label for="contract.bond">Bond</label>
            <g:field class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('bond') ? 'is-invalid' : 'is-valid'}" type="number" name="contract.bond" value="${contract.bond}" step="1" min="0"/>
            <div class="invalid-feedback"><g:fieldError field="bond" bean="${contract}"/></div>
        </div>
        <div class="form-group col-md-4">
            <label for="contract.rentPeriod">Rent Period</label>
            <g:field class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('rentPeriod') ? 'is-invalid' : 'is-valid'}" type="number" name="contract.rentPeriod" value="${contract.rentPeriod}" step="1" min="0"/>
            <div class="invalid-feedback"><g:fieldError field="rentPeriod" bean="${contract}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="contract.useStdPayDates">Use Standard Pay Dates?</label>
            <g:checkBox class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('useStdPayDates') ? 'is-invalid' : 'is-valid'}" name="contract.useStdPayDates" value="${contract.useStdPayDates}"/>
            <div class="invalid-feedback"><g:fieldError field="useStdPayDates" bean="${contract}"/></div>
        </div>
    </div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="contract.room.id">Room</label>
        <g:select class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('property') ? 'is-invalid' : 'is-valid'}"
                  name="contract.room.id"
                  noSelection="${['': '']}"
                  from="${holder.Room.findAll(sort: "name")}"
                  optionKey="id"
                  optionValue="name" value="${contract.room?.id}"/>
        <div class="invalid-feedback"><g:fieldError field="room" bean="${contract}"/></div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label for="contract.carSpot.id">Car Spot</label>
        <g:select class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('property') ? 'is-invalid' : 'is-valid'}"
                  name="contract.carSpot.id"
                  noSelection="${['': '']}"
                  from="${holder.Room.findAll(sort: "name")}"
                  optionKey="id"
                  optionValue="name" value="${contract.carSpot?.id}"/>
        <div class="invalid-feedback"><g:fieldError field="carSpot" bean="${contract}"/></div>
    </div>
</div>
    <div class="form-group">
        <label for="contract.notes">Notes</label>
        <g:textArea class="form-control ${!contract.hasErrors() ? '' : contract.errors.getFieldError('notes') ? 'is-invalid' : 'is-valid'}" name="contract.notes" rows="5" cols="100">
${contract.notes}
        </g:textArea>
        <div class="invalid-feedback"><g:fieldError field="notes" bean="${contract}"/></div>
    </div>


<script type="text/javascript">
$(function() {
$('[name$=checkIn]').datetimepicker({format: "Y-m-d H:i"});
$('[name$=checkOut]').datetimepicker({format: "Y-m-d H:i"});
});
</script>
