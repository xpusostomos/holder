<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'contract.label', default: 'Contract')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-contract" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-contract" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
                <ol class="property-list contract">
                    <li class="fieldcontain">
                        <span id="agent-label" class="property-label">Agent</span>
                        <div class="property-value" aria-labelledby="agent-label">${contract.agent}</div>
                    </li>
                    <li class="fieldcontain">
                        <span id="room-label" class="property-label">Room</span>
                        <div class="property-value" aria-labelledby="room-label">${contract.room}</div>
                    </li>
                    <li class="fieldcontain">
                        <span id="person-label" class="property-label">Person</span>
                        <div class="property-value" aria-labelledby="person-label">${contract.person}</div>
                    </li>
                    <li class="fieldcontain">
                        <span id="checkIn-label" class="property-label">Check In</span>
                        <div class="property-value" aria-labelledby="checkIn-label">${contract.checkIn?.toLocalDate()}</div>
                    </li>
                    <li class="fieldcontain">
                        <span id="checkOut-label" class="property-label">Check Out</span>
                        <div class="property-value" aria-labelledby="checkOut-label">${contract.checkOut?.toLocalDate()}</div>
                    </li>
                    <li class="fieldcontain">
                        <span id="rent-label" class="property-label">Rent</span>
                        <div class="property-value" aria-labelledby="rent-label">${contract.rent}</div>
                    </li>
                </ol>
            <g:form resource="${this.contract}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="editShortTerm" resource="${this.contract}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
