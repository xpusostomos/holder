<%@ page import="holder.Person" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:select name="${property}" from="${Person.getAllTags()}" noSelection="${['': '']}" required="${required}"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${Person.getAllTags()}" noSelection="${['': '']}"/>
    </g:else>
</div>
