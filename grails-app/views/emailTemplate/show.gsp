<%@ page import="holder.User" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'emailTemplate.label', default: 'EmailTemplate')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-emailTemplate" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-emailTemplate" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="emailTemplate" />
            <g:form resource="${this.emailTemplate}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.emailTemplate}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <g:link class="copy" action="copy" resource="${this.emailTemplate}"><g:message code="default.button.copy.label" default="Copy" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    <table>
        <thead>
        <tr><th>Guest</th><th>Template ID</th><th>Template Tags</th><th>Template Room</th><th>Subject</th><th>Body</th></tr>
        </thead>
        <g:each in="${holder.Person.currentGuests}" var="person">
            <tr>
                <td>${person.name}</td>
                <g:set var="template" value="${holder.EmailTemplate.findTemplate(emailTemplate.name, person.tagList, person.contracts.sort {a,b -> b.dateCreated <=> a.dateCreated}.first().room)}"/>
%{--                <g:set var="bindings" value="${[config: grailsApplication.config, principal: User.principal, person: person, contract: person.contracts.find(), schedule: null, token: null, task: null]}"/>--}%
                <td>${template ? "$template.id" : 'No template found' }</td>
                <td>${template?.tagList?.size() > 0 ? "$template.tagList" : '' }</td>
                <td>${template?.room ? "$template.room" : '' }</td>
                <td>${template?.subject}</td>
                <td>${template?.body}</td>
            </tr>
        </g:each>
    </table>
    </body>
</html>
