<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'emailTemplate.label', default: 'EmailTemplate')}" />
    <title><g:message code="default.email.label" args="[entityName]" /></title>
</head>
<body>
<a href="#create-emailTemplate" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="create-emailTemplate" class="content scaffold-create" role="main">
    <h1><g:message code="default.email.label" args="[entityName]" default="Send Email" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.emailTemplate}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.emailTemplate}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.emailTemplate}" method="POST">
        <fieldset class="form">
            %{--            <f:all bean="emailTemplate"/>--}%
            <f:field bean="bulkEmailCommand" property="templateName"/>
            <f:field bean="bulkEmailCommand" property="room"/>
            <f:field bean="bulkEmailCommand" property="tag"/>
            <f:field bean="bulkEmailCommand" property="alsoSendToBillingEmail"/>
            <f:field bean="bulkEmailCommand" property="toAddress"/>
            <f:field bean="bulkEmailCommand" property="ccAddress"/>
            <f:field bean="bulkEmailCommand" property="bccAddress"/>
            <f:field bean="bulkEmailCommand" property="body"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
