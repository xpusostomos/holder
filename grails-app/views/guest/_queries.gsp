<table>
    <thead>
    <tr><th colspan="4">${title}</th></tr>
    <tr><th>Comment</th><th>Date</th><th>From</th><th></th></tr>
    </thead>
    <tbody>
    <g:if test="${queryList.isEmpty()}">
        <tr><td colspan="2">None</td></tr>
    </g:if>
    <g:else>
        <g:each in="${queryList}" var="r">
            <tr>
                <td><b>${r.text}<b></b></td><td>${r.dateCreated.toLocalDate()}</td><td>${r.from.name}</td>
                <td>
                    <g:if test="${r.comments.isEmpty()}">
                        <g:form>
%{--                            <g:hiddenField name="comment.id" value="${r.id}"/>--}%
                            <g:hiddenField name="comment.parent.id" value="${r.id}"/>
                            <g:hiddenField name="comment.from.id" value="${r.from.id}"/>
                            <g:hiddenField name="comment.to.id" value="${r.to.id}"/>
                            <g:if test="${!r.closed}">
                            <div class="form-group">
                                <label for="comment.text">Add Comment</label>
                                <g:textArea class="form-control" type="text" name="comment.text" rows="2" cols="100"/>
                            </div>
                            <g:actionSubmit class="btn btn-primary" action="addComment" value="Add Comment"/>
                            <g:actionSubmit class="btn btn-primary" action="closeQuery" value="Close Query"/>
                            </g:if>
                        </g:form>
                    </g:if>
                </td>
            </tr>
            <g:each in="${r.comments.sort{ a, b -> a.dateCreated <=> b.dateCreated }}" var="c" status="i">
                <tr>
                    <td>${c.text}</td><td>${c.dateCreated.toLocalDate()}</td><td>${c.from.name}</td>
                    <td>
                        <g:if test="${i == r.comments.size()-1}">
                            <g:form>
                                <g:hiddenField name="comment.parent.id" value="${r.id}"/>
%{--                                <g:hiddenField name="comment.parent.id" value="${r.id}"/>--}%
%{--                                <g:hiddenField name="comment.from.id" value="${r.from.id}"/>--}%
                                <g:hiddenField name="comment.from.id" value="${r.from.id}"/>
                                <g:hiddenField name="comment.to.id" value="${r.to.id}"/>
                                <g:if test="${!r.closed}">
                                <div class="form-group">
                                    <label for="comment.text">Add Comment</label>
                                    <g:textArea class="form-control" type="text" name="comment.text" rows="2" cols="100"/>
                                </div>
                                <g:actionSubmit class="btn btn-primary" action="addComment" value="Add Comment"/>
                                <g:actionSubmit class="btn btn-primary" action="closeQuery" value="Close Query"/>
                                </g:if>
                            </g:form>
                        </g:if>
                    </td>
                </tr>
            </g:each>
        </g:each>
    </g:else>
    </tbody>
</table>
