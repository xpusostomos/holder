<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="holder.TipTypeEnum; holder.Tip; holder.Comment; java.time.LocalDate" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:if test="${person}">
    <title>Home page - ${person.name}</title>
    </g:if>
</head>

<body>
<g:if test="${person}">
    <h1>Home page - ${person.name}</h1>

    <p>
        Room:
        <g:each in="${person.contracts.findAll{it.active}.room}">
            ${it.name}
        </g:each>
    </p>
    Last Payment: ${lastPayment?.date} - $${lastPayment?.amount}

    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <br/>
    <g:hasErrors bean="${this.booking}">
        <ul class="errors" role="alert">
            <g:eachError bean="${query}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form>
        <g:hiddenField name="query.from.id" value="${person.user.id}"/> // TODO get rid of user id
        <div class="form-group">
            <label for="query.text">Request Assistance from Landlord. Message:</label>
            <g:textArea class="form-control" type="text" name="query.text" rows="5" cols="100"/>
        </div>
        <g:actionSubmit class="btn btn-primary" action="submitQuery" value="Submit Query"/>
    </g:form>
    <g:if test="${Comment.findAllByParentAndClosedAndFrom(null, false, person.user).size() > 0}">
        <g:render template="/guest/queries" model="[queryList: Comment.findAllByParentAndClosedAndFrom(null, false, person.user, [sort: 'dateCreated']), title: 'Outstanding Queries', person: person]"/>
    </g:if>
    <g:render template="calendar" model="[dates: dates, events: events]"/>
    <g:render template="/person/ledger" model="[person: person, person: accountEntry.person, openingBalance: 0.0, accountEntries: person.accountEntries]"/>
    <g:if test="${Comment.findAllByParentAndClosedAndFrom(null, true, person.user).size() > 0}">
    <g:render template="/guest/queries" model="[queryList: Comment.findAllByParentAndClosedAndFrom(null, true, person.user, [sort: 'dateCreated']), title: 'Closed Queries', person: person]"/>
    </g:if>

    <g:render template="/guest/tipList" model="[tips: [holder.Tip.getRandomTip(holder.TipTypeEnum.TIP, person)], title: 'Tip of the Day']"/>
    <table>
    <tr><th><g:link action="tips" id="${person.id}">Click Here to see all tips</g:link></th></tr>
    </table>

</g:if>
</body>
</html>
