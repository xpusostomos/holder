
<table class="table table-striped table-bordered">
    <thead class="thead-light">
    <tr><th colspan="7">${title}</th></tr>
    <tr>
        <th>ID</th>
        <th>Name</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${personList}">
        <tr>
            <td>${it.id}</td>
            <td><g:link controller="guest" action="home" id="${it.id}">${it.name}</g:link></td>
        </tr>
    </g:each>
    </tbody>
</table>
