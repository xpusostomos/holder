<table>
    <thead>
    <tr><th colspan="4">${title}</th></tr>
    %{--<tr><th>Comment</th><th>Date</th><th>From</th><th></th></tr>--}%
    </thead>
    <tbody>
    <g:each in="${tips}" var="t">
        <g:render template="tip" model="[tip: t]"/>
    </g:each>
    </tbody>
</table>
