<table>
    <thead>
    <tr><th colspan="4">${title}</th></tr>
    %{--<tr><th>Comment</th><th>Date</th><th>From</th><th></th></tr>--}%
    </thead>
    <tbody>
    <g:each in="${tips.keySet()}" var="c">
        <tr>
            <th>${c}</th>
        </tr>
        <g:each in="${tips.get(c)}" var="t">
            <g:render template="tip" model="[tip: t]"/>
        </g:each>
    </g:each>
    </tbody>
</table>
