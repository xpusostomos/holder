<asset:stylesheet src="calendar.css"/>
<%@ page import="java.time.LocalDate" %>
<g:set var="cal" value="${Calendar.getInstance()}"/>
<g:set var="monthPairs" value="${((dates?:[]) + events.findAll{it.key}.collect { it.key }).sort().collect { [Y: it.getYear(), M: it.getMonthValue()-1]}.unique()}"/>
%{--<g:set var="Y" value="${cal.get(Calendar.YEAR)}"/>--}%
<g:set var="today" value="${LocalDate.now()}"/>
<g:set var="monthNames" value="${["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]}"/>
<g:set var="monthLength" value="${[31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]}"/>

<table class="calendar">
<g:each in="${monthPairs}" var="${YM}">
    <g:set var="days[1]" value="${(YM.Y % 4 == 0 && YM.Y % 100 != 0) || (YM.Y % 400 == 0) ? 29 : 28}"/>
    <tr><th colspan="7">${monthNames[YM.M]} ${YM.Y}</th></tr>
    <tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr>
    <g:set var="ld" value="${java.time.LocalDate.of(YM.Y, YM.M+1, 1)}"/>
%{--    <tr><td>${ld}</td></tr>--}%
%{--    <% cal.set(Calendar.MONTH, YM.M) %>--}%
%{--    <% cal.set(Calendar.DAY_OF_MONTH, 1) %>--}%
%{--    <tr><td>DAY: ${ld} ${ld.getDayOfWeek().getValue()}</td></tr>--}%
    <g:set var="spaces" value="${ld.getDayOfWeek().getValue()-1}"/>
    <g:set var="d" value="${0}"/>
%{--    <g:each in="${0..<spaces}">--}%
%{--        <td></td>--}%
%{--    </g:each>--}%
%{--    <g:each in="${spaces..7-1}" var="n">--}%
%{--        <g:set var="day" value="${LocalDate.of(YM.Y, YM.M+1, d+1)}"/>--}%
%{--        <td class="${day.equals(today) ? 'today' : ''}">--}%
%{--            <p class="${day.equals(today) ? 'today' : ''}">${d+1}</p>--}%
%{--        </td>--}%
%{--        <g:set var="d" value="${d+1}"/>--}%
%{--    </g:each>--}%
    <g:each in="${0..5}" var="w">
        <g:if test="${d < monthLength[YM.M]}">
            <tr>
                <g:if test="${w == 0}">
                    <g:each in="${0..<spaces}">
                        <td></td>
                    </g:each>
                </g:if>
                <g:each in="${(w*7+d)..(w*7+d+6-spaces)}" var="n">
                    <g:if test="${d < monthLength[YM.M]}">
                        <g:set var="day" value="${LocalDate.of(YM.Y, YM.M+1, d+1)}"/>
                        <td class="${day.equals(today) ? 'today' : ''}">
                            <p class="${day.equals(today) ? 'today' : ''}">${d+1}</p>
                            <g:each in="${events.findAll { it.key == day }}">
                                <br>
                                <p class="event" style="color: ${it.color};">
                                    <g:if test="${it.controller || it.action || it.id}">
                                        <g:link style="color: ${it.color};" controller="${it.controller}" action="${it.action}" id="${it.id}">${it.value}</g:link>
                                    </g:if>
                                    <g:elseif test="${it.url}">
                                        <a href="${it.url}">${it.value}</a>
                                    </g:elseif>
                                    <g:else>
                                        ${it.value}
                                    </g:else>
                                </p>
                            </g:each>
                        </td>
                        <g:set var="d" value="${d+1}"/>
                    </g:if>
                    <g:else>
                        <td></td>
                    </g:else>
                </g:each>
                <g:if test="${w == 0}">
                    <g:set var="spaces" value="${0}"/>
                </g:if>
            </tr>
        </g:if>
    </g:each>
</g:each>
</table>
