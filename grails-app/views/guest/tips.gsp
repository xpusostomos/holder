<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="holder.Comment; java.time.LocalDate" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:if test="${person}">
        <title>Home page - ${person.name}</title>
    </g:if>
</head>

<body>
<g:render template="/guest/tips" model="[tips: tips, title: 'Tips']"/>
</body>
</html>
