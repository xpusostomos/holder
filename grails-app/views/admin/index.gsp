<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 7/12/2020
  Time: 2:42 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
</head>

<body>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<g:link controller="admin"
        action="diagnostics">DIAGNOSTICS</g:link>
</sec:ifAllGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
<g:if test="${schedule.size() > 0}">
    <h1><g:message code="admin.schedule.label" default="Schedule List" /></h1>
    <f:table collection="${schedule}" properties="task.name, person, due, lateHours, lastReminder, remindersSent, task.description"/>
</g:if>

<g:if test="${todo.size() > 0}">
    <h1><g:message code="admin.todo.label" default="To Do List" /></h1>
    <f:table collection="${todo}" properties="summary,room,person,deadline"/>
</g:if>
</sec:ifAnyGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
<g:if test="${requestList.size() > 0}">
<h1><g:message code="admin.requestInspections.label" default="Requests for Inspections" /></h1>
<f:table collection="${requestList}" properties="id,dateInspection,name,email,phone"/>
</g:if>
</sec:ifAnyGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
<g:if test="${inspectionList.size() > 0}">
<h1><g:message code="admin.upcomingInspections.label" default="Upcoming Inspections" /></h1>
<f:table collection="${inspectionList}" properties="name,dateInspection,timeUntilInspection,email,phone"/>
</g:if>
</sec:ifAnyGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
<g:if test="${moneyOwing.size() > 0}">
<h1><g:message code="admin.moneyOwing.label" default="Money Owing" /></h1>
%{--<g:render template="/person/index" model="[personList: moneyOwing, title: 'MONEY OWING']"/>--}%
<f:table collection="${moneyOwing}" properties="name,home,amountOwing,paymentNotified,rooms,user.email,phone"/>
</g:if>
</sec:ifAnyGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
<g:if test="${bookingList.size() > 0}">
<h1><g:message code="admin.unapprovedBookings.label" default="Potential Bookings" /></h1>
%{--<g:render template="/booking/index" model="[bookingList: bookingList, title: 'BOOKINGS']"/>--}%
<f:table collection="${bookingList}" properties="id,name,email,dateInspection,checkIn,checkOut"/>
</g:if>
</sec:ifAnyGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<g:if test="${contractList.size() > 0}">
<h1><g:message code="admin.unapprovedContracts.label" default="Pending Contracts" /></h1>
%{--<g:render template="/contract/index" model="[contractList: contractList, title: 'OUTSTANDING CONTRACTS']"/>--}%
<f:table collection="${contractList}" properties="id,checkIn,checkOut,moveOut,rent,bond,booking"/>
</g:if>
</sec:ifAllGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<g:if test="${queryList.size() > 0}">
<h1><g:message code="admin.outstandingQueries.label" default="Outstanding Comments"/></h1>
<f:table collection="${queryList}" properties="id,from,to,text"/>
%{--<g:render template="/query/index" model="[requestList: requestList, title: 'REQUEST']"/>--}%
</g:if>
</sec:ifAllGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN'>
    <h1><g:message code="admin.rooms.label" default="Rooms" /></h1>
    <f:table collection="${roomList}" properties="name,description,lessee,lessee.home,availableDesc,moveOutDate,moveInDate,validContracts" foo="bar" widget-foo="bar1"/>
</sec:ifAnyGranted>
<sec:ifAnyGranted roles='ROLE_VICE_ADMIN'>
    <h1><g:message code="admin.rooms.label" default="Rooms" /></h1>
    <f:table collection="${roomList}" properties="name,description,lessee, availableDesc,moveOutDate,moveInDate" foo="bar" widget-foo="bar1"/>
</sec:ifAnyGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<g:render template="rawdata"/>
</sec:ifAllGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<g:uploadForm action="update" method="POST">
    <fieldset class="buttons">
        <g:actionSubmit type="submit" controller="admin" action="generateInvoices" value="${message(code: 'default.button.generateInvoices.label', default: 'Generate Invoices')}" />
        <g:actionSubmit type="submit" controller="admin" action="generateSchedule" value="${message(code: 'default.button.generateSchedule.label', default: 'Generate Schedule')}" />
        <g:actionSubmit type="submit" controller="admin" action="generateReminder" value="${message(code: 'default.button.generateReminder.label', default: 'Generate Reminder')}" />
    </fieldset>
</g:uploadForm>
</sec:ifAllGranted>

<sec:ifAnyGranted roles='ROLE_ADMIN'>
    <ul>
    <li><g:link controller="accountEntry" action="index">Account Entry</g:link></li>
    </ul>
</sec:ifAnyGranted>
<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_ACCOUNT_ADD'>
    <ul>
        <li><g:link controller="contract" action="indexShortTerm">Recent Contracts (Recommended)</g:link></li>
        <li><g:link controller="contract" action="indexShortTerm" params="[all: true]">All Contracts</g:link></li>
    </ul>
</sec:ifAnyGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
<ul>
    <li><g:link controller="finance" action="index">Finance Summary</g:link></li>
    <li><g:link controller="finance" action="businessSummary">Business Summary</g:link></li>
    <li><g:link controller="finance" action="journal">Journal</g:link></li>
    <li><g:link controller="finance" action="ledger">Ledger</g:link></li>
    <li><g:link controller="finance" action="generateOpeningEntries">Generate Opening Entries</g:link></li>
    <li><g:link controller="finance" action="generateClosingEntries">Generate Closing Entries</g:link></li>
    <li><g:link controller="finance" action="trialBalance">Trial Balance</g:link></li>
    <li><g:link controller="finance" action="profitAndLoss">Profit and Loss</g:link></li>
    <li><g:link controller="finance" action="balanceSheet">Balance Sheet</g:link></li>
    <li><g:link controller="asset" action="depSchedule">Depreciation Schedule</g:link></li>
    <li><g:link controller="accountEntry" action="bulkEntry">Bulk Entry</g:link></li>
</ul>
</sec:ifAllGranted>

<sec:ifAllGranted roles='ROLE_ADMIN'>
    <h1><g:message code="admin.endpoints.label" default="End Points" /></h1>
    <ul>
        <li><a href="/actuator">actuator</a></li>
        <li><a href="/actuator/beans">beans</a></li>
        <li><a href="/actuator/caches">caches</a></li>
        <li><a href="/actuator/health">health</a></li>
        <li><a href="/actuator/info">info</a></li>
        <li><a href="/actuator/conditions">conditions</a></li>
        <li><a href="/actuator/shutdown">shutdown</a></li>
        <li><a href="/actuator/configprops">configprops</a></li>
        <li><a href="/actuator/env">env</a></li>
        <li><a href="/actuator/loggers">loggers</a></li>
        <li><a href="/actuator/heapdump">heapdump</a></li>
        <li><a href="/actuator/threaddump">threaddump</a></li>
        <li><a href="/actuator/metrics">metrics</a></li>
        <li><a href="/actuator/quartz">quartz</a></li>
        <li><a href="/actuator/scheduledtasks">scheduledtasks</a></li>
        <li><a href="/actuator/mappings">mappings</a></li>
    </ul>
</sec:ifAllGranted>
<a href="https://gitlab.com/xpusostomos/holder">Source Code is Here</a>

<div id="ft">
    <hr/>
    version <g:meta name="info.app.version"/>  user: <sec:username/> Built with Grails <g:meta name="info.app.grailsVersion"/> on <g:meta name="info.app.build.date"/>
    <g:if test="Environment.isDevelopmentMode()">
        Database: ${grailsApplication.config.getProperty("dataSource.url")} ${grailsApplication.config.getProperty("dataSource.driverClassName")}
    </g:if>
    %{--${grailsApplication.config.getProperty("grails.config.locations")}--}%
</div>

</body>
</html>
