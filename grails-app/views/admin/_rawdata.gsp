<g:set var="max" value="100"/>
<h1><g:message code="admin.rawData.label" default="Raw Data" /></h1>
<table>
    <tr>
        <td><g:link controller="config" params="${[max: max]}">CONFIG</g:link></td>
    </tr>
    <tr>
        <td><g:link controller="booking" params="${[max: max]}">BOOKING</g:link></td>
    </tr>
    <tr>
        <td><g:link controller="user" params="${[max: max]}">USER</g:link></td>
        <td><g:link controller="role" params="${[max: max]}">ROLE</g:link></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="token" params="${[max: max]}">TOKEN</g:link></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="person" params="${[max: max]}">PERSON</g:link></td>
        <td><g:link controller="contract" params="${[max: max]}">CONTRACT</g:link></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="comment" params="${[max: max]}">COMMENT</g:link></td>
        <td></td>
    </tr>
    <tr>
        <td><g:link controller="account" params="${[max: max]}">ACCOUNT</g:link></td>
        <td><g:link controller="accountEntry" params="${[max: max]}">ACCOUNT ENTRY</g:link></td>
        <td><g:link controller="asset" params="${[max: max]}">ASSET</g:link></td>
    </tr>
    <tr>
        %{--        <td><g:link controller="property">PROPERTY</g:link></td>--}%
        %{--        <td><g:link controller="section">SECTION</g:link></td>--}%
        <td><g:link controller="room" params="${[max: max]}">ROOM</g:link></td>
        <td><g:link controller="tip" params="${[max: max]}">TIP</g:link></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="toDo" params="${[max: max]}">TODO</g:link></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="emailTemplate" params="${[max: max]}">EMAIL TEMPLATE</g:link></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="htmlTemplate" params="${[max: max]}">HTML TEMPLATE</g:link></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="album" params="${[max: max]}">ALBUM</g:link></td>
        <td><g:link controller="photo" params="${[max: max]}">PHOTO</g:link></td>
    </tr>

    <tr>
        <td><g:link controller="task" params="${[max: max]}">TASK</g:link></td>
        <td><g:link controller="taskPerson" params="${[max: max]}">TASK-PERSON</g:link></td>
        <td><g:link controller="schedule" params="${[max: max]}">SCHEDULE</g:link></td>
    </tr>
    <tr>
        <td></td>
        <td><g:link controller="subtask" params="${[max: max]}">SUBTASK</g:link></td>
    </tr>
</table>
