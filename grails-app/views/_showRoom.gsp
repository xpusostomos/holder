<p>
    <g:if test="${room.level == holder.RoomLevelEnum.ROOM}">
    ${room.name},
    </g:if>
   ${room.notes}
</p>
<f:display bean="room" property="album" width="500"/>
<g:if test="${room.parent}">
    <g:render template="/showRoom" model="[room: room.parent]"/>
</g:if>
