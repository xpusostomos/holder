<%@ page import="java.time.LocalDateTime; java.time.LocalDate" %>
<h3 style="margin-top: 50px;">Schedule for ${task}</h3>
<table>
    <tr><th>Date</th><th>Person</th></tr>
    <g:each in="${task.schedules.findAll{!it.complete}}" var="s">
        <tr>
            <td style="color: ${s.due.isBefore(java.time.LocalDateTime.now()) ? 'red' : 'black'};"><g:formatDate date="${s.due}"/></td>
            <td>${s.person}</td>
        </tr>
    </g:each>
</table>
<g:render template="/guest/calendar"
          model="[events: task.schedules.collect{[value: it.task.name + ' (' + it.person.name + ')' + (it.complete ? ' (done)' : ''), key: it.due.toLocalDate(), color: (!it.complete && it.due.isBefore(java.time.LocalDateTime.now()) ? 'red' : 'black')]}]"/>
