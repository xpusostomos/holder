<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'emailTemplate.label', default: 'EmailTemplate')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>

<h2>Great! You've completed the task.</h2>

<g:render template="futureSchedule" model="[task: schedule.task]"/>

<sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN'>
    <g:link controller="task" action="show" id="${schedule.task.id}">GOTO TASK</g:link>
</sec:ifAnyGranted>
</body>
</html>
