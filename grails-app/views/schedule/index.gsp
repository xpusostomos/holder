<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'schedule.label', default: 'Schedule')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-schedule" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-schedule" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${scheduleList}" except="[]"/>

            <div class="pagination">
                <g:paginate total="${scheduleCount ?: 0}" />
            </div>
        </div>
    <g:uploadForm action="update" method="POST">
        <fieldset class="buttons">
            <g:actionSubmit type="submit" controller="admin" action="generateSchedule" value="${message(code: 'default.button.generateSchedule.label', default: 'Generate Schedule')}" />
            <g:actionSubmit type="submit" controller="admin" action="generateReminder" value="${message(code: 'default.button.generateReminder.label', default: 'Generate Reminder')}" />
        </fieldset>
    </g:uploadForm>
    </body>
</html>
