<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'emailTemplate.label', default: 'EmailTemplate')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<h1>Complete Task</h1>
<h2>${schedule.task.name} due on <g:formatDate date="${schedule.due}"/></h2>
${schedule.task.description}
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<g:form action="complete" id="${schedule.id}">
    <table  class="table table-responsive table-striped">
        <g:each in="${schedule.task.subtasks}" var="subtask" status="i">
            <tr>
                <td class="min">${subtask.name}</td>
                <td class="min"><g:checkBox name="subtask[${i}]" checked="${errors && !errors[i]}"/></td>
                <td><g:if test="${errors && errors[i]}"><div class="alert alert-danger">Task not complete</div></g:if></td>
            </tr>
        </g:each>
    </table>
    <g:submitButton name="completeButton" class="btn btn-primary" value="${message(code: 'default.button.save.label', default: 'Complete')}" />
</g:form>
<g:render template="futureSchedule" model="[task: schedule.task]"/>
</body>
</html>
