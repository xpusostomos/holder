<%@ page import="java.time.LocalDate" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'account.label', default: 'Account')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-account" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-account" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display prefix='account.' bean="account" order="id,name,parent,offset,depreciation,hasPersonalUse"/>
            <g:form resource="${this.account}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.account}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    <table>
        <thead>
        <tr><th>Date</th><th>Description</th><th>Property</th><th>Ref</th><th style="text-align:right">Debit</th><th style="text-align:right">Credit</th><th>Balance</th></tr>
        </thead>
        <tbody>
        <g:set var="balance" value="${BigDecimal.ZERO}"/>
        <g:set var="opening" value="${ java.time.LocalDate.of(1, 1, 1)}"/>
        <g:each in="${(account.debitAccountEntries + account.creditAccountEntries).sort {a, b -> a.date <=> b.date ?: a.id <=> b.id }}" var="acc">
            <g:if test="${opening != holder.Account.openingDate(acc.date)}">
                <tr><th colspan="7" style="text-align:center">${holder.Account.openingDate(acc.date)} - ${holder.Account.closingDate(acc.date)}</th></tr>
            </g:if>
            <g:set var="balance" value="${opening == holder.Account.openingDate(acc.date) ? balance.add(acc.signedAmount(account)) : acc.signedAmount(account)}"/>
            <g:set var="opening" value="${holder.Account.openingDate(acc.date)}"/>
%{--            <g:set var="balance" value="${balance.add(acc.signedAmount(account))}"/>--}%
            <tr>
                <td><g:link controller="accountEntry" action="show" id="${acc.id}">${acc.date}</g:link></td>
                <td>${acc.description}</td>
                <td>${acc.premises.name}</td>
                <td>${acc.debitAccount == account ? acc.creditAccount : acc.debitAccount}</td>
                <td style="text-align:right">${acc.debitAccount == account ? acc.amount : ''}</td>
                <td style="text-align:right">${acc.creditAccount == account ? acc.amount : ''}</td>
                <td style="text-align:right">${balance}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
    </body>
</html>
