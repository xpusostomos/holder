<%@ page import="holder.User; holder.Person; holder.LoginCommand" %>
<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
        <g:layoutTitle default="Grails"/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <asset:stylesheet src="application.css"/>


%{--    <asset:javascript src="/webjars/jquery/3.5.1/jquery.min.js"/>--}%
%{--    <asset:javascript src="/webjars/bootstrap/4.5.3/js/bootstrap.bundle.min.js"/>--}%
%{--    <asset:stylesheet src="/webjars/bootstrap/4.5.3/css/bootstrap.min.css"/>--}%

%{--    <asset:javascript src="/webjars/jquery-datetimepicker/2.5.21/build/jquery.datetimepicker.full.min.js"/>--}%
%{--    <asset:stylesheet src="/webjars/jquery-datetimepicker/2.5.21/jquery.datetimepicker.css"/>--}%

%{--    <asset:javascript src="/webjars/php-date-formatter/1.3.6/js/php-date-formatter.min.js"/>--}%

    <g:layoutHead/>
</head>

<body>
<g:set var="springSecurityService" bean="springSecurityService"/>
<div class="container-fluid">
    <a href="/"><asset:image style="padding-top: 10px;" src="house/front-of-house.jpg" alt="HOME" width="100"/></a>
    Turner Rooms for Rent
    <div style="float: right; vertical-align: top;" align="right">
    <sec:ifLoggedIn>
        <sec:ifAnyGranted roles='ROLE_ADMIN,ROLE_VICE_ADMIN,ROLE_USER'>
            <g:link controller="admin">ADMIN</g:link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </sec:ifAnyGranted>
        <sec:ifAllGranted roles='ROLE_GUEST'>
%{--            <g:link controller="guest" action="home" id="${Person.findByUser(User.get(springSecurityService.authentication.principal.id)).id}">MY HOME PAGE</g:link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--}%
            <g:link controller="guest" action="home" id="${Person.findByUser(springSecurityService.currentUser)?.id}">HOME PAGE</g:link>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </sec:ifAllGranted>
        Logged in as <sec:username/>
        <fieldset class="buttons" style="display:inline-block;vertical-align: top;">
            <g:submitButton name="Logout" form="logoutForm"/>
        </fieldset>
    </sec:ifLoggedIn>
    <sec:ifSwitched>
        <fieldset class="buttons" style="display:inline-block;vertical-align: top;">
            <g:submitButton name="resume" value="Resume as ${grails.plugin.springsecurity.SpringSecurityUtils.switchedUserOriginalUsername}" form="impersonateLogoutForm" method="POST"/>
        </fieldset>
    </sec:ifSwitched>
    <sec:ifAllGranted roles='ROLE_SWITCH_USER'>
        <g:field type='text' name='username' list="usernames" form="impersonateLoginForm"/>
        <datalist id="usernames">
            <g:each in="${holder.Role.findByAuthority('ROLE_USER').users}" var="user">
                <option value="${user.username}">${user.name}</option>
            </g:each>
            <g:each in="${holder.Person.currentGuests}" var="person">
                <g:if test="${person.user}">
                <option value="${person.user.username}">${person.name}</option>
                </g:if>
            </g:each>
        </datalist>
        <fieldset class="buttons" style="display:inline-block;vertical-align: top;">
            <g:submitButton name="switch" value='Switch' form="impersonateLoginForm"/>
        </fieldset>
    </sec:ifAllGranted>
    <sec:ifNotLoggedIn>
        <label for="username">Email</label>
        <g:field type="text" size="32" length="32"
                             name="username" autocomplete="username" form="loginForm"/>
        <label for="password">Password</label>
        <g:field type="password" size="32" length="32"
                 name="password" autocomplete="current-password" form="loginForm"/>
        <fieldset class="buttons" style="display:inline-block;vertical-align: top;">
            <g:submitButton name="loginButton" value="Login" form="loginForm"/>
        </fieldset>
    </sec:ifNotLoggedIn>
</div>
%{--<g:if test="${flash.errors}">--}%
%{--    <g:each in="${flash.errors}">--}%
%{--        <div class="alert alert-danger">${it}</div>--}%
%{--    </g:each>--}%
%{--</g:if>--}%
%{--<g:if test="${flash.messages}">--}%
%{--    <g:each in="${flash.messages}">--}%
%{--        <div class="alert alert-success">${it}</div>--}%
%{--    </g:each>--}%
%{--</g:if>--}%

%{--<nav class="navbar navbar-expand-lg navbar-dark navbar-static-top" role="navigation">--}%
%{--    <a class="navbar-brand" href="/#"><asset:image src="grails.svg" alt="Grails Logo"/></a>--}%
%{--    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">--}%
%{--        <span class="navbar-toggler-icon"></span>--}%
%{--    </button>--}%

%{--    <div class="collapse navbar-collapse" aria-expanded="false" style="height: 0.8px;" id="navbarContent">--}%
%{--        <ul class="nav navbar-nav ml-auto">--}%
%{--            <g:pageProperty name="page.nav"/>--}%
%{--        </ul>--}%
%{--    </div>--}%

%{--</nav>--}%


%{--<div class="footer row" role="contentinfo">--}%
%{--    <div class="col">--}%
%{--        <a href="http://guides.grails.org" target="_blank">--}%
%{--            <asset:image src="advancedgrails.svg" alt="Grails Guides" class="float-left"/>--}%
%{--        </a>--}%
%{--        <strong class="centered"><a href="http://guides.grails.org" target="_blank">Grails Guides</a></strong>--}%
%{--        <p>Building your first Grails app? Looking to add security, or create a Single-Page-App? Check out the <a href="http://guides.grails.org" target="_blank">Grails Guides</a> for step-by-step tutorials.</p>--}%

%{--    </div>--}%
%{--    <div class="col">--}%
%{--        <a href="http://docs.grails.org" target="_blank">--}%
%{--            <asset:image src="documentation.svg" alt="Grails Documentation" class="float-left"/>--}%
%{--        </a>--}%
%{--        <strong class="centered"><a href="http://docs.grails.org" target="_blank">Documentation</a></strong>--}%
%{--        <p>Ready to dig in? You can find in-depth documentation for all the features of Grails in the <a href="http://docs.grails.org" target="_blank">User Guide</a>.</p>--}%

%{--    </div>--}%

%{--    <div class="col">--}%
%{--        <a href="https://grails-slack.cfapps.io" target="_blank">--}%
%{--            <asset:image src="slack.svg" alt="Grails Slack" class="float-left"/>--}%
%{--        </a>--}%
%{--        <strong class="centered"><a href="https://grails-slack.cfapps.io" target="_blank">Join the Community</a></strong>--}%
%{--        <p>Get feedback and share your experience with other Grails developers in the community <a href="https://grails-slack.cfapps.io" target="_blank">Slack channel</a>.</p>--}%
%{--    </div>--}%
%{--</div>--}%


<div id="spinner" class="spinner" style="display:none;">
    <g:message code="spinner.alt" default="Loading&hellip;"/>
</div>

<asset:javascript src="application.js"/>
<g:layoutBody/>
<g:if env="development"><div class="card"><div class="card-header bg-danger text-white">DEVELOPMENT</div></div></g:if>
<g:if env="test"><div class="card"><div class="card-header bg-warning text-white">TEST</div></div></g:if>
</div>
<g:form controller="logout" name="logoutForm" method="POST">
</g:form>
<g:form controller="logout" action='impersonate' name="impersonateLogoutForm" method="POST">
</g:form>
<g:form controller="login" action="authenticate" name="loginForm" method="POST">
</g:form>
<g:form controller="login" action='impersonate' name="impersonateLoginForm" method="POST">
</g:form>

<div style="text-align: right;font-size: 8px">gv:<g:meta name="info.app.grailsVersion"/> bd:<g:meta name="info.app.build.date"/></div>
</body>
</html>
