<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<hr>
<g:render template="/showRoom" model="[room: room]"/>
</body>
</html>
