<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 7/12/2020
  Time: 9:48 am
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edit Booking</title>
</head>

<body>
<h1>Booking ${booking.id}</h1>
<g:uploadForm action="edit">
    <g:hiddenField name="booking.id" value="${booking.id}"/>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.firstName">First Name</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('firstName') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.firstName" value="${booking.firstName}"/>
            <div class="invalid-feedback"><g:fieldError field="firstName" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.lastName">Last Name</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('lastName') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.lastName" value="${booking.lastName}"/>
            <div class="invalid-feedback"><g:fieldError field="lastName" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.email">Email</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('email') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.email" value="${booking.email}"/>
            <div class="invalid-feedback"><g:fieldError field="email" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.phone">Phone</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('phone') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.phone" value="${booking.phone}"/>
            <div class="invalid-feedback"><g:fieldError field="phone" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.checkIn">Estimated move in date</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('checkIn') ? 'is-invalid' : 'is-valid'}" type="date" name="booking.checkIn" value="${formatDate(format:'yyyy-MM-dd', date:booking.checkIn)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkIn" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.checkOut">Estimated move out date (guess is fine)</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('checkOut') ? 'is-invalid' : 'is-valid'}" type="to" name="booking.checkOut" value="${formatDate(format:'yyyy-MM-dd', date:booking.checkOut)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkOut" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="booking.employment">If student, enter course name, otherwise list employment or occupation</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('employment') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.employment" value="${booking.employment}"/>
            <div class="invalid-feedback"><g:fieldError field="employment" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-4">
            <label for="booking.male">Sex</label>
            <g:select class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('male') ? 'is-invalid' : 'is-valid'}" name="booking.male"  from="['', 'Male', 'Female']" keys="['', true, false]" value="${booking.male}" />
            <div class="invalid-feedback"><g:fieldError field="male" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-group">
        <label for="booking.dateInspection">Suggest date/time to inspect room</label>
        <g:datePicker class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('dateInspection') ? 'is-invalid' : 'is-valid'}" precision="minute" relativeYears="${0..1}" name="booking.dateInspection" value="${booking.dateInspection}"/>
        <div class="invalid-feedback"><g:fieldError field="dateInspection" bean="${booking}"/></div>
    </div>
    <div class="form-group">
        <label for="booking.applyNotes">Applicant's Comments</label>
        <g:textArea class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('applyNotes') ? 'is-invalid' : 'is-valid'}" name="booking.applyNotes" rows="5" cols="100">
${booking.applyNotes}
        </g:textArea>
        <div class="invalid-feedback"><g:fieldError field="applyNotes" bean="${booking}"/></div>
    </div>
    <div class="form-group">
        <label for="booking.notes">Admin Comments</label>
        <g:textArea class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('notes') ? 'is-invalid' : 'is-valid'}" name="booking.notes" rows="5" cols="100">
${booking.notes}
        </g:textArea>
        <div class="invalid-feedback"><g:fieldError field="notes" bean="${booking}"/></div>
    </div>
%{--    <div class="form-group">--}%
%{--        <label for="booking.closed">Closed</label>--}%
%{--        <g:checkBox class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('closed') ? 'is-invalid' : 'is-valid'}" name="booking.closed" value="${booking.closed}"/>--}%
%{--        <div class="invalid-feedback"><g:fieldError field="closed" bean="${booking}"/></div>--}%
%{--    </div>--}%
    <div class="form-group">
        <g:each in="${booking?.album?.photos}" var="it" status="i">
            <g:link controller="booking" action="viewScan" id="${it.id}">File ${i}: ${it.name}</g:link>
        </g:each>
    </div>
    <div class="form-group">
        <label for="booking.album.photos[0].data">Upload any documents or references you would like to submit</label>
        <g:field name="booking.album.photos[0].data" type="file"/>
        <g:field name="booking.album.photos[1].data" type="file"/>
        <g:field name="booking.album.photos[2].data" type="file"/>
    </div>
    <g:actionSubmit class="btn btn-primary" action="save" value="Save"/>
    <g:actionSubmit class="btn btn-primary" action="approve" value="Approve Application"/>
    <g:actionSubmit class="btn btn-primary" action="reject" value="Reject Application"/>
</g:uploadForm>
