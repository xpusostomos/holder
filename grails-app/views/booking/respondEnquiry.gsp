<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'booking.label', default: 'Booking')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<a href="#edit-booking" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="edit-booking" class="content scaffold-edit" role="main">
    <h1><g:message code="default.respond.label" default="Respond to Booking" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.booking}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.booking}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:uploadForm method="PUT">  <!-- ONLY CHANGE FROM STD is uploadForm -->
        <g:hiddenField name="version" value="${this.booking?.version}" />
        <g:hiddenField name="booking.id" value="${booking.id}"/>
        <g:hiddenField name="email.to" value="${email.to}"/>
        <fieldset class="form">
            <f:field prefix="email." bean="email" property="subject"/>
            <f:field prefix="email." bean="email" property="body"/>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="email" action="sendResponse" value="${message(code: 'default.button.preview.label', default: 'Send Response')}"/>
        </fieldset>
    </g:uploadForm>
</div>
</body>
</html>
