<table class="table table-striped table-bordered">
    <thead class="thead-light">
    <tr><th colspan="5">${title}</th></tr>
    <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Inspection</th>
        <th>Move In</th>
        <th>Move Out</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${bookingList}">
        <tr>
            <td><g:link controller="booking" action="show" id="${it.id}">${it.firstName} ${it.lastName}</g:link></td>
            <td><a href="mailto:${it.email}" target="_blank">${it.email}</a></td>
            <td>${it.dateInspection}</td>
            <td>${it.checkIn}</td>
            <td>${it.checkOut}</td>
        </tr>
    </g:each>
    </tbody>
</table>
