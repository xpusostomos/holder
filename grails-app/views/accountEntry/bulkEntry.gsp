<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 28/11/21
  Time: 11:46 am
--%>

<%@ page import="holder.RoomLevelEnum; holder.Room; holder.Account" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'bulkEntry.label', default: 'Bulk Entry')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>
<body>
<g:if test="${flash.message}">
    <div class="message" role="status">${flash.message}</div>
</g:if>
<g:hasErrors bean="${this.bulk}">
    <ul class="errors" role="alert">
        <g:eachError bean="${this.bulk}" var="error">
            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
        </g:eachError>
    </ul>
</g:hasErrors>
<g:form method="POST">
    <table>
        <tr>
            <td style="text-align: right;">Type</td><td><g:select name="type" from="${holder.BulkEntryType.values()}" value="${bulk.type}"/></td>
        </tr>
        <tr>
            <td style="text-align: right;">Single Account</td><td><g:select name="singleAccount" from="${bulk.legalSingleAccounts}" noSelection="${['': '']}" value="${bulk.singleAccount?.id}" optionKey="id"/></td>
        </tr>
        <tr>
            <td style="text-align: right;">Debit Single?</td><td><g:checkBox name="debitSingle" value="${bulk.debitSingle}"/></td>
        </tr>
        <tr>
            <td style="text-align: right;">Period Begin</td><td><g:field type="date" name="periodBegin" value="${bulk.periodBegin}"/></td>
        </tr>
        <tr>
            <td style="text-align: right;">Period End</td><td><g:field type="date" name="periodEnd" value="${bulk.periodEnd}"/></td>
        </tr>
    </table>
    <table>
        <thead>
        <tr>
            <th>Date</th><th>Description</th><th>Account</th><th>Premises</th><th>Amount</th><th>Provisional?</th><th>Useful life</th><th>Depreciation Method</th><th>Notes</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${bulk.entries}" var="ent" status="i">
            <tr>
                <td><g:field type="date" name="entries[${i}].date" value="${ent.date}"/></td>
                <td><g:field type="text" name="entries[${i}].description" value="${ent.description}" size="40"/></td>
                <td><g:select name="entries[${i}].account" from="${bulk.legalMultipleAccounts}" noSelection="${['': '']}" value="${ent.account?.id}" optionKey="id"/></td>
                <td><g:select name="entries[${i}].premises" from="${Room.findAllByLevel(holder.RoomLevelEnum.PREMISES, [sort: 'name'])}" noSelection="${['': '']}" value="${ent.premises?.id}" optionKey="id"/></td>
                <td><g:field type="number" name="entries[${i}].amount" value="${ent.amount}" min="0" step=".01"/></td>
                <td><g:checkBox name="entries[${i}].provisional" value="${ent.provisional}"/></td>
                <td><g:field type="number" name="entries[${i}].usefulLife" value="${ent.usefulLife}" min="0" max="100" step=".0001"/></td>
                <td><g:select name="entries[${i}].depreciationMethod" from="${holder.DepreciationMethod.values()}" noSelection="${['': '']}" value="${ent.depreciationMethod}"/></td>
                <td><g:field type="text" name="entries[${i}].notes" value="${ent.notes}" size="80"/></td>
%{--                <td style="color: red;">${bulk.entries[i].errors}</td>--}%
            </tr>
            <g:if test="${ent.hasEntry()}">
                <g:hasErrors bean="${ent}">
                    <tr>
                        <td colspan="6">
                            <ul class="errors" role="alert">
                                <g:eachError bean="${ent}" var="error">
                                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                                </g:eachError>
                            </ul>
                        </td>
                    </tr>
                </g:hasErrors>
            </g:if>
            <g:if test="${ent.possibleDups.size() > 0}">
                <tr>
                    <td colspan="6">
                        <ul class="warnings" role="alert">
                            <g:each in="${ent.possibleDups}" var="dup">
                                <li data-field-id="foo"><g:link action="show" id="${dup.id}">${dup.date} ${dup.description} (${dup.premises.getName()}) ${dup.amount} ${dup.notes}</g:link></li>
                            </g:each>
                        </ul>
                    </td>
                </tr>
            </g:if>
        </g:each>
        </tbody>
    </table>
    <g:actionSubmit value="Preview" action="bulkEntry"/>
    <g:actionSubmit value="Add 10 Entries" action="bulkAddTen"/>
    <g:actionSubmit value="Save" action="bulkSave"/>
</g:form>
</body>
</html>
