<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'accountEntry.label', default: 'Accept Payment')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="acceptPayment" class="content scaffold-create" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.accountEntry}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.accountEntry}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.accountEntry}" method="PUT" action="saveDebt">
    <g:hiddenField name="person.id" value="${accountEntry.person.id}"/>
    <g:hiddenField name="premises.id" value="${accountEntry.premises.id}"/>
    <p>
    Name: ${accountEntry.person.name}
    </p>
    <p>
    Rooms:
    <g:each in="${accountEntry.person.contracts.findAll{it.active}}">
        ${it.room.name}
    </g:each>
    </p>
    <p>
        Amount Owing: ${accountEntry.person.amountOwing}
    </p>
    <g:actionSubmit class="btn btn-primary" action="saveDebt" value="Submit Payment"/>
    <g:render template="/person/ledger" model="[title: 'ACCOUNT ENTRIES', person: accountEntry.person, openingBalance: 0.0, accountEntries: accountEntry.person.accountEntries]"/>
        <fieldset class="form">
            <f:field bean="accountEntry" property="date"/>
            <f:field bean="accountEntry" property="periodBegin"/>
            <f:field bean="accountEntry" property="periodEnd"/>
            <f:field bean="accountEntry" property="type"/>
            <f:field bean="accountEntry" property="description"/>
            <f:field bean="accountEntry" property="amount"/>
            <f:field bean="accountEntry" property="contract"/>
        </fieldset>
        <fieldset class="buttons">
            <input class="saveDebt" type="submit" value="${message(code: 'default.button.recordDebt.label', default: 'Save Debt Entry')}" />
        </fieldset>
    </g:form>
%{--            <f:field></f:field>--}%
</div>
</body>
</html>
