<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'accountEntry.label', default: 'Keep Bond')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="acceptPayment" class="content scaffold-create" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.accountEntry}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.accountEntry}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.accountEntry}" method="PUT" action="saveKeepBond">
    <g:hiddenField name="person.id" value="${accountEntry.person.id}"/>
    <g:hiddenField name="room.id" value="${accountEntry.room.id}"/>
    <p>
    Name: ${accountEntry.person.name}
    </p>
    <p>
    Rooms:
    <g:each in="${accountEntry.person.contracts.findAll{it.active}}">
        ${it.room.name}
    </g:each>
    </p>
    <p>
        Amount Owing: ${accountEntry.person.amountOwing}
    </p>
%{--    <div class="form-row">--}%
%{--        <div class="form-group col-md-6">--}%
%{--            <label for="accountEntry.date">Date</label>--}%
%{--            <g:field class="form-control ${!accountEntry.hasErrors() ? '' : accountEntry.errors.getFieldError('date') ? 'is-invalid' : 'is-valid'}" type="date" name="accountEntry.date" value="${formatDate(format:'yyyy-MM-dd', date:accountEntry.date)}"/>--}%
%{--            <div class="invalid-feedback"><g:fieldError field="date" bean="${accountEntry}"/></div>--}%
%{--        </div>--}%
%{--        <div class="form-group col-md-6">--}%
%{--            <label for="accountEntry.creditAmount">Amount</label>--}%
%{--            <g:field class="form-control ${!accountEntry.hasErrors() ? '' : accountEntry.errors.getFieldError('creditAmount') ? 'is-invalid' : 'is-valid'}" type="text" name="accountEntry.creditAmount" value="${accountEntry.creditAmount}"/>--}%
%{--            <div class="invalid-feedback"><g:fieldError field="creditAmount" bean="${accountEntry}"/></div>--}%
%{--        </div>--}%
%{--    </div>--}%
    <g:actionSubmit class="btn btn-primary" action="saveKeepBond" value="Keep Bond"/>
    <g:render template="/person/ledger" model="[title: 'ACCOUNT ENTRIES', person: accountEntry.person, openingBalance: 0.0, accountEntries: accountEntry.person.accountEntries]"/>






        <fieldset class="form">
            <f:field bean="accountEntry" property="date"/>
            <f:field bean="accountEntry" property="amount"/>

%{--            <f:all bean="accountEntry" properties="date,amount,method" />--}%
        </fieldset>
        <fieldset class="buttons">
            <input class="keepBond" type="submit" value="${message(code: 'default.button.keepBond.label', default: 'Keep Bond')}" />
        </fieldset>
    </g:form>
%{--            <f:field></f:field>--}%
</div>
</body>
</html>
