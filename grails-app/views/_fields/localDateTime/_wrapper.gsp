<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:field type="text" name="${property}" value="${holder.DateTimeValueConverter.format(value)}" required="${required}"/>
    </g:if>
    <g:else>
        <g:field type="text" name="${property}" value="${holder.DateTimeValueConverter.format(value)}"/>
    </g:else>
</div>
<script type="text/javascript">
// $(function() {
$('[name$=${property}]').datetimepicker({format: "d-M-Y H:i"});
// });
</script>
