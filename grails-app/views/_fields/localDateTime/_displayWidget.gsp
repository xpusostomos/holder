%{--<span class="datetime">${holder.DateTimeValueConverter.format(value)}</span>--}%
<g:if test="${value == java.time.LocalDateTime.MIN}">
    NOW
</g:if>
<g:elseif test="${value == java.time.LocalDateTime.MAX}">
    UNKNOWN
</g:elseif>
<g:else>
${holder.DateTimeValueConverter.format(value)}
</g:else>
