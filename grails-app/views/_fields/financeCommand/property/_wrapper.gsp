<%@ page import="holder.Room; holder.RoomLevelEnum; holder.Account" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:select name="${property}" from="${holder.Room.findAllByLevel(holder.RoomLevelEnum.PREMISES, [sort: 'name'])}" noSelection="${['': '']}" value="${value?.id}" required="${required}" optionKey="id"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${Room.findAllByLevel(holder.RoomLevelEnum.PREMISES, [sort: 'name'])}" noSelection="${['': '']}" value="${value?.id}" optionKey="id"/>
    </g:else>
</div>
