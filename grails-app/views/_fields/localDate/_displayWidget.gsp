%{--<span class="date">${value}</span>--}%
%{--<input type="date" value="${value}" readonly/>--}%
%{--<g:formatDate date="${value}" format="dd/MM/yyyy"/>--}%
%{--${holder.DateTimeValueConverter.format(value)}--}%
<g:if test="${value == java.time.LocalDate.MIN}">
    NOW
</g:if>
<g:elseif test="${value == java.time.LocalDate.MAX}">
    UNKNOWN
</g:elseif>
<g:else>
${holder.DateValueConverter.format(value)}
</g:else>
