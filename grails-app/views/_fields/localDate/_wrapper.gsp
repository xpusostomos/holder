%{--<div class="fieldcontain${required?' required':''}">--}%
%{--    <label for='${property}'>${label}--}%
%{--        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>--}%
%{--    </label>--}%
%{--    <g:if test="${required}">--}%
%{--        <g:field type="date" name="${property}" value="${value}" id="${property}" required="true"/>--}%
%{--    </g:if>--}%
%{--    <g:else>--}%
%{--        <g:field type="date" name="${property}" value="${value}" id="${property}"/>--}%
%{--    </g:else>--}%
%{--</div>--}%

<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:field type="text" name="${property}" value="${holder.DateValueConverter.format(value)}" required="${required}"/>
    </g:if>
    <g:else>
        <g:field type="text" name="${property}" value="${holder.DateValueConverter.format(value)}"/>
    </g:else>
</div>
<script type="text/javascript">
$(function() {
$('[name$=${property}]').datetimepicker({format: "d-M-Y", timepicker:false});
});
</script>
