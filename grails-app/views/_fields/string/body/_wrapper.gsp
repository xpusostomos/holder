<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:textArea name="${property}" required="true" rows="40" cols="160">${value}</g:textArea>
    </g:if>
    <g:else>
        <g:textArea name="${property}" rows="40" cols="160">${value}</g:textArea>
    </g:else>
</div>

