<div class="fieldcontain${required?' required':''}" id="${property + '-group'}">
    <label for='${property + '.photos[0].data'}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
%{--    <g:if test="${!value}">--}%
%{--        <g:if test="${required}">--}%
%{--            <g:field type="file" name="${property + '-predata'}" onchange="removeImage('${property}');" required=""/>--}%
%{--        </g:if>--}%
%{--        <g:else>--}%
%{--            <g:field type="file" name="${property + '-predata'}" onchange="removeImage('${property}');"/>--}%
%{--        </g:else>--}%
%{--    </g:if>--}%
%{--    <g:else>--}%
     <g:set var="ind" value="0"/>
<g:if test="${value}">
    <g:each in="${value.photos}" var="photo" status="i">
        <g:set var="ind" value="${i+1}"/>
        <g:hiddenField name="${property + '.photos[' + i + '].id'}" value="${value.photos[i].getProperty('id')}"/>
        <img id="${property + '.photos[' + i + ']-img'}" src="${g.createLink(controller: 'booking', action: 'viewScan', id: value.photos[i].getProperty('id'))}" width="80"/>
        <img id="${property + '.photos[' + i + ']-delete'}" src="/assets/skin/database_delete.png" onclick="removeImageAlbum('${property + '.photos[' + i + ']'}');">
        <g:field type="file" name="${property + '.photos[' + i + ']-predata'}" hidden="true"/>
    </g:each>
</g:if>
</div>
<script type="text/javascript">
$(function() {
    addUploadButton('${property}' + '-group', ${ind}, '${property}' + '.photos[{index}].data');
});
</script>
