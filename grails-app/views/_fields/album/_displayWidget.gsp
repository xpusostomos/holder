    <g:if test="${value}">
        <g:each in="${value.photos}" var="photo" status="i">
%{--            <img src="${g.createLink(controller: 'booking', action: 'viewScan', id: value.photos[i].getProperty('id'))}" width="80"/>--}%
            <g:render template="/_fields/photo/displayWidget" model="[value: value.photos[i], width: width]"/>
        </g:each>
    </g:if>
