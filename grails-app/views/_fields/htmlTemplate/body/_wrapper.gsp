<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:textArea class="largetext" name="${property}" required="true">${value}</g:textArea>
    </g:if>
    <g:else>
        <g:textArea class="largetext" name="${property}">${value}</g:textArea>
    </g:else>
</div>

