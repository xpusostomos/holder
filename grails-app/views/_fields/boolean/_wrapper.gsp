<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${grailsApplication.isDomainClass(bean.getClass()) && bean.class.constrainedProperties[property]?.appliedConstraints?.find { it instanceof org.grails.datastore.gorm.validation.constraints.NullableConstraint }?.nullable}">
        <g:if test="${required}">
            <g:select name="${property}" from="${[[key: true, value: 'YES'], [key: false, value: 'NO']]}" noSelection="${['': '']}" optionKey="key" optionValue="value" value="${value}" required="true"/>
        </g:if>
        <g:else>
            <g:select name="${property}" from="${[[key: true, value: 'YES'], [key: false, value: 'NO']]}" noSelection="${['': '']}" optionKey="key" optionValue="value" value="${value}"/>
        </g:else>
    </g:if>
    <g:else>
        <g:checkBox name="${property}" value="${value}"/>
    </g:else>
</div>


