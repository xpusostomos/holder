<%@ page import="holder.Account" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:select name="${property}" from="${Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(60250, 60300, [sort: 'id'])}" noSelection="${['': '']}" value="${value?.id}" required="${required}" optionKey="id"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${Account.findAllByParentIsNotNullAndIdGreaterThanEqualsAndIdLessThan(60250, 60300, [sort: 'id'])}" noSelection="${['': '']}" value="${value?.id}" optionKey="id"/>
    </g:else>
</div>
