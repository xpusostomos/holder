<%@ page import="holder.Room; holder.RoomLevelEnum; holder.Account" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:set var="springSecurityService" bean="springSecurityService"/>
    <g:set var="user" value="${springSecurityService.currentUser}"/>
    <g:set var="rooms" value="${user.rooms}"/>
    <sec:ifAllGranted roles='ROLE_ADMIN'>
        <g:set var="rooms" value="${holder.Room.findAllByLevel(holder.RoomLevelEnum.PREMISES, [sort: 'name'])}"/>
    </sec:ifAllGranted>
    <g:if test="${required}">
       <g:select name="${property}" from="${rooms}" noSelection="${['': '']}" value="${value?.id}" required="${required}" optionKey="id"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${rooms}" noSelection="${['': '']}" value="${value?.id}" optionKey="id"/>
    </g:else>
</div>
