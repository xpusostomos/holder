<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
%{--    <g:if test="${!value}">--}%
        <g:if test="${required}">
            <g:field type="file" name="${property + '-predata'}" onchange="removeImage('${property}');" required="true"/>
        </g:if>
        <g:else>
            <g:field type="file" name="${property + '-predata'}" onchange="removeImage('${property}');"/>
        </g:else>
%{--    </g:if>--}%
%{--    <g:else>--}%
    <g:if test="${value}">
        <g:hiddenField name="${property + '.id'}" value="${value.getProperty('id')}"/>
        <img id="${property + '.img'}" src="${g.createLink(controller: 'booking', action: 'viewScan', id: value.getProperty('id'))}" width="80"/>
        <img src="/assets/skin/database_delete.png" onclick="removeImage('${property}');">
    </g:if>
</div>

