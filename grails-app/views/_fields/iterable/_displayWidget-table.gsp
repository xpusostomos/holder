<g:each in="${value}" var="it" status="i">
    <g:if test="${i != 0}">, </g:if>
%{--    ${it.getClass().getName()} ... ${grailsApplication.getDomainClass(it.getClass().getName().replaceFirst('\\$.*', '')).name}--}%
    <g:link controller="${grailsApplication.getDomainClass(it.getClass().getName().replaceFirst('\\$.*', '')).name}" action="show" id="${ it.id }">${ it.toString() }</g:link>
</g:each>
