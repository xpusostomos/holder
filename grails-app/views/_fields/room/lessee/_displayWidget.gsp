<g:if test="${value}">
    <g:link controller="person" action="show" id="${value.id}">${value.name}</g:link>
</g:if>
