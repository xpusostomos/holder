<g:if test="${value}">
    <g:link controller="room" action="show" id="${value.id}">${value.name}</g:link>
</g:if>
