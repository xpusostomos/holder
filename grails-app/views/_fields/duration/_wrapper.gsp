<%@ page import="holder.DurationValueConverter; holder.UserRoom; holder.RoomLevelEnum; holder.Room" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
%{--    <g:if test="${required}">--}%
        <g:field type="text" name="${property}" value="${DurationValueConverter.format(value)}" required="${required}"/>
%{--    </g:if>--}%
%{--    <g:else>--}%
%{--        <g:field type="text" name="${property}" value="${value.toString()}"/>--}%
%{--    </g:else>--}%
</div>
