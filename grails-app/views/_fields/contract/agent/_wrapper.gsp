<%@ page import="holder.PersonType; holder.Person" %>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:select name="${property}" from="${Person.findAllByType(holder.PersonType.AGENT)}" noSelection="${['': '']}" value="${value?.id}" required="${required}" optionKey="id"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${Person.findAllByType(holder.PersonType.AGENT)}" noSelection="${['': '']}" value="${value?.id}" optionKey="id"/>
    </g:else>
</div>
