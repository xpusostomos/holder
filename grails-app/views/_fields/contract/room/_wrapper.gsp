<%@ page import="holder.UserRoom; holder.RoomLevelEnum; holder.Room" %>
<g:set var="springSecurityService" bean="springSecurityService"/>
<sec:ifAllGranted roles="ROLE_ADMIN">
    <g:set var="rooms" value="${Room.findAllByLevel(RoomLevelEnum.PREMISES).collectMany { it.allRooms }}"/>
</sec:ifAllGranted>
<sec:ifNotGranted roles="ROLE_ADMIN">
    <g:set var="rooms" value="${holder.UserRoom.findAllByUser(springSecurityService.currentUser).collect {it.room}}"/>
</sec:ifNotGranted>
<div class="fieldcontain${required?' required':''}">
    <label for='${property}'>${label}
        <g:if test="${required}"><span class='required-indicator'>*</span></g:if>
    </label>
    <g:if test="${required}">
        <g:select name="${property}" from="${rooms}" noSelection="${['': '']}" value="${value?.id}" required="${required}" optionKey="id"/>
    </g:if>
    <g:else>
        <g:select name="${property}" from="${rooms}" noSelection="${['': '']}" value="${value?.id}" optionKey="id"/>
    </g:else>
</div>
