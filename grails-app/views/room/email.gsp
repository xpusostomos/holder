<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'email.label', default: 'Send Email')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<a href="#edit-email" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="edit-email" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]" /> to ${room.name}: ${room.description}</h1>
    <g:uploadForm action="send" id="${room?.id}" method="PUT">
    <table>
        <tr><th>Room:</th><th>Sending To:</th></tr>
        <g:each in="${email.candidates}" var="c" status="i">
            <tr>
                <g:hiddenField name="candidates[${i}].person.id" value="${c.person.id}"/>
                <td>${c.person.rooms.findAll { it.hasParent(room) }}</td>
                <td>${c.person}</td>
                <td>${c.person.user.emailDisplay}</td>
                <td><g:checkBox name="candidates[${i}].checked" checked="${c.checked}"/></td>
            </tr>
        </g:each>
    </table>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${email}">
        <ul class="errors" role="alert">
            <g:eachError bean="${email}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
        <fieldset class="form">
            <f:field bean="email" property="alsoSendToBillingEmail"/>
            <f:field bean="email" property="subject"/>
            <f:field bean="email" property="body"/>
            <f:field bean="email" property="attachments"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="send" class="send" value="${message(code: 'default.button.send.label', default: 'Send')}" />
        </fieldset>
    </g:uploadForm>

</div>
</body>
</html>
