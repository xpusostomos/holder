<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-person" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-person" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="person" />
            <ol class="property-list person">
                <li class="fieldcontain">
                    <span id="home-label" class="property-label">Home Page</span>
                    <div class="property-value" aria-labelledby="home-label"><f:display bean="person" property="home" label="HOME"/></div>
                </li>
                <li class="fieldcontain">
                    <span id="amountOwing-label" class="property-label">Amount Owing</span>
                    <div class="property-value" aria-labelledby="amountOwing-label"><f:display bean="person" property="amountOwing" label="Amount Owing">${new java.text.DecimalFormat('$#0.00;$#0.00 CR').format(person.amountOwing)}</f:display></div>
                </li>
                <li class="fieldcontain">
                    <span id="balances-label" class="property-label">Balances by Premises</span>
                    <div class="property-value" aria-labelledby="amountOwing-label"><f:display bean="person" property="balances" label="Balances by Premises">${person.balances.collectEntries{ [it.key, new java.text.DecimalFormat('$#0.00;$#0.00 CR').format(it.value)]}}</f:display></div>
                </li>
                <li class="fieldcontain">
                <span id="bondHeld-label" class="property-label">Bond Held</span>
                <div class="property-value" aria-labelledby="bondHeld-label"><f:display bean="person" property="bondHeld" label="Bond Held">${new java.text.DecimalFormat('$#0.00;$#0.00 CR').format(person.bondHeld)}</f:display></div>
            </li>
                <li class="fieldcontain">
                    <span id="rooms-label" class="property-label">Rooms</span>
                    <div class="property-value" aria-labelledby="rooms-label"><f:display bean="person" property="rooms" label="Rooms"/></div>
                </li>
            </ol>
            <g:form resource="${this.person}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.person}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    <g:link class="acceptPayment" controller="accountEntry" action="acceptPayment" id="${this.person.id}"><g:message code="default.button.payment.label" default="Accept Payment" /></g:link>
                    <g:link class="recordDebt" controller="accountEntry" action="recordDebt" id="${this.person.id}"><g:message code="default.button.debt.label" default="Record Debt" /></g:link>
                    <g:link class="refundBond" controller="accountEntry" action="refundBond" id="${this.person.id}"><g:message code="default.button.refundBond.label" default="Refund Bond" /></g:link>
                    <g:link class="payRefund" controller="person" action="payRefund" id="${this.person.id}"><g:message code="default.button.payRefund.label" default="Pay Refund" /></g:link>
                    <g:link class="payPerson" controller="person" action="payPerson" id="${this.person.id}"><g:message code="default.button.payRefund.label" default="Pay Person" /></g:link>
                    <g:link class="keepBond" controller="accountEntry" action="keepBond" id="${this.person.id}"><g:message code="default.button.refundBond.label" default="Keep Bond" /></g:link>
                    <g:link class="home" controller="guest" action="home" id="${this.person.id}"><g:message code="default.button.home.label" default="Home" /></g:link>
                    <g:link class="email" action="email" resource="${this.person}"><g:message code="default.button.email.label" default="Email" /></g:link>
                    <g:link class="email" action="rentDueEmail" resource="${this.person}"><g:message code="default.button.email.label" default="Rent Due Email" /></g:link>
                </fieldset>
            </g:form>
%{--            <g:form controller="accountEntry" action="acceptPayment" id="${person.id}" method="GET" name="acceptPaymentForm">--}%
%{--            </g:form>--}%
        </div>
    <sec:ifAnyGranted roles='ROLE_ADMIN'>
        <g:render template="ledger" model="[person: person, openingBalance: 0.0, accountEntries: person.accountEntries]"/>
        <g:if test="${person.user}">
%{--            TODO--}%
        <g:render template="/user/mail" model="[entityId: person.id, entityName: 'person']"/>
        </g:if>
    </sec:ifAnyGranted>

    </body>
</html>
