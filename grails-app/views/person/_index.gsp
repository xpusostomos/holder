
<table class="table table-striped table-bordered">
    <thead class="thead-light">
    <tr><th colspan="7">${title}</th></tr>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Owing</th>
        <th>Home</th>
        <th>Email</th>
        <th>Phone</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${personList}">
        <tr>
            <td>${it.id}</td>
            <td><g:link controller="person" action="edit" id="${it.id}">${it.name}</g:link></td>
            <td><g:link controller="person" action="acceptPayment" id="${it.id}">${it.amountOwing}</g:link></td>
            <td><g:link controller="guest" action="home" id="${it.id}">HOME</g:link></td>
            <td><a href="mailto:${it.user.email}" target="_blank">${it.user.email}</a></td>
            <td>${it.phone}</td>
        </tr>
    </g:each>
    </tbody>
</table>
