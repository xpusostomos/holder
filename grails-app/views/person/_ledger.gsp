
<g:set var="f" value="${new java.text.DecimalFormat('$#0.00;$#0.00 CR')}"/>
<table>
    <g:set var="aes" value="${accountEntries.sort { a, b -> a.date <=> b.date ?: a.id <=> b.id }}"/>
    <g:set var="balance" value="${openingBalance}"/>
    <tr><th colspan="4">${title}</th></tr>
    <tr><th>Date</th><th>Description</th><th style="text-align:right">Amount</th><th style="text-align:right">Balance</th></tr>
    <tr><td></td><td>Opening Balance</td><td></td><td style="text-align:right">${f.format(balance)}</td></tr>
    <g:each in="${aes}" var="ae">
        <g:set var="balance" value="${balance.add(ae.personAmount)}"/>
        <tr><td>
            <sec:ifAllGranted roles="ROLE_ADMIN"><g:link controller="accountEntry" action="show" id="${ae.id}">${ae.date}</g:link></sec:ifAllGranted>
            <sec:ifNotGranted roles="ROLE_ADMIN">${ae.date}</sec:ifNotGranted>
        </td><td>${ae.description}</td><td style="text-align:right">${f.format(ae.personAmount)}</td><td style="text-align:right">${f.format(balance)}</td></tr>
    </g:each>
    <tr><td></td><td>BALANCE DUE NOW</td><td></td><td style="text-align:right">${f.format(balance)}</td></tr>
</table>
