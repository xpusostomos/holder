<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<br>
Thanks ${person.firstName}!
<br>
${holder.User.principal.name} has been notified.
</body>
</html>
