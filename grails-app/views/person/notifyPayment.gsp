<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<h1>Notify of payment</h1>
<g:form action="paymentNotified">
    <g:hiddenField name="person.id" value="${person.id}"/>
    Hi, click below to notify that you've made payment for ${person.firstName},
    <br>
    Comments: <g:textArea class="small-comment" name="comment"/>
    <br>
    <g:submitButton class="btn btn-primary" name="notifyPayment" value="Click to notify of Payment"/>
</g:form>
</body>
</html>
