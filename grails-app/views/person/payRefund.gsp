<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'paymentCommand.label', default: 'Refund Bond')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="acceptPayment" class="content scaffold-create" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.paymentCommand}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.paymentCommand}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form method="POST" action="savePayRefund">
        <g:hiddenField name="person.id" value="${paymentCommand.person.id}"/>
        <p>
            Name: ${paymentCommand.person.name}
        </p>

        <p>
            Rooms:
            <g:each in="${paymentCommand.person.contracts.findAll { it.active }}">
                ${it.room.name}
            </g:each>
        </p>

        <p>
            Amount Owing: ${paymentCommand.person.amountOwing}
        </p>
    %{--        <g:actionSubmit class="btn btn-primary" action="saveRefundBond" value="Refund Bond"/>--}%
        <g:render template="/person/ledger" model="[title: 'ACCOUNT ENTRIES', person: paymentCommand.person, openingBalance: 0.0, accountEntries: paymentCommand.person.accountEntries]"/>


        <fieldset class="form">
            <f:field bean="paymentCommand" property="date"/>
            <f:field bean="paymentCommand" property="amount"/>
            <f:field bean="paymentCommand" property="method"/>
            <f:field bean="paymentCommand" property="description"/>
            <f:field bean="paymentCommand" property="notes"/>

        </fieldset>
        <fieldset class="buttons">
            <input class="refundBond" type="submit"
                   value="${message(code: 'default.button.refundBond.label', default: 'Pay Refund')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
