<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 8/12/2020
  Time: 2:03 am
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Thankyou</title>
</head>

<body>
<div>
Thankyou for your application. Someone will be in touch within 24 hours
</div>
<div>
<a href="/">HOME</a>
</div>
</body>
</html>
