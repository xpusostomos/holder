<%@ page import="org.springframework.validation.FieldError" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'booking.label', default: 'Booking')}" />
        <title><g:message code="default.apply.label" default="Apply to see Room" /></title>
    </head>
    <body>
        <a href="#create-booking" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id="create-booking" class="content scaffold-create" role="main">
            <h1><g:message code="default.apply.label" default="Apply to see Room" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.booking}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.booking}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            If you could give me some basic details about yourself, and tell me when you want to look at the room,
            I can get back to you and organise the details.
            <g:uploadForm action="save" method="POST" autocomplete="off">
                <fieldset class="form">
                    %{--                    <f:all bean="booking" order="firstName,lastName,email,phone,checkIn,checkOut"/>--}%
                    <f:field bean="booking" property="firstName"/>
                    <f:field bean="booking" property="lastName"/>
                    <f:field bean="booking" property="email"/>
                    <f:field bean="booking" property="phone"/>
                    <f:field bean="booking" property="checkIn" label="Desired move in date"/>
                    <f:field bean="booking" property="checkOut" label="Estimated move out date (guess is fine)"/>
                    <f:field bean="booking" property="employment" label="If student, enter course name, otherwise employment or occupation"/>
                    <f:field bean="booking" property="sex" label="gender" required="true"/>
%{--                     needing required, kind of a bug I reported--}%
                    <f:field bean="booking" property="dateInspection" label="Suggest date/time to inspect room (can do via Skype/Whatsapp etc if required)"/>
                    <f:field bean="booking" property="applyNotes" label="Comments / Questions"/>
                    <f:field bean="booking" property="album" label="Upload any documents you would like to submit"/>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.apply.label', default: 'Submit Application')}" />
                </fieldset>
            </g:uploadForm>
        </div>
    </body>
</html>
