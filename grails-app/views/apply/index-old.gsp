<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 7/12/2020
  Time: 4:35 pm
--%>

<%@ page import="java.time.Year" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Apply to see Room</title>
%{--    <script>--}%
%{--    function addUploadButton(group, name, index) {--}%
%{--       if (!document.getElementById(name)) {--}%
%{--           var input = document.createElement("input");--}%
%{--           input.type = "file";--}%
%{--           input.name = name;--}%
%{--           index++;--}%
%{--           input.setAttribute("onchange", `addUploadButton(\${group}, \${name}, \${index})`);--}%
%{--           input.id = name;--}%
%{--           var container = document.getElementById("photoGroup");--}%
%{--           var para = document.createElement("p")--}%
%{--           container.appendChild(para);--}%
%{--           para.appendChild(input);--}%
%{--       }--}%
%{--    }--}%
%{--</script>--}%
</head>

<body>
<h1>Application to view room</h1>
Enter some basic details about yourself and your requirements, and I'll get back to you shortly.
<g:uploadForm>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.firstName">First Name</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('firstName') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.firstName" value="${booking.firstName}"/>
            <div class="invalid-feedback"><g:fieldError field="firstName" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.lastName">Last Name</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('lastName') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.lastName" value="${booking.lastName}"/>
            <div class="invalid-feedback"><g:fieldError field="lastName" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.email">Email</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('email') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.email" value="${booking.email}"/>
            <div class="invalid-feedback"><g:fieldError field="email" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.phone">Phone</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('phone') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.phone" value="${booking.phone}"/>
            <div class="invalid-feedback"><g:fieldError field="phone" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="booking.checkIn">Estimated move in date</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('checkIn') ? 'is-invalid' : 'is-valid'}" type="date" name="booking.checkIn" value="${formatDate(format:'yyyy-MM-dd', date:booking.checkIn)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkIn" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-6">
            <label for="booking.checkOut">Estimated move out date (guess is fine)</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('checkOut') ? 'is-invalid' : 'is-valid'}" type="date" name="booking.checkOut" value="${formatDate(format:'yyyy-MM-dd', date:booking.checkOut)}"/>
            <div class="invalid-feedback"><g:fieldError field="checkOut" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="booking.employment">If student, enter course name, otherwise list employment or occupation</label>
            <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('employment') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.employment" value="${booking.employment}"/>
            <div class="invalid-feedback"><g:fieldError field="employment" bean="${booking}"/></div>
        </div>
        <div class="form-group col-md-4">
            <label for="booking.male">Gender</label>
            <g:select class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('male') ? 'is-invalid' : 'is-valid'}" name="booking.male"  from="['', 'Male', 'Female']" keys="['', true, false]" value="${booking.male}" />
            <div class="invalid-feedback"><g:fieldError field="male" bean="${booking}"/></div>
        </div>
    </div>
    <div class="form-group">
        <label for="booking.dateInspection">Suggest date/time to inspect room</label>
        <g:field class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('dateInspection') ? 'is-invalid' : 'is-valid'}" type="text" name="booking.dateInspection" onload="this.datetimepicker()" value="${booking.dateInspection?.format(holder.DateTimeValueConverter.stdFormatter)}"/>
        <div class="invalid-feedback"><g:fieldError field="dateInspection" bean="${booking}"/></div>
    </div>
    <div class="form-group">
        <label for="booking.applyNotes">Comments</label>
        <g:textArea class="form-control ${!booking.hasErrors() ? '' : booking.errors.getFieldError('applyNotes') ? 'is-invalid' : 'is-valid'}" name="booking.applyNotes" rows="5" cols="100">
${booking.applyNotes}
        </g:textArea>
        <div class="invalid-feedback"><g:fieldError field="applyNotes" bean="${booking}"/></div>
    </div>
    <div class="form-group" id="photoGroup">
        <label>Upload any attachments you would like to submit</label>
%{--        <p><g:field name="booking.album.photos[0].data" type="file" onchange="addUploadButton('photoGroup', 0, 'booking.album.photos[{index}].data')"/></p>--}%
    </div>
    <g:actionSubmit class="btn btn-primary" action="save" value="Submit Application"/>
</g:uploadForm>
<script type="text/javascript">
$(function() {
    $('[name$=dateInspection]').datetimepicker({format: "Y-m-d H:i"});
    addUploadButton('photoGroup', 0, 'booking.album.photos[{index}].data');
});
</script>
</body>
</html>
