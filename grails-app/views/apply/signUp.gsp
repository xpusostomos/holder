<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'room.label', default: 'Room')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
<a href="#edit-room" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="edit-room" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.person}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.person}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.contract}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.contract}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:uploadForm action="signUp" method="POST" name="mainForm">

        <g:hiddenField name="contract.id" value="${contract.id}"/>
        <g:hiddenField name="user.id" value="${user.id}"/>
        <g:hiddenField name="user.firstName" value="${user.firstName}"/>
        <g:hiddenField name="user.lastName" value="${user.lastName}"/>
    %{--    <g:hiddenField name="person.phone" value="${person.phone}"/>--}%
        <g:hiddenField name="person.id" value="${person.id}"/>
        <g:hiddenField name="person.sex" value="${person.sex}"/>
        <g:hiddenField name="person.employment" value="${person.employment}"/>
        <g:hiddenField name="passKey" value="${passKey}"/>
        <fieldset class="form">
            <f:field prefix="user." bean="user" property="email" required="true"/>
            <f:field prefix="user." bean="user" property="passwd" label="Choose Password (for later access to this web site)" required="true"/>
            <f:field prefix="person." bean="person" property="phone" required="true"/>
            <f:field prefix="person." bean="person" property="photo" label="Upload a photo of yourself" required="true"/>
            <f:field prefix="person." bean="person" property="passport" label="Upload a photo of your passport or drivers license" required="true"/>
            %{--<label for="user.email">Email</label>--}%
%{--        <g:field class="form-control ${!user.hasErrors() ? '' : user.errors.getFieldError('email') ? 'is-invalid' : 'is-valid'}" type="text" name="user.email" value="${user.email}"/>--}%
%{--        <div class="invalid-feedback"><g:fieldError field="email" bean="${user}"/></div>--}%
%{--    </div>--}%
%{--    <div class="form-group col-md-6">--}%
%{--        <label for="user.rawPassword">Password (for later access to this web site)</label>--}%
%{--        <g:field class="form-control ${!user.hasErrors() ? '' : user.errors.getFieldError('rawPassword') ? 'is-invalid' : 'is-valid'}" type="text" name="user.rawPassword" value="${user.rawPassword}"/>--}%
%{--        <div class="invalid-feedback"><g:fieldError field="rawPassword" bean="${user}"/></div>--}%
%{--    </div>--}%
%{--    </div>--}%
%{--    <div class="form-row">--}%
%{--    <div class="form-group col-md-6">--}%
%{--        <label>Upload a photo of yourself</label>--}%
%{--        <p><g:field name="person.photo.data" type="file"/></p>--}%
%{--    </div>--}%
%{--    <div class="form-group col-md-6">--}%
%{--        <label>Upload a photo of your passport or drivers license</label>--}%
%{--        <p><g:field name="person.passport.data" type="file"/></p>--}%
%{--    </div>--}%
%{--    </div>--}%
            <span class="border border-primary">
            <h1>Contract Terms</h1>
            %{--        <div class="contract">--}%
            ${raw(contract.contractTerms)}
            %{--        </div>--}%
        </span>
        </fieldset>
    </g:uploadForm>
    <g:form action="signUp" method="POST" name="rejectForm">
        <g:hiddenField name="contract.id" value="${contract.id}"/>
        <g:hiddenField name="passKey" value="${passKey}"/>
    </g:form>
    <fieldset class="buttons">
        <g:actionSubmit class="accept" action="signed" value="${message(code: 'default.button.accept.label', default: 'Agree to Contract')}" form="mainForm"/>
        <g:actionSubmit class="reject" action="rejected" value="${message(code: 'default.button.reject.label', default: 'Do not agree')}" form="rejectForm"/>
    </fieldset>
</div>
</body>
</html>
