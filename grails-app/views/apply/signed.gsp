<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 20/03/2021
  Time: 5:23 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Bookings</title>
</head>

<body>
<p>
You're now signed up! You can collect the keys and move in!

Keep a note of your password, so you can login here to check when rent is due.
Now click <g:link controller="guest" action="home" id="${person.id}">here to go to your home page!</g:link>

</p>
</body>
</html>
