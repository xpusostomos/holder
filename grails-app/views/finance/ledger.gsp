<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 21/11/21
  Time: 3:41 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<g:form controller="finance" action="ledger" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton class="btn btn-primary" name="submit" ></g:submitButton>

    <table class="border-table">
        <thead>
        <tr>
            <th>Date</th>
            <th>P?</th>
            <th>Description</th>
            <th>Premises</th>
            <th>Ref</th>
            <th style="text-align:right">Debit</th>
            <th style="text-align:right">Credit</th>
            <th style="text-align:right">Balance</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${accounts}" var="acc">
            <g:set var="bal" value="${BigDecimal.ZERO}"/>
            <g:if test="${acc.entries(c.fromDate, c.toDate, c.premises).size() > 0}">
                <tr><th colspan="8" style="text-align:center">${acc.id} ${acc.name}</th></tr>
            </g:if>
%{--            <g:each in="${(acc.debitAccountEntries + acc.creditAccountEntries).sort { a, b -> a.date <=> b.date}}" var="ent">--}%
            <g:each in="${acc.entries(c.fromDate, c.toDate, c.premises)}" var="ent">
                <tr>
                    <td><g:link controller="accountEntry" action="show" id="${ent.id}">${ent.date}</g:link></td>
                    <td>${ent.provisional ? '*' :''}</td>
                    <td>${ent.description}</td>
                    <td>${ent.premises}</td>
                    <td>
                        <g:if test="${ent.debitAccount == acc}">
                            ${ent.creditAccount}
                        </g:if>
                        <g:else>
                            ${ent.debitAccount}
                        </g:else>
                    </td>
                    <td style="text-align:right">
                        <g:if test="${ent.debitAccount == acc}">
                            <g:set var="bal" value="${bal.add(ent.amount)}"/>
                            ${ent.amount}
                        </g:if>
                    </td>
                    <td style="text-align:right">
                        <g:if test="${ent.creditAccount == acc}">
                            <g:set var="bal" value="${bal.minus(ent.amount)}"/>
                            ${ent.amount}
                        </g:if>
                    </td>
                    <td style="text-align:right">
                        ${bal}
                    </td>
                </tr>
            </g:each>
        </g:each>
%{--        <tr>--}%
%{--            <td><b>TOTAL</b></td>--}%
%{--            <td style="text-align:right"><b>${dtotal}</b></td>--}%
%{--            <td style="text-align:right"><b>${-ctotal}</b></td>--}%
%{--        </tr>--}%
        </tbody>
    </table>
</g:form>
</body>
</html>
