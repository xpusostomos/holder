<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 21/11/21
  Time: 3:41 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<g:form controller="finance" action="balanceSheet" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton name="submit" ></g:submitButton>

    <table class="border-table">
        <thead>
        <tr>
            <th>Account</th><th style="text-align:right">Amount</th>
        </tr>
        </thead>
        <tbody>
        <g:set var="dtotal" value="${BigDecimal.ZERO}"/>
        <g:set var="ctotal" value="${BigDecimal.ZERO}"/>
        <g:set var="etotal" value="${BigDecimal.ZERO}"/>
        <g:each in="${assetAccounts}" var="acc">
            <g:set var="bal" value="${acc.balance(c.fromDate, c.toDate, c.premises)}"/>
            <g:if test="${!acc.parent || !bal.equals(BigDecimal.ZERO)}">
                <tr>
                    <g:if test="${acc.parent}">
                        <td><g:link controller="account" action="show" id="${acc.id}">${acc.name}</g:link></td>
                    </g:if>
                    <g:else>
                        <td><b>${acc.name}</b></td>
                    </g:else>
                    <td style="text-align:right">
                        <g:if test="${bal != 0.0}">
                            ${bal}
                            <g:set var="dtotal" value="${dtotal + bal}"/>
                        </g:if>
                    </td>
                </tr>
            </g:if>
        </g:each>
        <tr>
            <td><b>TOTAL ASSETS</b></td>
            <td style="text-align:right"><b>${dtotal}</b></td>
        </tr>
        <g:each in="${liabilityAccounts}" var="acc">
            <g:set var="bal" value="${acc.balance(c.fromDate, c.toDate, c.premises)}"/>
            <g:if test="${!acc.parent || !bal.equals(BigDecimal.ZERO)}">
                <tr>
                    <g:if test="${acc.parent}">
                        <td><g:link controller="account" action="show" id="${acc.id}">${acc.name}</g:link></td>
                    </g:if>
                    <g:else>
                        <td><b>${acc.name}</b></td>
                    </g:else>
                    <td style="text-align:right">
                        <g:if test="${bal != 0.0}">
                            ${-bal}
                            <g:set var="ctotal" value="${ctotal + bal}"/>
                        </g:if>
                    </td>
                </tr>
            </g:if>
        </g:each>
        <tr>
            <td><b>TOTAL LIABILITIES</b></td>
            <td style="text-align:right"><b>${-ctotal}</b></td>
        </tr>
        <g:each in="${equityAccounts}" var="acc">
            <g:set var="bal" value="${acc.balance(c.fromDate, c.toDate, c.premises)}"/>
            <g:if test="${!acc.parent || !bal.equals(BigDecimal.ZERO)}">
                <tr>
                    <g:if test="${acc.parent}">
                        <td><g:link controller="account" action="show" id="${acc.id}">${acc.name}</g:link></td>
                    </g:if>
                    <g:else>
                        <td><b>${acc.name}</b></td>
                    </g:else>
                    <td style="text-align:right">
                        <g:if test="${bal != 0.0}">
                            ${-bal}
                            <g:set var="ctotal" value="${ctotal + bal}"/>
                            <g:set var="etotal" value="${etotal + bal}"/>
                        </g:if>
                    </td>
                </tr>
            </g:if>
        </g:each>
        <tr>
            <td><b>TOTAL EQUITY</b></td>
            <td style="text-align:right"><b>${-etotal}</b></td>
        </tr>
        <tr>
            <td><b>TOTAL LIABILITIES AND EQUITY</b></td>
            <td style="text-align:right"><b>${-ctotal}</b></td>
        </tr>
        </tbody>
    </table>
</g:form>
</body>
</html>
