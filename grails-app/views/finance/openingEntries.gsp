<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 23/11/21
  Time: 10:12 am
--%>

<%@ page import="holder.Account" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<table class="border-table">
    <thead>
    <tr>
        <th colspan="5">Opening Entries</th>
    </tr>
    <tr>
        <th>Date</th>
        <th>Property</th>
        <th>Debit Account</th>
        <th>Credit Account</th>
        <th>Amount</th>
%{--        <th>Balance</th>--}%
    </tr>
    </thead>
    <tbody>
    <g:set var="balance" value="${BigDecimal.ZERO}"/>
    <g:each in="${accountEntries.collect {it.date}.unique().sort()}" var="date">
        <tr><th colspan="4" style="text-align: center;">${date}</th></tr>
%{--        <g:set var="balance" value="${BigDecimal.ZERO}"/>--}%
        <g:each in="${accountEntries.findAll{ it.date == date}}" var="ent">
%{--            <g:set var="balance" value="${balance.add(ent.amount)}"/>--}%
            <tr>
                <td><g:link controller="accountEntry" action="show" id="${ent.id}">${ent.date}</g:link></td>
                <td>${ent.premises}</td>
                <td>${ent.debitAccount}</td>
                <td>${ent.creditAccount}</td>
                <td>${ent.amount}</td>
%{--                <td>${balance}</td>--}%
            </tr>
        </g:each>
    </g:each>
    </tbody>
</table>

</body>
</html>
