<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 21/11/21
  Time: 3:41 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<g:form controller="finance" action="journal" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton class="btn btn-primary" name="submit" ></g:submitButton>

    <table class="border-table">
        <thead>
        <tr>
            <th>Date</th><th>Prov?</th><th>Detail</th><th>Premises</th><th>Account</th><th style="text-align:right">Debit</th><th style="text-align:right">Credit</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${accountEntries}" var="ent">
            <tr>
                <td><g:link controller="accountEntry" action="show" id="${ent.id}">${ent.date}</g:link></td>
                <td></td>
                <td>${ent.debitAccount.name}</td>
                <td>${ent.premises}</td>
                <td>${ent.debitAccount.id}</td>
                <td style="text-align:right">
                    ${ent.amount}
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>${ent.creditAccount.name}</td>
                <td>${ent.premises}</td>
                <td>${ent.creditAccount.id}</td>
                <td></td>
                <td style="text-align:right">
                    ${ent.amount}
                </td>
            </tr>
            <g:if test="${ent.description}">
                <tr>
                    <td style="background-color: #dddddd"></td>
                    <td style="background-color: #dddddd">${ent.provisional ? '*' :''}</td>
                    <td style="background-color: #dddddd">${ent.description}</td>
                    <td style="background-color: #dddddd">${ent.premises}</td>
                    <td style="background-color: #dddddd"></td>
                    <td style="background-color: #dddddd"></td>
                    <td style="background-color: #dddddd"></td>
                </tr>
            </g:if>
        </g:each>
        </tbody>
    </table>
</g:form>
</body>
</html>
