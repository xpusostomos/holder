<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 5/10/21
  Time: 10:10 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>

<body>
<g:form controller="finance" action="index" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton class="btn btn-primary" name="submit" ></g:submitButton>

</g:form>
%{--<g:actionSubmit class="btn btn-primary" action="index" value="Refresh"/>--}%
%{--    <g:submitButton name="submit" ></g:submitButton>--}%
<div style="overflow-y:scroll;">
<table>
    <thead>
    <tr>
        <th></th>
        <g:each in="${accounts.sort { a, b -> a.id <=> b.id }}" var="acc">
            <th><g:link controller="account" action="show" id="${acc.id}">${acc.id } ${acc.name}</g:link></th>
        </g:each>
    </tr>
    </thead>
    <g:each in="${summary.keySet().sort()}" var="period">
        <tr>
            <th>${period}</th>
            <g:each in="${accounts.sort { a, b -> a.id <=> b.id }}" var="acc">
                <th>
                    ${summary.get(period).get(acc)}
                </th>
            </g:each>
        </tr>
    </g:each>
    <tr>
        <th>TOTALS</th>
        <g:each in="${accounts.sort { a, b -> a.id <=> b.id }}" var="acc">
            <th>${totals.get(acc)}</th>
        </g:each>
    </tr>
</table>
</div>
</body>
</html>
