<%@ page import="holder.Contract" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<g:form controller="finance" action="businessSummary" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton name="submit" ></g:submitButton>

    <table class="border-table">
        <thead>
        <tr>
            <th>Period</th><th>Income</th><th>Expenses</th><th>Ex Personal Expenses</th><th>Profit</th><th>Ex Personal Profit</th>
        </tr>
        </thead>
        <tbody>
        <g:set var="incomeTotal" value="${BigDecimal.ZERO}"/>
        <g:set var="expenseTotal" value="${BigDecimal.ZERO}"/>
        <g:set var="pincomeTotal" value="${BigDecimal.ZERO}"/>
        <g:set var="pexpenseTotal" value="${BigDecimal.ZERO}"/>
        <g:set var="tdate" value="${Contract.stdPeriodEndFor(c.fromDate)}"/>
        <g:set var="fdate" value="${holder.Contract.stdPeriodStartFor(tdate, true)}"/>
        <g:while test="${Contract.stdPeriodEndFor(fdate) <= c.toDate}">
            <g:set var="tdate" value="${Contract.stdPeriodEndFor(fdate)}"/>
            <g:set var="income" value="${-incomeAccounts.collect { it.trialBalance(fdate, tdate, c.premises, false)}.sum()}"/>
            <g:set var="expenses" value="${expenseAccounts.collect { it.trialBalance(fdate, tdate, c.premises, false)}.sum()}"/>
            <g:set var="incomeTotal" value="${incomeTotal + income}"/>
            <g:set var="expenseTotal" value="${expenseTotal + expenses}"/>
            <g:set var="pincome" value="${-incomeAccounts.collect { it.trialBalance(fdate, tdate, c.premises, true)}.sum()}"/>
            <g:set var="pexpenses" value="${expenseAccounts.collect { it.trialBalance(fdate, tdate, c.premises, true)}.sum()}"/>
            <g:set var="pincomeTotal" value="${pincomeTotal + pincome}"/>
            <g:set var="pexpenseTotal" value="${pexpenseTotal + pexpenses}"/>
            <tr>
                <td>${fdate} - ${tdate}</td>
                <td>${income}</td>
                <td>${expenses}</td>
                <td>${pexpenses}</td>
                <td>${income - expenses}</td>
                <td>${pincome - pexpenses}</td>
            </tr>
            <g:set var="fdate" value="${holder.Contract.stdPeriodStartFor(tdate.plusDays(1), true)}"/>
        </g:while>
        <tr>
            <th>TOTAL ${Contract.stdPeriodStartFor(c.fromDate, true)} - ${tdate}</th>
            <th>${incomeTotal}</th>
            <th>${expenseTotal}</th>
            <th>${pexpenseTotal}</th>
            <th>${incomeTotal - expenseTotal}</th>
            <th>${pincomeTotal - pexpenseTotal}</th>
        </tr>
        </tbody>
    </table>
</g:form>
</body>
</html>
