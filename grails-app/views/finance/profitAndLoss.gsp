<%--
  Created by IntelliJ IDEA.
  User: chris
  Date: 21/11/21
  Time: 3:41 pm
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Finance</title>
</head>
<body>
<g:form controller="finance" action="profitAndLoss" method="GET">
    <f:field bean="c" property="fromDate"/>
    <f:field bean="c" property="toDate"/>
    <f:field bean="c" property="byYear"/>
    <f:field bean="c" property="calendarYear"/>
    <f:field bean="c" property="premises"/>
    <g:submitButton name="submit" ></g:submitButton>

    <table class="border-table">
        <thead>
        <tr>
            <th>Account</th><th style="text-align:right">Amount</th><th style="text-align:right">Less Personal Use ${c.premises?.personalUse ? "( ${c.premises.personalUse * 100}% )":''}</th>
        </tr>
        </thead>
        <tbody>
        <g:set var="dtotal" value="${BigDecimal.ZERO}"/>
        <g:set var="ctotal" value="${BigDecimal.ZERO}"/>
        <g:set var="pdtotal" value="${BigDecimal.ZERO}"/>
        <g:set var="pctotal" value="${BigDecimal.ZERO}"/>
        <g:each in="${incomeAccounts}" var="acc">
            <g:set var="bal" value="${acc.trialBalance(c.fromDate, c.toDate, c.premises, false)}"/>
            <g:set var="pbal" value="${acc.trialBalance(c.fromDate, c.toDate, c.premises, true)}"/>
            <g:if test="${!acc.parent || !bal.equals(BigDecimal.ZERO)}">
                <tr>
                    <g:if test="${acc.parent}">
                        <td><g:link controller="account" action="show" id="${acc.id}">${acc.name}</g:link></td>
                    </g:if>
                    <g:else>
                        <td><b>${acc.name}</b></td>
                    </g:else>
                    <td style="text-align:right">
                        <g:if test="${bal != 0.0}">
                            ${-bal}
                            <g:set var="ctotal" value="${ctotal + bal}"/>
                        </g:if>
                    </td>
                    <td style="text-align:right">
                        <g:if test="${pbal != 0.0}">
                            ${-pbal}
                            <g:set var="pctotal" value="${pctotal + pbal}"/>
                        </g:if>
                    </td>
                </tr>
            </g:if>
        </g:each>
        <tr>
            <td><b>TOTAL INCOME</b></td>
            <td style="text-align:right"><b>${-ctotal}</b></td><td style="text-align:right"><b>${-pctotal}</b></td>
        </tr>
        <g:each in="${expenseAccounts}" var="acc">
            <g:set var="bal" value="${acc.trialBalance(c.fromDate, c.toDate, c.premises, false)}"/>
            <g:set var="pbal" value="${acc.trialBalance(c.fromDate, c.toDate, c.premises, true)}"/>
            <g:if test="${!acc.parent || !bal.equals(BigDecimal.ZERO)}">
                <tr>
                    <g:if test="${acc.parent}">
                        <td><g:link controller="account" action="show" id="${acc.id}">${acc.name}</g:link></td>
                    </g:if>
                    <g:else>
                        <td><b>${acc.name}</b></td>
                    </g:else>
                    <td style="text-align:right">
                        <g:if test="${bal != 0.0}">
                            ${bal}
                            <g:set var="dtotal" value="${dtotal + bal}"/>
                        </g:if>
                    </td>
                    <td style="text-align:right">
                        <g:if test="${pbal != 0.0}">
                            ${pbal}
                            <g:set var="pdtotal" value="${pdtotal + pbal}"/>
                        </g:if>
                    </td>
                </tr>
            </g:if>
        </g:each>
        <tr>
            <td><b>TOTAL EXPENSES</b></td>
            <td style="text-align:right"><b>${dtotal}</b></td><td style="text-align:right"><b>${pdtotal}</b></td>
        </tr>
        <tr>
            <td><b>PROFIT</b></td>
            <td style="text-align:right"><b>${-(ctotal + dtotal)}</b></td><td style="text-align:right"><b>${-(pctotal + pdtotal)}</b></td>
        </tr>
        </tbody>
    </table>
</g:form>
</body>
</html>
