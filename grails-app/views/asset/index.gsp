<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'asset.label', default: 'Asset')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-asset" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label" args="['Asset']" /></g:link></li>
        <li><g:link class="list" action="depSchedule"><g:message code="default.asset.list.label" default="Depreciation Schedule" /></g:link></li>
    </ul>
</div>
%{--<g:link method="GET" resource="${accountEntryList[0]}">THIS IS A LINK</g:link>--}%
<div id="list-asset" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:set var="assetList" value="${accountEntryList}"/>
    <f:table collection="${accountEntryList}" properties="id,description,usefulLife,depreciationMethod" template="tableThisController"/>
</div>
</body>
</html>
