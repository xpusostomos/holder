<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'accountEntry.label', default: 'AccountEntry')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-accountEntry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.asset.list.label" default="Asset List"/></g:link></li>
                <li><g:link class="list" action="depSchedule"><g:message code="default.asset.list.label" default="Depreciation Schedule" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.asset.new.label" default="New Asset"/></g:link></li>
            </ul>
        </div>
        <div id="edit-accountEntry" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="['Asset']" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.accountEntry}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.accountEntry}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form action="update" id="${accountEntry.id}" method="PUT">
                <g:hiddenField name="version" value="${this.accountEntry?.version}" />
                <fieldset class="form">
                    <f:all bean="accountEntry" order="date,premises,debitAccount,creditAccount,amount,description,usefulLife,depreciationMethod,depreciationAccount,provisional,notes"/>
                    <f:field bean="accountEntry" property="initialAccumulatedDepreciation"/>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
%{--                    <g:actionSubmit class="save" action="updateAsset" value="Update"/>--}%
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
