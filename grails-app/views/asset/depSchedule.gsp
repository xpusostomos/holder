<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'accountEntry.label', default: 'AccountEntry')}" />
        <title>Depreciation Schedule</title>
    </head>
    <body>
        <a href="#list-accountEntry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.asset.list.label" default="Asset List"/></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.asset.new.label" default="New Asset"/></g:link></li>
            </ul>
        </div>
    <g:form action="depSchedule">
        <f:field bean="c" property="fromDate"/>
        <f:field bean="c" property="toDate"/>
        <f:field bean="c" property="byYear"/>
        <f:field bean="c" property="calendarYear"/>
        <f:field bean="c" property="premises"/>
        <g:submitButton class="btn btn-primary" name="submit" ></g:submitButton>
%{--        From: <g:field type="date" name="from" value="${from}"/>--}%
%{--        To: <g:field type="date" name="to" value="${to}"/>--}%
%{--        <g:actionSubmit class="btn btn-primary" action="depSchedule" value="Show"/>--}%
        <div id="list-accountEntry" class="content scaffold-list" role="main">
            <h1>Depreciation Schedule</h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
%{--            <f:table collection="${schedule}" properties="asset.id,asset.date,asset.description,asset.debitAccount,asset.depreciationAccount,asset.depreciationRate,asset.depreciationMethod,asset.openingValue"/>--}%
        <g:set var="tcost" value="${0.0}"/>
        <g:set var="topen" value="${0.0}"/>
        <g:set var="tdepreciation" value="${0.0}"/>
        <g:set var="tclose" value="${0.0}"/>
        <table>
            <thead>
            <tr>
                <th>ID</th><th>Purchase Date</th><th>Description</th><th>Premises</th><th>Cost</th><th>Debit Account</th><th>Depreciation Account</th>
                <th>Useful Life</th><th>Depreciation Rate</th><th>Depreciation Method</th><th>Opening Value</th><th>Depreciation</th><th>Closing Value</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${schedule}" var="si">
                <g:set var="tcost" value="${tcost+si.source.amount}"/>
                <g:set var="topen" value="${topen+si.openingValue}"/>
                <g:set var="tdepreciation" value="${tdepreciation+si.depreciation}"/>
                <g:set var="tclose" value="${tclose+si.closingValue}"/>
                <tr>
                    <td><g:link action="show" id="${si.source.id}">${si.source.id}</g:link></td>
                    <td>${si.source.date}</td>
                    <td>${si.source.description}</td>
                    <td>${si.source.premises}</td>
                    <td>${si.source.amount}</td>
                    <td>${si.source.debitAccount}</td>
                    <td>${si.source.depreciationAccount}</td>
                    <td>${si.source.usefulLife}</td>
                    <td>${si.depreciationRate}</td>
                    <td>${si.source.depreciationMethod}</td>
                    <td>${si.openingValue}</td>
                    <td title="${si.accountEntry?.id}">${si.depreciation}</td>
                    <td>${si.closingValue}</td>
                </tr>
            </g:each>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>${tcost}</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>${topen}</td>
                <td>${tdepreciation}</td>
                <td>${tclose}</td>
            </tr>
            </tbody>
        </table>
        <fieldset class="buttons">
            <g:actionSubmit class="generate" action="generate" type="submit" value="${message(code: 'default.button.generate.label', default: 'Generate')}" />
            <g:actionSubmit class="lowValuePool" action="lowValuePool" type="submit" value="${message(code: 'default.button.lowValuePool.label', default: 'Populate Low Value Pool')}" />
        </fieldset>
    </g:form>
        </div>
    </body>
</html>
