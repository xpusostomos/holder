<%@ page import="holder.AccountEntry" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'accountEntry.label', default: 'AccountEntry')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-accountEntry" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.asset.list.label" default="Asset List"/></g:link></li>
                <li><g:link class="list" action="depSchedule"><g:message code="default.asset.list.label" default="Depreciation Schedule" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.asset.new.label" default="New Asset"/></g:link></li>
            </ul>
        </div>
        <div id="show-accountEntry" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="['Asset']" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <ol class="property-list person">
                <li class="fieldcontain">
                    <span id="date-label" class="property-label">Date</span>
                    <div class="property-value" aria-labelledby="date-label">
                        <f:display bean="accountEntry" property="date"/>
                </li>
                <li class="fieldcontain">
                    <span id="debitAccount-label" class="property-label">Debit Account</span>
                    <div class="property-value" aria-labelledby="debitAccount-label">
                        <f:display bean="accountEntry" property="debitAccount"/>
                </li>
                <li class="fieldcontain">
                    <span id="premises-label" class="property-label">Premises</span>
                    <div class="property-value" aria-labelledby="premises-label">
                        <f:display bean="accountEntry" property="premises"/>
                </li>
                <li class="fieldcontain">
                    <span id="creditAccount-label" class="property-label">Credit Account</span>
                    <div class="property-value" aria-labelledby="creditAccount-label">
                        <f:display bean="accountEntry" property="creditAccount"/>
                </li>
                <li class="fieldcontain">
                    <span id="amount-label" class="property-label">Amount</span>
                    <div class="property-value" aria-labelledby="amount-label">
                        <f:display bean="accountEntry" property="amount"/>
                </li>
                <li class="fieldcontain">
                    <span id="description-label" class="property-label">Description</span>
                    <div class="property-value" aria-labelledby="description-label">
                        <f:display bean="accountEntry" property="description"/>
                </li>
                <li class="fieldcontain">
                    <span id="usefulLife-label" class="property-label">Useful Life</span>
                    <div class="property-value" aria-labelledby="usefulLife-label">
                        <f:display bean="accountEntry" property="usefulLife"/>
                </li>
%{--                <li class="fieldcontain">--}%
%{--                    <span id="depreciationRate-label" class="property-label">Depreciation Rate</span>--}%
%{--                    <div class="property-value" aria-labelledby="depreciationRate-label">--}%
%{--                        <f:display bean="accountEntry" property="depreciationRate"/>--}%
%{--                </li>--}%
                <li class="fieldcontain">
                    <span id="depreciationMethod-label" class="property-label">Depreciation Method</span>
                    <div class="property-value" aria-labelledby="depreciationMethod-label">
                        <f:display bean="accountEntry" property="depreciationMethod"/>
                </li>
                <li class="fieldcontain">
                    <span id="accountEntry-label" class="property-label">Depreciation Account</span>
                    <div class="property-value" aria-labelledby="accountEntry-label">
                        <f:display bean="accountEntry" property="depreciationAccount"/>
                </li>
                <li class="fieldcontain">
                    <span id="provisional-label" class="property-label">Provisional?</span>
                    <div class="property-value" aria-labelledby="provisional-label">
                        <f:display bean="accountEntry" property="provisional"/>
                </li>
                <li class="fieldcontain">
                    <span id="notes-label" class="property-label">Notes</span>
                    <div class="property-value" aria-labelledby="notes-label">
                        <f:display bean="accountEntry" property="notes"/>
                </li>
            </ol>
            <g:form action="delete" id="${accountEntry.id}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" id="${accountEntry.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    <table>
        <thead>
        <tr><th>Date</th><th>Description</th><th>Amount</th><th>Balance</th></tr>
        </thead>
        <g:set var="bal" value="${accountEntry.amount}"/>
        <tbody>
        <tr><td>${accountEntry.date}</td><td>Opening Value</td><td></td><td>${accountEntry.amount}</td></tr>
        <g:each in="${holder.AccountEntry.findBySource(accountEntry, [sort: 'date'])}" var="dep">
            <g:set var="bal" value="${bal-dep.amount}"/>
            <tr><td>${dep.date}</td><td>${dep.description}</td><td>${-dep.amount}</td><td>${bal}</td></tr>
        </g:each>
        </tbody>
    </table>
    </body>
</html>
