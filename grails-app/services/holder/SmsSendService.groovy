package holder

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import groovy.json.JsonException
import groovy.json.JsonSlurper
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.client.DefaultHttpClientConfiguration
import io.micronaut.http.client.netty.DefaultHttpClient

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class SmsSendService {
    def grailsApplication
    static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern('yyyy-MM-dd HH:mm:ss')
    static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern('yyyy-MM-dd')

    String getAuthorization() {
        "${grailsApplication.config.getProperty('smsKey')}:${grailsApplication.config.getProperty('smsSecret')}".bytes.encodeBase64().toString()
    }

    static Map<String,String> wholesaleSmsMap = [ text: 'message', sendAt: 'send_at', messageId: 'message_id' ]

    Map<String,String> transformWholesaleSmsInput(Map<String,String> input) {
        input.collectEntries {
            if (it.key == 'to' && it.value instanceof List) {
                [to: it.value.join(',')]
            } else if (it.key == 'sendAt' && it.value instanceof LocalDate) {
                [ sendAt: it.value.format(dateFormatter)]
            } else if (it.key == 'sendAt' && it.value instanceof LocalDateTime) {
                [ sendAt: it.value.format(dateTimeFormatter) ]
            } else {
                it
            }
        }.collectEntries { [(wholesaleSmsMap[it.key] ?: it.key) : it.value]}
    }

    Map<String,String> transformWholesaleSmsOutput(Map<String,String> output) {
        Map<String,String> reverseMap = wholesaleSmsMap.collectEntries {[(it.value): it.key]}
        output?.collectEntries {[(reverseMap[it.key] ?: it.key) : it.value]}.collectEntries {
            if (it.key == 'sendAt') {
                [sendAt: LocalDateTime.parse(it.value, dateTimeFormatter)]
            } else {
                it
            }
        }
    }

    Map send(Map input) {
        log.debug "send input: $input"
        URI endPoint = grailsApplication.config.getProperty('smsEndPoint').toURI()
        if (!input['from']) {
            String from = Config.findByName('SMS_FROM').string
            if (from) {
                input.from = from
            }
        }
        def newParams = transformWholesaleSmsInput(input)
        def result = comms(endPoint, newParams)
        def output = transformWholesaleSmsOutput(result)
        log.debug "send output: $output"
        output
    }

    def balance() {
        URI endPoint = grailsApplication.config.getProperty('smsBalanceEndPoint').toURI()
        def rtn = comms(endPoint, smsParams)
        return rtn?.balance
    }

    def cancel(String id) {
        URI endPoint = grailsApplication.config.getProperty('smsCancelEndPoint').toURI()
        def rtn = comms(endPoint, [id: id])
        return rtn
    }

    def comms(URI endPoint, def smsParams) {
        new DefaultHttpClient(endPoint).withCloseable { client ->
            HttpRequest request = HttpRequest.POST(endPoint, smsParams).header('Authorization', "Basic $authorization").contentType(MediaType.APPLICATION_FORM_URLENCODED)
            try {
                HttpResponse<String> resp = client.toBlocking().exchange(request, String)
                String json = resp.body()
                try {
                    def rtn = new JsonSlurper().parseText(json)
                } catch (JsonException x) {
                    log.error "Invalid JSON for $location: $json"
                    throw new SmsException(x)
                }
            } catch (Exception x) {
                log.error x.getLocalizedMessage()
                try {
                    def result = new JsonSlurper().parseText(x.response?.body?.value)
                    throw new SmsException(x, result)
                } catch (JsonException x2) {
                    log.error "Invalid error JSON for $location: $x.response?.body?.value"
                    throw new SmsException(x)
                }
            }
        }
    }


}

class SmsException extends Exception {
    def result

    SmsException(Exception x) {
        super(x)
    }
    SmsException(Exception x, def result) {
        super(x)
        this.result = result
    }
}
