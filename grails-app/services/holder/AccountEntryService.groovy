package holder

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

@Service(AccountEntry)
@Transactional
abstract class AccountEntryService {

    abstract AccountEntry get(Serializable id)

    abstract List<AccountEntry> list(Map args)

    abstract List<AccountEntry> findAll(Map args)

    abstract List<AccountEntry> findAllByUserCreated(User created, Map args)

    abstract Long countByUserCreated(User created)

    abstract  Long count()

    abstract void delete(Serializable id)

    abstract AccountEntry save(AccountEntry AccountEntry)

    void save(Collection<AccountEntry> entries) {
        entries.each { it.save() }
    }
}
