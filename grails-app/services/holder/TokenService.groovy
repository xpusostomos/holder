package holder

import grails.gorm.services.Service

@Service(Token)
interface TokenService {

    Token get(Serializable id)

    List<Token> list(Map args)

    List<Token> findAll(Map args)

    Long count()

    void delete(Serializable id)

    Token save(Token token)

}
