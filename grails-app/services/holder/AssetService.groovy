package holder

import grails.gorm.services.Service

@Service(AccountEntry)
interface AssetService {

    AccountEntry get(Serializable id)

    List<AccountEntry> list(Map args)

    List<AccountEntry> findAll(Map args)

    List<AccountEntry> findByDepreciationMethodIsNotNull(Map args)

    List<AccountEntry> findByDepreciationMethodIsNotNullAndPremises(Room premises, Map args)

    Long count()

    void delete(Serializable id)

    AccountEntry save(AccountEntry AccountEntry)

}
