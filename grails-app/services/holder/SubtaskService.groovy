package holder

import grails.gorm.services.Service

@Service(Schedule)
interface SubtaskService {

    Subtask get(Serializable id)

    List<Subtask> list(Map args)

    List<Subtask> findAll(Map args)

    Long count()

    void delete(Serializable id)

    Subtask save(Subtask subtask)

}
