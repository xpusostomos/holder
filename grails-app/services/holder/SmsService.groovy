package holder

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

@Service(Sms)
class SmsService {
    SmsSendService smsSendService

    Sms send (Map input) {
        Sms sms = new Sms(input)
        sms.save()
        Map rtn = smsSendService.send(input)
        sms.messageId = rtn.messageId
        sms
    }

//    def comms(URI endPoint, def smsParams) {
//        super.comms(endPoint, smsParams)
//    }
}
