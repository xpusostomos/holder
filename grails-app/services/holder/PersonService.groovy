package holder

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional
import grails.plugin.asyncmail.AsynchronousMailService
import org.apache.commons.lang3.RandomStringUtils
import grails.plugin.asyncmail.AsynchronousMailMessage
import org.springframework.beans.factory.annotation.Autowired

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period

@Service(Person)
abstract class PersonService {
//    AsyncMailService asyncMailService = applicationContext.getBean('asyncMailService')
//    AsyncMailService asyncMailService
    MailIndexService mailIndexService

    abstract Person get(Serializable id)

    abstract List<Person> list(Map args)

    abstract List<Person> findAll(Map args)

    abstract Long count()

    abstract void delete(Serializable id)

    abstract Person save(Person Person)

    Person newPersonFromContract(Contract contract) {
        User user = User.findByEmail(contract.booking.email)
        if (!user) {
            user = new User(
                    username: contract.booking.email,
                    passwd: RandomStringUtils.randomAlphabetic(8),
                    email: contract.booking.email,
                    firstName: contract.booking.firstName,
                    lastName: contract.booking.lastName
            )
            user.validate() // prepop update user
        }
        Person person
        if (user.id) {
            person = Person.findByUser(user)
        }
        if (!person) {
            person = new Person(
                    user: user,
                    firstName: contract.booking.firstName,
                    lastname: contract.booking.lastName,
                    email: contract.booking.email,
                    phone: contract.booking.phone,
                    sex: contract.booking.sex,
                    employment: contract.booking.employment,
                    type: PersonType.GUEST
            )
        }
        contract.person = person
        return person
    }

    // TODO this should not save() the objects, only create them
    Set<UserRole> createRoles(User user) {
        Role userRole = Role.findByAuthority('ROLE_USER')
        Role guestRole = Role.findByAuthority('ROLE_GUEST')
        Set<UserRole> rtn = new HashSet<>()
        if (!user.getAuthorities().contains(userRole)) {
            rtn.add(UserRole.create(user, userRole))
        }
        if (!user.getAuthorities().contains(guestRole)) {
            rtn.add(UserRole.create(user, guestRole))
        }
        return rtn
    }

    @Transactional
    def saveShortTermContract(Person person, Contract contract) {
        contract.bond = 0.0
        contract.checkoutDayIsPaid = false
        contract.agreed = true
        contract.noticeGiven = contract.checkIn
        contract.moveOut = contract.checkOut
        contract.payUntil = contract.checkOut.toLocalDate()
        contract.useStdPayDates = false
        contract.rentPeriod = contract.daysInContract

        contract.save()
        if (contract.booking && !contract.booking.dateDecided) {
            contract.booking.positiveDecision = true
            contract.booking.dateDecided = LocalDate.now()
        }
        // We must generate fees after person is saved.
//        contract.generateSignupAccountEntries().each { it.save() }
        AccountEntry iae = contract.generateAccountEntry(null, contract.payUntil, false).save()
        contract.accountEntries.contains(iae) || contract.addToAccountEntries(iae)
        contract.generateAgencyAccountEntries().each {
            contract.accountEntries.contains(it) || contract.addToAccountEntries(it)
            it.save()
        }
        AccountEntry payment = AccountEntry.findAllByContract(
            contract).find {
            it.debitAccountId == ChartOfAccounts.BANK.id &&
                    it.creditAccountId == ChartOfAccounts.ACCOUNTS_PAYABLE.id
        }
        if (!payment) {
            payment = new AccountEntry(
                    contract: contract,
                    debitAccountId: ChartOfAccounts.BANK.id,
                    creditAccountId: ChartOfAccounts.ACCOUNTS_PAYABLE.id
            )
        }
        payment.person = iae.person
        payment.room = iae.room
        payment.date = iae.date
        payment.periodBegin = iae.periodBegin
        payment.periodEnd = iae.periodEnd
        payment.amount = iae.amount
        payment.type = AccountEntryTypeEnum.PAYMENT
        payment.description = "Booking payment (auto entry) for contract $contract.id"
        payment.save()
        contract.each { it.closed = true }
    }

    @Transactional
    def signup(Contract contract) {
        Person person = newPersonFromContract(contract)
        person.save()
        contract.agreed = true
        contract.accountEntries.each {
            it.person = person
        }
        contract.save()
        contract.person.user.save() // TODO get rid of users?
//        contract.person.save()
        createRoles(contract.person.user)
        if (!contract.booking.dateDecided) {
            contract.booking.positiveDecision = true
            contract.booking.dateDecided = LocalDate.now()
        }
        // We must generate fees after person is saved.
        contract.generateSignupAccountEntries().forEach { it.save() }
    }


    def sendEmail(EmailCommand email) {
        email.candidates.findAll { it.checked }.collect {it.person}.forEach {
            sendEmail(it, email)
        }
    }

    AsynchronousMailMessage sendEmail(Person person, EmailCommand email) {
        AsynchronousMailService asynchronousMailService = applicationContext.getBean('asynchronousMailService')
        def addresses = [ person.emailDisplay  ]
        def ccAddresses = []
        if (email.alsoSendToBillingEmail && person.billingEmail) {
            ccAddresses += person.billingEmailDisplay
        }
        int maxAttempts = Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool ? 3 : 0
        AsynchronousMailMessage mm = asynchronousMailService.sendMail {
            to addresses
            if (ccAddresses.size() >= 1) {
                cc ccAddresses
            }
            subject email.subject
            text email.body
//            externalReference person.user.id // TODO oh shit, have we hard coded user ids into emails
            maxAttemptsCount maxAttempts
            email.attachments?.photos.each {
                attach it.name, it.mimeType, it.data
            }
        }
        mailIndexService.create(mm, null, [
                [id: person.id, name: 'person'],
                [id: person.user?.id, name: 'user']])
        mm
    }

    AsynchronousMailMessage generatePaymentDueEmail(Person person) {
        person.waitingOnPayment = true
        Token token = new Token(
                user: person.user,
                expiry: LocalDateTime.now().plusDays(14),
                oneTime: false
        )
        EmailTemplate.findTemplate('RENT_REMINDER', person.tagList, person.currentRoom).sendMail(
                [
                        person  : person,
                        owing   : person.amountOwing,
                        dueDate : person.paymentDue,
                        token   : token
                ],
                person.emailEnabled && Config.getBool(ConfigNameEnum.EMAIL_REMINDER_ENABLED),
                [
                        [id: person.id, name: 'person'],
                        [id: person.user?.id, name: 'user']
                ],
                person.smsEnabled)
    }
}
