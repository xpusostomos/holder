package holder

import grails.gorm.services.Service
import grails.gorm.transactions.Transactional

import java.time.LocalDateTime

@Service(Schedule)
@Transactional
abstract class ScheduleService {

    abstract Schedule get(Serializable id)

    abstract List<Schedule> list(Map args)

    abstract List<Schedule> findAll(Map args)

    abstract Long count()

    abstract void delete(Serializable id)

    abstract Schedule save(Schedule schedule)

    def checkReminderAndSendEmail(Schedule schedule) {
        LocalDateTime d = schedule.nextReminder
        if (!schedule.complete && !schedule.cancelled && schedule.sendMail && d <= LocalDateTime.now()) {
            sendReminder(schedule, d < schedule.due ? 'TASK_REMIND_EARLY' : 'TASK_REMIND_LATE')
            schedule.lastReminder = d
            schedule.remindersSent++
        }
    }

    def sendReminder(Schedule schedule, String emailName) {
        Token token = new Token(
                user: schedule.person.user,
                expiry: LocalDateTime.now().plusDays(14),
                oneTime: false
        )
        EmailTemplate.findTemplate(emailName, schedule.person.tagList, schedule.task.room)?.sendMail(
                [
                        person: schedule.person,
                        task: schedule.task,
                        schedule: schedule,
                        token: token
                ],
                schedule.person.emailEnabled,
                [
                        [id: schedule.person.id, name: 'person'],
                        [id: schedule.person.user?.id, name: 'user']
                ],
                schedule.person.smsEnabled)
        EmailTemplate.findTemplate('ADMIN_TASK_LATE', [], null)?.sendMail(
                [
                        person: schedule.person,
                        task: schedule.task,
                        schedule: schedule,
                        token: token
                ],
                true,
                [
                        [id: User.ADMIN_USER_ID, name: 'user']
                ])
    }
}
