package holder

import grails.gorm.transactions.Transactional
import org.grails.web.util.WebUtils
import org.springframework.context.i18n.LocaleContextHolder

//@Transactional
class MyValidateService {

    def messageSource

    def checkErrors(List objects) {
        def errors = objects.collect { o ->
            o.errors.allErrors.collect { e ->
                messageSource.getMessage(e, LocaleContextHolder.getLocale())
            }
        }.flatten()
        if (errors.size() <= 0) {
            def flash = WebUtils.retrieveGrailsWebRequest().flashScope
            flash.errors = null
        }
        return errors
    }
}
