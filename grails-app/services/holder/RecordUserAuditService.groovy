package holder

import grails.events.annotation.gorm.Listener
import org.grails.datastore.mapping.engine.event.PreInsertEvent
import org.grails.datastore.mapping.engine.event.PreUpdateEvent
import org.grails.datastore.mapping.engine.event.ValidationEvent

//@Transactional
class RecordUserAuditService {
    def springSecurityService

//    @Listener([Account,AccountEntry,Album,Booking,Comment,Config,Contract,EmailTemplate,HtmlTemplate,Person,Photo,Role,Room,Tip,User,UserRole,Task,ToDo,Subtask,TaskPerson,Schedule,Token,MailIndex])
//    void onPreInsert(PreInsertEvent event) {
//        User user = springSecurityService.currentUser
//        event.entityAccess.setProperty('userCreated', user)
//        event.entityAccess.setProperty('userUpdated', user)
//    }

    @Listener([Account,AccountEntry,Album,Booking,Comment,Config,Contract,EmailTemplate,HtmlTemplate,Person,Photo,Role,Room,Tip,User,UserRole,Task,ToDo,Subtask,TaskPerson,Schedule,Token,MailIndex])
    void onPreUpdate(PreUpdateEvent event) {
        User user = springSecurityService.currentUser
        if (user == null) {
            // The background job doesn't have a user from spring
            user = User.get(User.ADMIN_USER_ID)
        }
        event.entityAccess.setProperty('userUpdated', user)
    }

    @Listener([Account,AccountEntry,Album,Booking,Comment,Config,Contract,EmailTemplate,HtmlTemplate,Person,Photo,Role,Room,Tip,User,UserRole,Task,ToDo,Subtask,TaskPerson,Schedule,Token,MailIndex])
    void onValidate(ValidationEvent event) {
        User user = springSecurityService.currentUser
        if (user == null) {
            user = User.get(User.ADMIN_USER_ID)
        }
        if (!event.entityAccess.getProperty('userCreated')) {
            event.entityAccess.setProperty('userCreated', user)
        }
        if (!event.entityAccess.getProperty('userUpdated')) {
            event.entityAccess.setProperty('userUpdated', user)
        }
//        else if (event.entityObject.id && event.entityObject.dirty) {
//            event.entityAccess.setProperty('userUpdated', user)
//        }
    }
}
