package holder

import grails.gorm.services.Service

@Service(Booking)
interface BookingService {

    Booking get(Serializable id)

    List<Booking> list(Map args)

    List<Booking> findAll(Map args)

    Long count()

    void delete(Serializable id)

    Booking save(Booking Booking)

}
