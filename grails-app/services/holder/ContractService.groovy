package holder

import grails.gorm.services.Service

@Service(Contract)
interface ContractService {

    Contract get(Serializable id)

    List<Contract> list(Map args)

    List<Contract> findAll(Map args)

    Long count()

    void delete(Serializable id)

    Contract save(Contract Contract)

}
