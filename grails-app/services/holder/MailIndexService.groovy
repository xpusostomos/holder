package holder

import grails.gorm.transactions.Transactional
import grails.plugin.asyncmail.AsynchronousMailMessage

@Transactional
class MailIndexService {

    def create(AsynchronousMailMessage mail, EmailTemplate template, List < Map > items) {
        for (i in items) {
            if (mail && i.id) {
                new MailIndex(
                        mail: mail,
                        entityId: i.id,
                        entityName: i.name
                ).save()
            }
        }
    }
}
