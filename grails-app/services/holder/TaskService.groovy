package holder

import grails.gorm.services.Service

@Service(Schedule)
interface TaskService {

    Task get(Serializable id)

    List<Task> list(Map args)

    List<Task> findAll(Map args)

    Long count()

    void delete(Serializable id)

    Task save(Task schedule)

}
