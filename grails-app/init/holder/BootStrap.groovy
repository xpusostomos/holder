package holder

import grails.plugin.springsecurity.SecurityFilterPosition
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.util.Environment
import groovy.time.TimeCategory
import org.apache.commons.lang3.RandomStringUtils
import org.grails.plugins.sql.ScriptRunner

import javax.sql.DataSource
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime

class BootStrap {
    def messageSource
    def grailsApplication
//    DataSource dataSource

    def init = { servletContext ->
        AppConfig.printInfo()
        SpringSecurityUtils.clientRegisterFilter('tokenAuthenticationFilter', SecurityFilterPosition.SECURITY_CONTEXT_FILTER.order + 10)
        javax.servlet.http.HttpServletRequest.metaClass.getSiteUrl = {
            return (delegate.scheme + "://" + delegate.serverName + (delegate.serverPort == 80 ? '' : ':' + delegate.serverPort) + delegate.getContextPath())
        }

        TimeZone.setDefault(TimeZone.getTimeZone("Australia/Sydney"))
        if (grailsApplication.config.getProperty('dataSource.dbCreateDDL')?.startsWith('create')) {
            dropDDL()
            createDDL()
        }
        Person.withTransaction {
            if (Role.list().size() < 1) {
                createDefaultLogin()
            }
        }
        if (Environment.isDevelopmentMode() && Config.findAll().size() < 1) {
            createDefaultData()
        }




//        Task.withTransaction {
//            Task t = new Task(
//                    user: User.principal,
//                    type: 'foo',
//                    start: LocalDateTime.of(2020, 10, 1, 12, 0, 13),
//                    lastReminder: LocalDateTime.of(2020, 10, 1, 14, 0, 13),
//                    timeBetweenReminders: Duration.ofSeconds(100)
//            )
//            t.save()
//        }
    }

    def destroy = {
        if (grailsApplication?.config?.getProperty('dataSource.dbCreateDDL')?.equals('create-drop')) {
            dropDDL()
        }
    }

    static createDDL() {
        DataSource dataSource = grails.util.Holders.applicationContext.getBean('dataSource') as DataSource
        ScriptRunner sr = new ScriptRunner(dataSource.getConnection(), true)
        sr.runScript(new InputStreamReader(BootStrap.class.classLoader.getResource('sql/create.sql').openStream()))
    }

    static dropDDL() {
        DataSource dataSource = grails.util.Holders.applicationContext.getBean('dataSource') as DataSource
        ScriptRunner sr = new ScriptRunner(dataSource.getConnection(), true)
        sr.runScript(new InputStreamReader(BootStrap.class.classLoader.getResource('sql/drop.sql').openStream()))
    }

    static createDefaultLogin() {
//        Person.withTransaction {
//            if (Role.list().size() < 1) {
                Role admin = new Role(
                        authority: 'ROLE_ADMIN'
                )
                admin.save()
                Role userRole = new Role(
                        authority: 'ROLE_USER'
                )
                userRole.save()
                Role switchUserRole = new Role(
                        authority: 'ROLE_SWITCH_USER'
                )
                switchUserRole.save()
                Role guestRole = new Role(
                        authority: 'ROLE_GUEST'

                )
                guestRole.save()
                Role principalRole = new Role(
                        authority: 'ROLE_PRINCIPAL'

                )
                principalRole.save()
                User principal = new User(
                        username: 'room.turner',
                        email: 'room.turner@gmail.com',
                        firstName: 'Chris',
                        lastName: 'Bitmead',
                        passwd: 'gandalf'
                )
                principal.save()
//                Person chris = new Person(
//                        user: uchris,
//                        male: true,
//                       phone: '0416245269',
//                        employment: 'manager'
//                )
//                chris.save()
                UserRole.create(principal, admin)
                UserRole.create(principal, userRole)
                UserRole.create(principal, switchUserRole)
                UserRole.create(principal, principalRole)


                Config config = new Config(
                        name: ConfigNameEnum.STD_PERIOD_END_DATE,
                        date: LocalDate.of(2020, 12, 27)
                )
                config.save()
                config = new Config(
                        name: ConfigNameEnum.RENT_PERIOD,
                        number: 28
                )
                config.save()
                config = new Config(
                        name: ConfigNameEnum.REMINDER_PERIOD,
                        number: 3
                )
                config.save()
                config = new Config(
                        name: ConfigNameEnum.EMAIL_ENABLED,
                        bool: false
                )
                config.save()
                config = new Config(
                        name: ConfigNameEnum.EMAIL_REMINDER_ENABLED,
                        bool: false
                )
                config.save()
                Room premises = new Room(
                        level: RoomLevelEnum.PREMISES,
                        roomType: RoomTypeEnum.COLLECTION,
                        name: '7H',
                        description: '7 Holder St, Turner'
                )
        premises.save()
                Room mainHouse = new Room(
                        level: RoomLevelEnum.STRUCTURE,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: premises,
                        name: 'M',
                        description: 'Main House',
                )
                mainHouse.save()

                Room upstairs = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: mainHouse,
                        name: 'U',
                        description: 'Upstairs House',
                )
                mainHouse.addToRooms(upstairs)
                upstairs.save()
                Room u4 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: upstairs,
                        name: 'U4',
                        description: 'Rumpus',
                        available: true,
                        notes: """
Double bed, balcony overlooking the park.
Share bathroom with three other people.
"""
                )
                u4.save()
                upstairs.addToRooms(u4)
                Room u3 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: upstairs,
                        name: 'U3',
                        description: 'Small',
                        available: true,
                        notes: """
Single bed, north facing.
Share bathroom with three other people.
"""
                )
                u3.save()
                upstairs.addToRooms(u3)
                Room u2 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: upstairs,
                        name: 'U2',
                        description: 'Purple',
                        available: true,
                        notes: """
Large room, double bed, north facing balcony. 
Share bathroom with three other people.
"""
                )
                u2.save()
                upstairs.addToRooms(u2)
                Room u1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: upstairs,
                        name: 'U1',
                        description: 'Blue',
                        available: true,
                        notes: """
Large room, double bed, north facing balcony. 
Share bathroom with three other people.
"""
                )
                u1.save()
                upstairs.addToRooms(u1)

                Room downstairs = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: mainHouse,
                        name: 'D',
                        description: 'Downstairs House',
                )
                mainHouse.addToRooms(downstairs)
                downstairs.save()
                Room d1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: downstairs,
                        name: 'D1',
                        description: 'Lounge',
                        available: true,
                        notes: """
Large room, king bed, ground floor overlooking the park.
Share bathroom with one other person.
"""
                )
                d1.save()
                downstairs.addToRooms(d1)


                Room backHouse = new Room(
                        level: RoomLevelEnum.STRUCTURE,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: premises,
                        name: 'S',
                        description: 'Secondary House',
                )
                backHouse.save()


                Room extension = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: backHouse,
                        name: 'E',
                        description: 'Extension',
                )
                backHouse.addToRooms(extension)
                extension.save()
                Room e1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: extension,
                        name: 'E1',
                        description: 'Extension 1',
                        available: true,
                        notes: """
Large room, queen bed, ground floor.
Share bathroom with one other person.
"""
                )
                e1.save()
                extension.addToRooms(e1)
                Room e2 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: extension,
                        name: 'E2',
                        description: 'Extension 2',
                        available: true,
                        notes: """
Large room, queen bed, ground floor, north facing.
Share bathroom with one other person.
"""
                )
                e2.save()
                extension.addToRooms(e2)
                Room e3 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: extension,
                        name: 'E3',
                        description: 'Extension 3',
                        available: true,
                        notes: """
Large room, queen bed, ground floor, north facing.
Share bathroom with one other person.
"""
                )
                e3.save()
                extension.addToRooms(e3)
                Room e4 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: extension,
                        name: 'E4',
                        description: 'Extension 4',
                        available: true,
                        notes: """
Large room, queen bed, ground floor.
Share bathroom with one other person.
"""
                )
                e4.save()
                extension.addToRooms(e4)

                Room granny = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: backHouse,
                        name: 'G',
                        description: 'Granny Flat',
                )
                backHouse.addToRooms(granny)
                granny.save()
                Room g2 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: granny,
                        name: 'G2',
                        description: 'Granny 2',
                        available: true,
                        notes: """
Double bed, ground floor, north facing, private ensuite.
"""
                )
                g2.save()
                granny.addToRooms(g2)
                Room g3 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: granny,
                        name: 'G3',
                        description: 'Granny 3',
                        available: true,
                        notes: """
Double bed, ground floor, north facing, private ensuite.
"""
                )
                g3.save()
                granny.addToRooms(g3)
                Room g1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: granny,
                        name: 'G1',
                        description: 'Granny 1',
                        available: true,
                        notes: """
Large room, queen bed, ground floor, private ensuite.
"""
                )
                g1.save()
                granny.addToRooms(g1)
                Room g4 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.ROOM,
                        parent: extension,
                        name: 'G4',
                        description: 'Granny 4',
                        available: true,
                        notes: """
Large room, single bed, ground floor, large private ensuite.
"""
                )
                g4.save()
                granny.addToRooms(g4)


                Room parking = new Room(
                        level: RoomLevelEnum.STRUCTURE,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: premises,
                        name: 'P',
                        description: 'Parking',
                )
        premises.addToRooms(parking)
                parking.save()
                Room frontParking = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: parking,
                        name: 'F',
                        description: 'Front Parking',
                )
                parking.addToRooms(frontParking)
                frontParking.save()
                Room f1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.CARSPOT,
                        parent: frontParking,
                        name: 'F1',
                        description: 'Front Parking 1',
                        available: false,
                        notes: """
Front parking near garage
"""
                )
                f1.save()
                frontParking.addToRooms(f1)
                Room f2 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.CARSPOT,
                        parent: frontParking,
                        name: 'F2',
                        description: 'Front Parking 2',
                        available: false,
                        notes: """
Front parking near street
"""
                )
                f2.save()
                frontParking.addToRooms(f2)


                Room rearParking = new Room(
                        level: RoomLevelEnum.SECTION,
                        roomType: RoomTypeEnum.COLLECTION,
                        parent: parking,
                        name: 'R',
                        description: 'Rear Parking',
                )
                parking.addToRooms(rearParking)
                frontParking.save()
                Room r1 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.CARSPOT,
                        parent: frontParking,
                        name: 'R1',
                        description: 'Rear Parking 1',
                        available: false,
                        notes: """
Rear parking left
"""
                )
                r1.save()
                rearParking.addToRooms(r1)
                Room r2 = new Room(
                        level: RoomLevelEnum.ROOM,
                        roomType: RoomTypeEnum.CARSPOT,
                        parent: frontParking,
                        name: 'R2',
                        description: 'Rear Parking 2',
                        available: false,
                        notes: """
Rear parking right
"""
                )
                r2.save()
                rearParking.addToRooms(r2)



                Tip cleanFloor = new Tip(
                        type: TipTypeEnum.CLEAN,
                        category: TipCategory.CLEANING,
                        code: 'CleanFloor',
                        brief: 'Clean floors',
                        text: 'Vacuum if carpet, mop if tiles. Consult landlord if you need supplies.',
                        room: premises
                )
                cleanFloor.save()

                Tip garbageDays = new Tip(
                        type: TipTypeEnum.TIP,
                        category: TipCategory.GARBAGE,
                        code: 'GarbageDays',
                        brief: 'Garbage collection days',
                        text: 'Garbage (red bin) is collected every week Friday morning and recycle (yellow bin) every two weeks. Search for Bin Collection Calendar at cityservices.act.gov.au for exact days',
                        room: premises
                )
                garbageDays.save()
                Tip recycle = new Tip(
                        type: TipTypeEnum.TIP,
                        category: TipCategory.GARBAGE,
                        code: 'Recycle',
                        brief: 'What to recycle',
                        text: 'The yellow waste bin is exclusively for recycle. If you want to know what is considered recycle, search for Recyclopaedia A-Z Listing at cityservices.act.gov.au',
                        room: premises
                )
                recycle.save()
//            }
            Account assets = new Account(id: 10000, name: 'Assets')
            assets.save()
            Account debtors = new Account(id: 11230, name: 'Rent Debtors', parent: assets)
            debtors.save()
            Account bank = new Account(id: 11120, name: 'Bank', parent: assets)
            bank.save()
            Account cash = new Account(id: 11160, name: 'Cash', parent: assets)
            cash.save()
            Account liabilities = new Account(id: 20000, name: 'Liabilities')
            liabilities.save()
            Account bondHeld = new Account(id: 21140, name: 'Bond Held', parent: liabilities)
            bondHeld.save()
            Account equity = new Account(id: 30000, name: 'Equity')
            equity.save()
            Account income = new Account(id: 40000, name: 'Equity')
            income.save()
            Account rentIncome = new Account(id: 45030, name: 'Rental Income', parent: income)
            rentIncome.save()
            Account fees = new Account(id: 44040, name: 'Fees')
            fees.save()
            Account expenses = new Account(id: 60000, name: 'Expenses')
            expenses.save()
            Account writeoff = new Account(id: 60740, name: 'Writeoff', parent: expenses)
            writeoff.save()
//        }
    }

    static createDefaultData() {
        Person.withTransaction {
            User principle = User.principal
            Role userRole = Role.findByAuthority('ROLE_USER')
            Role guestRole = Role.findByAuthority('ROLE_GUEST')
            Room a1 = Room.findByName("U1")
            Room a2 = Room.findByName("U2")
            Room b1 = Room.findByName("D1")

            User ufred = new User(
                    email: 'xpusostomos@gmail.com',
                    firstName: 'Fred',
                    lastName: 'Smith',
                    passwd: 'asdfasdfasdf'
            )
            ufred.save()
            UserRole.create(ufred, userRole)
            UserRole.create(ufred, guestRole)
            Person fred = new Person(
                    user: ufred,
                    sex: SexEnum.MALE,
                    phone: '041699999999',
                    employment: 'student'
            )
            fred.save()
            User ubill = new User(
                    email: 'ccthecc@gmail.com',
                    firstName: 'Bill',
                    lastName: 'Bloggs',
                    passwd: 'asdfasdfasdf'
            )
            ubill.save()
            UserRole.create(ubill, userRole)
            UserRole.create(ubill, guestRole)
            Person bill = new Person(
                    user: ubill,
                    sex: SexEnum.COUPLE,
                    phone: '041699999999',
                    employment: 'student'
            )
            bill.save()
            User ujill = new User(
                    email: 'idou747@gmail.com',
                    firstName: 'Jill',
                    lastName: 'Jamison',
                    passwd: 'asdfasdfasdf'
            )
            ujill.save()
            UserRole.create(ujill, userRole)
            UserRole.create(ujill, guestRole)
            Person jill = new Person(
                    user: ujill,
                    sex: SexEnum.FEMALE,
                    phone: '041699999999',
                    employment: 'student'
            )
            jill.save()
            Comment query = new Comment(
                    from: ujill,
                    to: principle,
                    text: 'Hot water doesnt work'
            )
            query.save()
            Comment comment = new Comment(
                    from: principle,
                    to: ujill,
                    parent: query,
                    text: 'Kawawa!'
            )
            comment.save()
            Contract contract = new Contract(
                    checkIn: LocalDateTime.of(2020, 11, 29, 12, 0),
                    checkOut: LocalDateTime.of(2021, 3, 15, 12, 0),
                    rent: 840.00,
                    bond: 840.00,
                    rentPeriod: 28,
                    noticeGiven: LocalDateTime.of(2021, 1, 1, 12, 0),
                    moveOut: LocalDateTime.of(2021, 1, 20, 12, 0),
                    person: fred,
                    room: a1,
                    agreed: true,
                    passKey: RandomStringUtils.randomAlphabetic(16)
            )
            contract.save()
            contract.generateAndPersistFees()
            contract = new Contract(
                    checkIn: LocalDateTime.of(2021, 1, 20, 12, 0),
                    checkOut: LocalDateTime.of(2021, 9, 20, 12, 0),
                    rent: 840.00,
                    bond: 840.00,
                    rentPeriod: 28,
                    person: jill,
                    room: a2,
                    agreed: true,
                    passKey: RandomStringUtils.randomAlphabetic(16)
            )
            contract.save()
            contract.generateAndPersistFees()
            contract = new Contract(
                    checkIn: LocalDateTime.of(2020, 11, 30, 12, 0),
                    checkOut: LocalDateTime.of(2021, 3, 15, 12, 0),
                    rent: 840.00,
                    bond: 840.00,
                    rentPeriod: 28,
                    person: bill,
                    room: b1,
                    agreed: true,
                    passKey: RandomStringUtils.randomAlphabetic(16)
            )
            contract.save()
            contract.generateAndPersistFees()
            AccountEntry ae = new AccountEntry(
                    person: jill,
                    type: AccountEntryTypeEnum.PAYMENT,
                    method: AccountEntryMethodEnum.CASH,
                    amount: -990.00,
                    description: 'Payment Received',
                    date: LocalDate.of(2021, 01, 20)
            )
            ae.save()
            ae = new AccountEntry(
                    person: jill,
                    type: AccountEntryTypeEnum.PAYMENT,
                    method: AccountEntryMethodEnum.BANK,
                    amount: -840.00,
                    description: 'Payment Received',
                    date: LocalDate.of(2021, 02, 22)
            )
            ae.save()
            Booking booking = new Booking(
                    firstName: 'Larry',
                    lastName: 'Lightfoot',
                    email: 'idou747@gmail.com',
                    phone: '02 99999999',
                    checkIn: LocalDate.of(2021, 03, 01),
                    checkOut: LocalDate.of(2021, 10, 02),
                    employment: 'Foreign relations student',
                    sex: SexEnum.MALE,
                    dateInspection: LocalDateTime.of(2021, 01, 22, 10, 00),
                    applyNotes: 'Pick me! Pick me!',
                    notes: 'bit of a jerk'
            )
            booking.save()
        }

    }
}

