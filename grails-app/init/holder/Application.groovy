package holder

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import org.grails.config.yaml.YamlPropertySourceLoader
import org.springframework.core.env.Environment
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.MapPropertySource
import org.springframework.core.env.PropertySource
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import java.nio.file.FileSystems
import java.nio.file.Path

class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }
}

