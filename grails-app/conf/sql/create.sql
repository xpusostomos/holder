create sequence hibernate_sequence start 1 increment 1;

create table account (
  id int8 not null,
  parent_id int8,
  name varchar(255) not null,
  offset_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table account_entry (
  id int8 default nextval('hibernate_sequence') not null,
  type varchar(255),
  method varchar(255),
  period_begin date,
  period_end date,
  date date not null,
  debit_account_id int8 not null,
  credit_account_id int8 not null,
  amount numeric(19, 2) not null,
  description varchar(255),
  notes varchar(255),
  person_id int8,
  contract_id int8 not null,
  room_id int8,
  property_id int8,
  source_id int8,
  depreciation_method varchar(255),
  depreciation_rate numeric(19,6),
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));
   
create table album (
  id int8 default nextval('hibernate_sequence') not null,
  type varchar(255) not null,
  name varchar(255) not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table async_mail_attachment (
  id int8 default nextval('hibernate_sequence') not null,
  attachments_idx int4,
  message_id int8 not null,
  mime_type varchar(255) not null,
  inline boolean not null,
  attachment_name varchar(255) not null,
  content bytea not null,
primary key (id));

create table async_mail_bcc (
  message_id int8 not null,
  bcc_idx int4,
  bcc_string varchar(256)
);

create table async_mail_cc (
  message_id int8 not null,
  cc_idx int4,
  cc_string varchar(256)
);

create table async_mail_header (
  message_id int8 not null,
  header_name varchar(255),
  header_value varchar(255)
);

create table async_mail_mess (
  id int8 default nextval('hibernate_sequence') not null,
  from_column varchar(256),
  reply_to varchar(256),
  envelope_from varchar(256),
  subject varchar(988) not null,
  text text not null,
  alternative text,
  html boolean not null,
  status varchar(255) not null,
  sent_date timestamp,
  begin_date timestamp not null,
  end_date timestamp not null,
  priority int4 not null,
  attempts_count int4 not null,
  max_attempts_count int4 not null,
  last_attempt_date timestamp,
  attempt_interval int8 not null,
  mark_delete boolean not null,
  mark_delete_attachments boolean not null,
  external_reference int8
  date_created timestamp default current_timestamp not null,
  version int8 default 0 not null,
primary key (id));

create table async_mail_to (
  message_id int8 not null,
  to_idx int4,
  to_string varchar(256)
);

create table booking (
  id int8 default nextval('hibernate_sequence') not null,
  email varchar(254) not null,
  first_name varchar(255) not null,
  last_name varchar(255) not null,
  phone varchar(255),
  sex varchar(255) not null, 
  employment varchar(255) not null,
  check_out date not null,
  check_in date not null,
  date_inspection timestamp,
  date_responded date,
  date_decided date,
  positive_decision boolean default false not null,
  confirmation_code varchar(255),
  apply_notes varchar(255),
  notes varchar(255),
  room_id int8,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table comment (
  id int8 default nextval('hibernate_sequence') not null,
  from_id int8 not null,
  to_id int8,
  closed boolean default false not null,
  text text not null,
  parent_id int8,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));
  
create table config (
  id int8 default nextval('hibernate_sequence') not null,
  name varchar(255) not null,
  string varchar(255),
  date date,
  number numeric(19, 2),
  bool bool,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table contract (
  id int8 default nextval('hibernate_sequence') not null,
  check_in timestamp not null,
  check_out timestamp not null,
  room_id int8 not null,
  car_spot_id int8,
  bond numeric(19, 2) not null,
  rent numeric(19, 2) not null,
  checkout_day_is_paid default true not null,
  rent_period int8 default 28 not null,
  use_std_pay_dates boolean default true not null,
  contract_terms varchar(32672),
  agreed boolean default false not null,
  notice_given timestamp,
  move_out timestamp,
  closed boolean default false not null,
  notes varchar(255),
  booking_id int8,
  predecessor_id int8,
  person_id int8,
  album_id int8,
  agent_id int8,
  pass_key varchar(255),
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table email_template (
  id int8 default nextval('hibernate_sequence') not null,
  name varchar(255) not null,
  room_id int8,
  to_address varchar(255),
  cc_address varchar(255),
  bcc_address varchar(255),
  tags varchar(512),
  subject varchar(78) not null,
  body text not null,
  indexes varchar(255),
  attachments_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table mail_index (
  id int8 default nextval('hibernate_sequence') not null,
  mail_id int8 not null,
  entity_id int8 not null,
  entity_name varchar(255) not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table html_template (
  id int8 default nextval('hibernate_sequence') not null,
  parent_id int8,
  name varchar(255) not null,
  room_id int8,
  body text not null,
  attachments_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table person (
  id int8 default nextval('hibernate_sequence') not null,
  email varchar(254),
  first_name varchar(255) not null,
  last_name varchar(255),
  type varchar(255) not null,
  sex varchar(255) not null,
  billing_email varchar(254),
  billing_first_name varchar(255),
  billing_last_name varchar(255),
  phone varchar(255),
  employment varchar(255) not null,
  email_enabled bool default true not null,
  abn varchar(11),
  String tags varchar(512),
  notes varchar(255),
  user_id int8,
  payment_notified bool default false not null,
  waiting_on_payment bool default false not null,
  avatar_id int8,
  passport_id int8,
  photo_id int8,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table photo (
  id int8 default nextval('hibernate_sequence') not null,
  type varchar(255) not null,
  name varchar(255) not null,
  mime_type varchar(127),
  album_id int8,
  data bytea not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table registration_code (
  id int8 default nextval('hibernate_sequence') not null,
  username varchar(255) not null,
  token varchar(255) not null,
  date_created timestamp default current_timestamp not null,
primary key (id));

create table role (
  id int8 default nextval('hibernate_sequence') not null,
  authority varchar(255) not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table room (
  id int8 default nextval('hibernate_sequence') not null,
  level int4 not null,
  room_type varchar(255) not null,
  name varchar(255) not null,
  available boolean default false not null,
  cleaning_fee numeric(19, 2) default 0 not null,
  description varchar(255) not null,
  notes varchar(255),
  parent_id int8,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table tip (
  id int8 default nextval('hibernate_sequence') not null,
  type varchar(255) not null,
  category varchar(255) not null,
  code varchar(255) not null,
  brief varchar(255) not null,
  text text not null,
  weighting float4 default 1.0 not null,
  room_id int8,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table usr (
  id int8 default nextval('hibernate_sequence') not null,
  username varchar(255) not null,
  email varchar(254) not null,
  first_name varchar(255) not null,
  last_name varchar(255) not null,
  passwd varchar(255) not null,
  enabled boolean default true not null,
  account_expired boolean default false not null,
  account_locked boolean default false not null,
  password_expired boolean default false not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table usr_role (
  user_id int8 not null,
  role_id int8 not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
primary key (user_id, role_id));

create table usr_room (
  user_id int8 not null,
  room_id int8 not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
primary key (user_id, room_id));

create table token (
  id int8 default nextval('hibernate_sequence') not null,
  user_id int8 not null,
  token varchar(255) default MD5(random()::text) not null,
  expiry timestamp default 'tomorrow'::TIMESTAMP not null,
  one_time boolean default true not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table schedule (
  id int8 default nextval('hibernate_sequence') not null,
  start timestamp not null,
  cancelled boolean not null,
  last_reminder timestamp,
  person_id int8 not null,
  complete boolean not null,
  schedule_id int8 not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table subtask (
  id int8 default nextval('hibernate_sequence') not null,
  order_by int4 not null,
  description varchar(255) not null,
  task_id int8 not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table task (
  id int8 default nextval('hibernate_sequence') not null,
  version int8 not null,
  due timestamp not null,
  calculate_ahead int4 not null,
  remind_early_interval int8 not null,
  remind_late_interval int8 not null,
  schedule_interval int8 not null,
  name varchar(255) not null,
  description varchar(255) not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table task_person (
  id int8 default nextval('hibernate_sequence') not null,
  person_id int8 not null,
  order_by int4 not null,
  schedule_id int8 not null,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create table to_do (
  id int8 default nextval('hibernate_sequence') not null,
  summary varchar(256) not null,
  text varchar(4000),
  room_id int8,
  person_id int8,
  deadline timestamp,
  priority numeric(19, 2) default 1.0 not null,
  complete boolean default false not null,
  album_id int8,
  date_created timestamp default current_timestamp not null,
  user_created_id int8 default 1 not null,
  last_updated timestamp default current_timestamp not null,
  user_updated_id int8 default 1 not null,
  version int8 default 0 not null,
primary key (id));

create index predecessor_idx on contract (predecessor_id);
create index async_mail_mess_status_idx on async_mail_mess (status);
create index async_mail_mess_external_reference_idx on async_mail_mess (external_reference);

alter table if exists role add constraint role_authority_UK unique (authority);
alter table if exists role add constraint role_user_created_FK foreign key (user_created_id) references usr;
alter table if exists role add constraint role_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists usr add constraint usr_username_UK unique (username);
alter table if exists usr add constraint usr_email_UK unique (email);
alter table if exists usr add constraint usr_user_created_FK foreign key (user_created_id) references usr;
alter table if exists usr add constraint usr_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists account add constraint account_user_created_FK foreign key (user_created_id) references usr;
alter table if exists account add constraint account_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists account_entry add constraint account_entry_contract_FK foreign key (contract_id) references contract;
alter table if exists account_entry add constraint account_entry_person_FK foreign key (person_id) references person;
alter table if exists account_entry add constraint account_entry_account_debit_FK foreign key (debit_account_id) references account;
alter table if exists account_entry add constraint account_entry_account_credit_FK foreign key (credit_account_id) references account;
alter table if exists account_entry add constraint account_entry_room_FK foreign key (room_id) references room;
alter table if exists account_entry add constraint account_entry_property_FK foreign key (property_id) references room;
alter table if exists account_entry add constraint account_entry_user_created_FK foreign key (user_created_id) references usr;
alter table if exists account_entry add constraint account_entry_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists account_entry add constraint account_entry_source_FK foreign key (source_id) references account_entry;


alter table if exists async_mail_attachment add constraint async_mail_attachment_async_mail_mess_FK foreign key (message_id) references async_mail_mess;

alter table if exists booking add constraint booking_room_FK foreign key (room_id) references room;
alter table if exists booking add constraint booking_album_FK foreign key (album_id) references album;
alter table if exists booking add constraint booking_user_created_FK foreign key (user_created_id) references usr;
alter table if exists booking add constraint booking_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists comment add constraint comment_usr_FK foreign key (from_id) references usr;
alter table if exists comment add constraint comment_album_FK foreign key (album_id) references album;
alter table if exists comment add constraint comment_usr_to_FK foreign key (to_id) references usr;
alter table if exists comment add constraint comment_parent_FK foreign key (parent_id) references comment;
alter table if exists comment add constraint comment_user_created_FK foreign key (user_created_id) references usr;
alter table if exists comment add constraint comment_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists contract add constraint contract_booking_FK foreign key (booking_id) references booking;
alter table if exists contract add constraint contract_predecessor_FK foreign key (predecessor_id) references contract;
alter table if exists contract add constraint contract_car_spot_FK foreign key (car_spot_id) references room;
alter table if exists contract add constraint contract_room_FK foreign key (room_id) references room;
alter table if exists contract add constraint contract_person_FK foreign key (person_id) references person;
alter table if exists contract add constraint contract_agent_FK foreign key (agent_id) references person;
alter table if exists contract add constraint contract_album_FK foreign key (album_id) references album;
alter table if exists contract add constraint contract_user_created_FK foreign key (user_created_id) references usr;
alter table if exists contract add constraint contract_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists email_template add constraint email_template_room_FK foreign key (room_id) references room;
alter table if exists email_template add constraint email_template_album_FK foreign key (attachments_id) references album;
alter table if exists email_template add constraint email_template_user_created_FK foreign key (user_created_id) references usr;
alter table if exists email_template add constraint email_template_user_updated_FK foreign key (user_updated_id) references usr;


alter table if exists mail_index add constraint mail_index_mail_entity_UK unique (mail_id, entity_id, entity_name);
alter table if exists mail_index add constraint mail_index_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists mail_index add constraint mail_index_user_created_FK foreign key (user_created_id) references usr;
alter table if exists mail_index add constraint mail_index_async_mail_mess_FK foreign key (mail_id) references async_mail_mess;

alter table if exists html_template add constraint html_template_room_FK foreign key (room_id) references room;
alter table if exists html_template add constraint html_template_album_FK foreign key (attachments_id) references album;
alter table if exists html_template add constraint html_template_parent_FK foreign key (parent_id) references html_template;
alter table if exists html_template add constraint html_template_user_created_FK foreign key (user_created_id) references usr;
alter table if exists html_template add constraint html_template_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists person add constraint person_photo_FK foreign key (photo_id) references photo;
alter table if exists person add constraint person_usr_FK foreign key (user_id) references usr;
alter table if exists person add constraint person_album_FK foreign key (album_id) references album;
alter table if exists person add constraint person_passport_FK foreign key (passport_id) references photo;
alter table if exists person add constraint person_avatar_FK foreign key (avatar_id) references photo;
alter table if exists person add constraint person_user_created_FK foreign key (user_created_id) references usr;
alter table if exists person add constraint person_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists photo add constraint photo_album_FK foreign key (album_id) references album;
alter table if exists photo add constraint photo_user_created_FK foreign key (user_created_id) references usr;
alter table if exists photo add constraint photo_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists room add constraint room_album_FK foreign key (album_id) references album;
alter table if exists room add constraint room_parent_FK foreign key (parent_id) references room;
alter table if exists room add constraint room_user_created_FK foreign key (user_created_id) references usr;
alter table if exists room add constraint room_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists tip add constraint tip_room_FK foreign key (room_id) references room;
alter table if exists tip add constraint tip_album_FK foreign key (album_id) references album;
alter table if exists tip add constraint tip_user_created_FK foreign key (user_created_id) references usr;
alter table if exists tip add constraint tip_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists usr_role add constraint usr_role_usr_FK foreign key (user_id) references usr;
alter table if exists usr_role add constraint usr_role_role_FK foreign key (role_id) references role;
alter table if exists usr_role add constraint usr_role_user_created_FK foreign key (user_created_id) references usr;
alter table if exists usr_role add constraint usr_role_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists usr_room add constraint usr_room_usr_FK foreign key (user_id) references usr;
alter table if exists usr_room add constraint usr_room_room_FK foreign key (room_id) references room;
alter table if exists usr_room add constraint usr_room_user_created_FK foreign key (user_created_id) references usr;
alter table if exists usr_room add constraint usr_room_user_updated_FK foreign key (user_updated_id) references usr;

create index token_expiry_idx on token (expiry);
create index token_token_idx on token (token);
alter table if exists token add constraint token_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists token add constraint token_user_FK foreign key (user_id) references usr;
alter table if exists token add constraint token_user_created_FK foreign key (user_created_id) references usr;

alter table if exists config add constraint config_user_created_FK foreign key (user_created_id) references usr;
alter table if exists config add constraint config_user_updated_FK foreign key (user_updated_id) references usr;

alter table if exists schedule add constraint schedule_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists schedule add constraint schedule_user_created_FK foreign key (user_created_id) references usr;
alter table if exists schedule add constraint schedule_person_FK foreign key (person_id) references person;
alter table if exists schedule add constraint schedule_task_FK foreign key (schedule_id) references task;
alter table if exists subtask add constraint subtask_task_FK foreign key (task_id) references task;
alter table if exists subtask add constraint subtask_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists subtask add constraint subtask_user_created_FK foreign key (user_created_id) references usr;
alter table if exists task add constraint task_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists task add constraint task_user_created_FK foreign key (user_created_id) references usr;
alter table if exists task_person add constraint task_person_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists task_person add constraint task_person_person_FK foreign key (person_id) references person;
alter table if exists task_person add constraint task_person_task_FK foreign key (task_id) references task;
alter table if exists task_person add constraint task_person_user_updated_FK foreign key (user_created_id) references usr;
alter table if exists to_do add constraint to_do_user_updated_FK foreign key (user_updated_id) references usr;
alter table if exists to_do add constraint to_do_user_created_FK foreign key (user_created_id) references usr;
alter table if exists to_do add constraint to_do_room_FK foreign key (room_id) references room;
alter table if exists to_do add constraint to_do_person_FK foreign key (person_id) references person;
alter table if exists to_do add constraint to_do_album_FK foreign key (album_id) references album;
