alter table if exists role drop constraint role_authority_UK;
alter table if exists role drop constraint role_user_created_FK;
alter table if exists role drop constraint role_user_updated_FK;

alter table if exists usr drop constraint usr_username_UK;
alter table if exists usr drop constraint usr_email_UK;
alter table if exists usr drop constraint usr_user_created_FK;
alter table if exists usr drop constraint usr_user_updated_FK;

alter table if exists account drop constraint account_user_created_FK;
alter table if exists account drop constraint account_user_updated_FK;

alter table if exists account_entry drop constraint account_entry_contract_FK;
alter table if exists account_entry drop constraint account_entry_person_FK;
alter table if exists account_entry drop constraint account_entry_account_debit_FK;
alter table if exists account_entry drop constraint account_entry_account_credit_FK;
alter table if exists account_entry drop constraint account_entry_user_created_FK;
alter table if exists account_entry drop constraint account_entry_user_updated_FK;
alter table if exists account_entry drop constraint account_entry_source_FK;

alter table if exists async_mail_attachment drop constraint async_mail_attachment_async_mail_mess_FK;

alter table if exists booking drop constraint booking_room_FK;
alter table if exists booking drop constraint booking_album_FK;
alter table if exists booking drop constraint booking_user_created_FK;
alter table if exists booking drop constraint booking_user_updated_FK;

alter table if exists comment drop constraint comment_usr_FK;
alter table if exists comment drop constraint comment_album_FK;
alter table if exists comment drop constraint comment_usr_to_FK;
alter table if exists comment drop constraint comment_parent_FK;
alter table if exists comment drop constraint comment_user_created_FK;
alter table if exists comment drop constraint comment_user_updated_FK;


alter table if exists contract drop constraint contract_booking_FK;
alter table if exists contract drop constraint contract_predecessor_FK;
alter table if exists contract drop constraint contract_car_spot_FK;
alter table if exists contract drop constraint contract_room_FK;
alter table if exists contract drop constraint contract_person_FK;
alter table if exists contract drop constraint contract_agent_FK;
alter table if exists contract drop constraint contract_album_FK;
alter table if exists contract drop constraint contract_user_created_FK;
alter table if exists contract drop constraint contract_user_updated_FK;

alter table if exists email_template drop constraint email_template_room_FK;
alter table if exists email_template drop constraint email_template_album_FK;
alter table if exists email_template drop constraint email_template_user_created_FK;
alter table if exists email_template drop constraint email_template_user_updated_FK;

alter table if exists mail_index drop constraint mail_index_email_template_FK;
alter table if exists mail_index drop constraint mail_index_user_updated_FK;
alter table if exists mail_index drop constraint mail_index_user_created_FK;
alter table if exists mail_index drop constraint mail_index_async_mail_mess_FK;

alter table if exists html_template drop constraint html_template_room_FK;
alter table if exists html_template drop constraint html_template_album_FK;
alter table if exists html_template drop constraint html_template_parent_FK;
alter table if exists html_template drop constraint html_template_user_created_FK;
alter table if exists html_template drop constraint html_template_user_updated_FK;

alter table if exists person drop constraint person_photo_FK;
alter table if exists person drop constraint person_usr_FK;
alter table if exists person drop constraint person_album_FK;
alter table if exists person drop constraint person_passport_FK;
alter table if exists person drop constraint person_avatar_FK;
alter table if exists person drop constraint person_user_created_FK;
alter table if exists person drop constraint person_user_updated_FK;

alter table if exists photo drop constraint photo_album_FK;
alter table if exists photo drop constraint photo_user_created_FK;
alter table if exists photo drop constraint photo_user_updated_FK;

alter table if exists room drop constraint room_album_FK;
alter table if exists room drop constraint room_parent_FK;
alter table if exists room drop constraint room_user_created_FK;
alter table if exists room drop constraint room_user_updated_FK;

alter table if exists tip drop constraint tip_room_FK;
alter table if exists tip drop constraint tip_album_FK;
alter table if exists tip drop constraint tip_user_created_FK;
alter table if exists tip drop constraint tip_user_updated_FK;

alter table if exists usr_role drop constraint usr_role_usr_FK;
alter table if exists usr_role drop constraint usr_role_role_FK;
alter table if exists usr_role drop constraint usr_role_user_created_FK;
alter table if exists usr_role drop constraint usr_role_user_updated_FK;

alter table if exists usr_room drop constraint usr_room_usr_FK;
alter table if exists usr_room drop constraint usr_room_room_FK;
alter table if exists usr_room drop constraint usr_room_user_created_FK;
alter table if exists usr_room drop constraint usr_room_user_updated_FK;

alter table if exists token drop constraint token_user_updated_FK;
alter table if exists token drop constraint token_user_FK;
alter table if exists token drop constraint token_user_created_FK;

alter table if exists config drop constraint config_user_created_FK;
alter table if exists config drop constraint config_user_updated_FK;

alter table if exists schedule drop constraint schedule_user_updated_FK;
alter table if exists schedule drop constraint schedule_user_created_FK;
alter table if exists schedule drop constraint schedule_person_FK;
alter table if exists schedule drop constraint schedule_task_FK;
alter table if exists subtask drop constraint subtask_task_FK;
alter table if exists subtask drop constraint subtask_user_updated_FK;
alter table if exists subtask drop constraint subtask_user_created_FK;
alter table if exists task drop constraint task_user_updated_FK;
alter table if exists task drop constraint task_user_created_FK;
alter table if exists task_person drop constraint task_person_user_updated_FK;
alter table if exists task_person drop constraint task_person_person_FK;
alter table if exists task_person drop constraint task_person_task_FK;
alter table if exists task_person drop constraint task_person_user_updated_FK;

alter table if exists to_do drop constraint to_do_user_updated_FK;
alter table if exists to_do drop constraint to_do_user_created_FK;
alter table if exists to_do drop constraint to_do_room_FK;
alter table if exists to_do drop constraint to_do_person_FK;
alter table if exists to_do drop constraint to_do_album_FK;
