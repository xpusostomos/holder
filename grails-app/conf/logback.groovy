appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern =
                '%level %logger %message%n%xException'
    }
}
logger("StackTrace", ERROR, ['STDOUT'], false)
logger("au.gov", DEBUG, ['STDOUT'], false)
logger("au.org.biodiversity", DEBUG, ['STDOUT'], false)
logger("grails.plugin.externalconfig", INFO, ['STDOUT'], false)
logger("org.hibernate.orm.deprecation", OFF)
root(WARN, ['STDOUT'])

//import grails.util.Environment
//import org.springframework.boot.logging.logback.ColorConverter
//import org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter
//
//import java.nio.charset.Charset
//
//conversionRule 'clr', ColorConverter
//conversionRule 'wex', WhitespaceThrowableProxyConverter
//
//// See http://logback.qos.ch/manual/groovy.html for details on configuration
//
//def targetDir = System.getProperty('catalina.base') ?: 'build'
//
//appender('STDOUT', ConsoleAppender) {
//    encoder(PatternLayoutEncoder) {
//        charset = Charset.forName('UTF-8')
//
//        pattern =
//                '%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} ' + // Date
//                        '%clr(%5p) ' + // Log level
//                        '%clr(---){faint} %clr([%15.15t]){faint} ' + // Thread
//                        '%clr(%-40.40logger{39}){cyan} %clr(:){faint} ' + // Logger
//                        '%m%n%wex' // Message
//    }
//}
//logger("StackTrace", ERROR, ['STDOUT'], true)
//logger("au.gov", DEBUG, ['STDOUT'])
//logger("org.hibernate.orm.deprecation", OFF)
//logger("org.grails.config.NavigableMap", OFF)
//root(WARN, ['STDOUT'])
//
