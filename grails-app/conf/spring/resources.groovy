import holder.UserPasswordEncoderListener
// Place your Spring DSL code here
beans = {
    userPasswordEncoderListener(UserPasswordEncoderListener)
    dateValueConverter holder.DateValueConverter
    dateTimeValueConverter holder.DateTimeValueConverter
    durationValueConverter holder.DurationValueConverter
    noopAuthenticationSuccessHandler holder.TokenAuthenticationSuccessHandler
    tokenAuthenticationProvider (holder.TokenAuthenticationProvider) {
        userDetailsService = ref('userDetailsService')
    }
    tokenAuthenticationFilter (holder.TokenAuthenticationFilter) {
        authenticationManager = ref('authenticationManager')
        rememberMeServices = ref('rememberMeServices')
        authenticationSuccessHandler = ref('noopAuthenticationSuccessHandler')
    }
}
