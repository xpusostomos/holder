

// Added by the Spring Security Core plugin:
//grails.plugin.springsecurity.userLookup.userDomainClassName = 'holder.User'
//grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'holder.UserRole'
//grails.plugin.springsecurity.authority.className = 'holder.Role'
//
//grails.plugin.springsecurity.userLookup.passwordPropertyName= 'passwd'



//grails.plugin.springsecurity.controllerAnnotations.staticRules = [
//	[pattern: '/',               access: ['permitAll']],
//	[pattern: '/error',          access: ['permitAll']],
//	[pattern: '/index',          access: ['permitAll']],
//	[pattern: '/depSchedule.gsp',      access: ['permitAll']],
//	[pattern: '/shutdown',       access: ['permitAll']],
//	[pattern: '/assets/**',      access: ['permitAll']],
//	[pattern: '/**/js/**',       access: ['permitAll']],
//	[pattern: '/**/css/**',      access: ['permitAll']],
//	[pattern: '/**/images/**',   access: ['permitAll']],
//	[pattern: '/**/favicon.ico', access: ['permitAll']],
//	[pattern: '/calendar/**', access: ['permitAll']],
//	[pattern: '/user/**', access: ['ROLE_ADMIN']],
//	[pattern: '/role/**', access: ['ROLE_ADMIN']],
//	[pattern: '/login/impersonate', access: ['ROLE_SWITCH_USER']],
//	[pattern: '/logout/impersonate', access: ['permitAll']],
//	[pattern: '/register/**', access: ['permitAll']], // forgot password routine
//	[pattern: '/*', access: ['permitAll']] // forgot password routine
////	[pattern: '/guest/**', access: ['ROLE_GUEST']] // Guest means resident of the home, not unknown user
//]

//grails.plugin.springsecurity.filterChain.chainMap = [
//	[pattern: '/assets/**',      filters: 'none'],
//	[pattern: '/**/js/**',       filters: 'none'],
//	[pattern: '/**/css/**',      filters: 'none'],
//	[pattern: '/**/images/**',   filters: 'none'],
//	[pattern: '/**/favicon.ico', filters: 'none'],
//	[pattern: '/**',             filters: 'JOINED_FILTERS']
//]

//grails.plugin.springsecurity.interceptUrlMap = [
//		[pattern: '/login/impersonate', access: ['permitAll']]
//]
//grails.plugin.springsecurity.securityConfigType = "InterceptUrlMap"

//grails.plugin.springsecurity.switchUser.useSwitchUserFilter = true
//grails.plugin.springsecurity.useSwitchUserFilter = true
//grails.plugin.springsecurity.ui.switchUserRoleName = 'ROLE_SWITCH_USER'
