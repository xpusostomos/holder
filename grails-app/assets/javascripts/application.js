// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require /webjars/jquery/3.7.1/dist/jquery.min
//= require popper.min
//= require bootstrap
//= require /webjars/php-date-formatter/1.3.6/js/php-date-formatter.min.js
//= require /webjars/jquery-datetimepicker/2.5.21/jquery.datetimepicker.js
//= require_self


// TODO you can cancel out of the 1st choose to undo, somehow should go back to less input buttons? Nice to have
function addUploadButton(group, index, name, required=false) {
   var next = name.replace("{index}", index);
   if (!document.getElementById(next)) {
       var input = document.createElement("input");
       input.type = "file";
       input.name = next;
       if (required && index == 0) {
           input.required = "true";
       }
       index++;
       input.setAttribute("onchange", `addUploadButton("${group}", ${index}, "${name}")`);
       input.id = next;
       var container = document.getElementById(group);
       container.appendChild(input);
   }
}

// TODO delete single image here didn't seem to work
function removeImage(property) {
    if (document.getElementById(property + ".id")) {
        document.getElementById(property + ".id").setAttribute("value", "");
        document.getElementById(property + ".id").setAttribute("name", "");
        document.getElementById(property + ".img").setAttribute("hidden", "true");
    }
    document.getElementById(property + "-predata").setAttribute("name", property + ".data");
    document.getElementById(property + "-predata").setAttribute("id", property + ".data");
}

function removeImageAlbum(property) {
    document.getElementById(property + "-img").setAttribute("hidden", "true");
    document.getElementById(property + "-delete").setAttribute("hidden", "true"); // only for Album
    document.getElementById(property + "-predata").setAttribute("name", property + ".data");
    document.getElementById(property + "-predata").setAttribute("id", property + ".data");
}


// opposite of date.toISOString()
//function parseISOString(s) {
//  var b = s.split(/\D+/);
//  return new Date(Date.UTC(b[0], --b[1], b[2], b[3], b[4], b[5], b[6]));
//}
//
//$(function() {
//    $('.date').each(function() {
//        let d = new Date(this.textContent);
//        this.textContent = d.toLocaleDateString('en-AU'); // Horrible hard coding this, but browsers suck.
//    });
//    $('.datetime').each(function() {
//        let d = parseISOString(this.textContent);
//        this.textContent = d.toLocaleDateString('en-AU'); // Horrible hard coding this, but browsers suck.
//    });
//});
