package holder
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

@Secured('ROLE_ADMIN')
class RoomController {
    static scaffold = Room
    RoomService roomService
    PersonService personService

    def index(Integer max) {
        params.max = Math.min(max ?: 50, 100)
//        params.sort = [level: 'asc', name: 'asc']
//        respond roomService.list(params).sort { a, b -> a.fullName <=> b.fullName }, model:[roomCount: roomService.count()]
        def rooms = roomService.list(params).findAll { it.level == RoomLevelEnum.PREMISES}.collectMany { it.allRooms }
//        respond roomService.list(params), model:[roomCount: roomService.count()]
        respond rooms, model:[roomCount: roomService.count()]
    }

    def email(Room room) {
        def email = new EmailCommand(
                candidates: room.allRooms.findAll{it.lessee}.collect { new PersonCandidate(person: it.lessee, checked: true)}
        )
        render view: 'email', model: [room: room,
                                      email: email
        ]
    }

    def send(Room room, EmailCommand email) {
        if (!email.validate()) {
            render view: 'email', model: [room: room, email: email]
            return
        }
        personService.sendEmail(email)
        redirect controller: 'room'
    }
}
