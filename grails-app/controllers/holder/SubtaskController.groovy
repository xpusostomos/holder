package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class SubtaskController {
    static scaffold = Subtask
}
