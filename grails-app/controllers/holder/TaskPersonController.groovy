package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class TaskPersonController {
    static scaffold = TaskPerson

}
