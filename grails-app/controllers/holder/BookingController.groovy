package holder
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import java.time.LocalDate

import static org.springframework.http.HttpStatus.*

@Secured('ROLE_ADMIN')
class BookingController {
    static scaffold = Booking
    def myValidateService
    def asynchronousMailService
    BookingService bookingService
    MailIndexService mailIndexService

    def save(Booking contract) {
        if (contract == null) {
            notFound()
            return
        }

//        load(contract)
        try {
            bookingService.save(contract)
        } catch (ValidationException e) {
            respond contract.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contract.label', default: 'Booking'), contract.id])
                redirect contract
            }
            '*' { respond contract, [status: CREATED] }
        }
    }


    def respondEnquiry(Booking booking) {
        EmailTemplate email = EmailTemplate.findByName('APPLICATION_RESPOND')
        EmailCommand command = new EmailCommand(
                to: email.bindAddresses([booking: booking]),
                subject: email.bindSubject([booking: booking]),
                body: email.bindBody([booking: booking])
        )
        render view: 'respondEnquiry', model: [booking: booking, email: command]
    }

    def sendResponse(EmailCommand email, Booking booking) {
        if (!email.validate()) {
            render view: 'respondEnquiry', model: [booking: booking, email: email]
            return
        }
        int maxAttempts = Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool ? 3 : 0

//        if (Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool) {
        asynchronousMailService.sendMail {
                to email.to
                subject email.subject
                text email.body
//                externalReference: booking.id
                maxAttempts: maxAttempts
                email.attachments?.photos.each {
                    attach it.name, it.mimeType, it.data
                }
            }
            booking.dateResponded = LocalDate.now()
            bookingService.save(booking)
//        }
        mailIndexService.create(mm, null, [
                [id: booking.id, name: 'booking']])
        redirect controller: 'admin', view: 'index'
    }


    def reject(Booking booking) {
        if (booking == null) {
            notFound()
            return
        }
        booking.dateDecided = LocalDate.now()
        booking.positiveDecision = false

        try {
            bookingService.save(booking)
        } catch (ValidationException) {
            respond booking.errors, view:'show'
            return
        }
        EmailTemplate.findByName('APPLICATION_REJECT')?.sendMail([booking: booking])
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'booking.reject', default: 'Booking rejected, email sent'), booking.id])
                redirect action: 'index'
            }
            '*' { respond booking, [status: CREATED] }
        }
    }

    def approve(Booking booking) {
        redirect controller: 'contract', action: 'drawUp', id: booking.id
    }

    def viewScan(Photo photo) {
        response.setHeader("Content-disposition", "inline; filename=\"${photo.name}\"")
        response.outputStream << photo.data
     }
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'booking.label', default: 'Booking'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

