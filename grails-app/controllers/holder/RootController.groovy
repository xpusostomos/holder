package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ANONYMOUS','ROLE_USER', 'ROLE_ADMIN'])
class RootController {

    def index() {
        String foo = request.siteUrl
        render view: '/index', model: [ availableRooms: Room.allAvailableOnKnownDate() ]
    }

    def showRoom(Room room) {
        respond room, view: '/showRoom'
    }
}
