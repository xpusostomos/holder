package holder
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import org.springframework.context.MessageSource
import static org.springframework.http.HttpStatus.*

import java.time.LocalDateTime

@Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN'])
class PersonController {
    static scaffold = Person
    PersonService personService
    AccountEntryService accountEntryService
    def myValidateService
    def mailService
    MessageSource messageSource

    def email(Person person) {
        render view: 'email', model: [person: person, email: new EmailCommand()]
    }

    def send(Person person, EmailCommand email) {
        if (!email.validate()) {
            render view: 'email', model: [room: room, email: email]
            return
        }
        personService.sendEmail(person, email)
        flash.message = 'Email sent'
        redirect controller: 'person'
    }

    def rentDueEmail(Person person) {
        if (personService.generatePaymentDueEmail(person)) {
            flash.message = 'email sent'
        } else {
            flash.message = 'email not sent'
        }
        redirect action: 'show', id: person.id
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER', 'ROLE_GUEST'])
    def notifyPayment(Person person) {
        respond person
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER', 'ROLE_GUEST'])
    def paymentNotified(Person person, String comment) {
        Token token = new Token(
                user: person.user,
                expiry: LocalDateTime.now().plusDays(14),
                oneTime: false
        )
        person.paymentNotified = true
        personService.save(person)
        EmailTemplate.findTemplate('ADMIN_NOTIFY_PAYMENT', [], null)?.sendMail(
                [
                        person: person,
                        token: token,
                        comment: comment
                ],
                true,
                [
                        [id: User.ADMIN_USER_ID, name: 'user']
                ])
        respond person
    }

    def payRefund(Person person) {
        PaymentCommand paymentCommand = new PaymentCommand(
                person: person,
                amount: -person.amountOwing,
                description: 'Refund paid'
        )
        respond paymentCommand, view: 'payRefund'
    }

    def payPerson(Person person) {
        PaymentCommand paymentCommand = new PaymentCommand(
                person: person,
                amount: -person.amountOwing,
                description: 'Pay person'
        )
        respond paymentCommand, view: 'payPerson'
    }

    def savePayPerson(PaymentCommand paymentCommand) {
        Map<Room,BigDecimal> payments = paymentCommand.person.getPotentialTransferByPremises(paymentCommand.amount)
        if (!payments) {
            flash.messages = "Cannot draw down that amount"
            respond paymentCommand.errors, view: 'payPerson'
            return
        }

        for (it in payments) {
            AccountEntry accountEntry = new AccountEntry()
            accountEntry.date = paymentCommand.date
            accountEntry.method = paymentCommand.method
            accountEntry.description = paymentCommand.description + ' ' + it.key
            accountEntry.person = paymentCommand.person
            accountEntry.notes = paymentCommand.notes
            accountEntry.premises = it.key
            accountEntry.amount = it.value
            accountEntry.type = AccountEntryTypeEnum.PAYMENT
            accountEntry.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
            if (accountEntry.method == AccountEntryMethodEnum.BANK) {
                accountEntry.creditAccountId = ChartOfAccounts.BANK.id
            } else if (accountEntry.method == AccountEntryMethodEnum.CASH) {
                accountEntry.creditAccountId = ChartOfAccounts.CASH.id
            } else if (accountEntry.method == AccountEntryMethodEnum.WRITEOFF) {
                accountEntry.creditAccountId = ChartOfAccounts.BAD_DEBTS.id
            }
            try {
                accountEntry.save()
                accountEntryService.save(accountEntry)
            } catch (ValidationException e) {
                flash.message = e.getLocalizedMessage()
                respond paymentCommand, view: 'payRefund'
                return
            }
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'payment.label', default: 'Payment'), paymentCommand.person.id])
                redirect controller: 'person', action: 'show', id: paymentCommand.person.id
            }
            '*' { redirect controller: 'person', action: 'show', id: paymentCommand.person.id,  [status: CREATED] }
        }
    }

    def savePayRefund(PaymentCommand paymentCommand) {
        if (paymentCommand == null) {
            notFound()
            return
        }
        if (!paymentCommand.validate()) {
            respond paymentCommand.errors, view: 'payRefund'
            return
        }
        BigDecimal total = -paymentCommand.amount
        for (def it in paymentCommand.person.balances) {
            AccountEntry accountEntry = new AccountEntry()
            accountEntry.date = paymentCommand.date
            accountEntry.method = paymentCommand.method
            accountEntry.description = paymentCommand.description
            accountEntry.person = paymentCommand.person
            accountEntry.notes = paymentCommand.notes
            accountEntry.premises = it.key
            if (it.value > total) {
                accountEntry.amount = total
                total = 0.0
            } else {
                accountEntry.amount = -it.value
                total -= -it.value
            }
            accountEntry.type = AccountEntryTypeEnum.PAYMENT
            accountEntry.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
            if (accountEntry.method == AccountEntryMethodEnum.BANK) {
                accountEntry.creditAccountId = ChartOfAccounts.BANK.id
            } else if (accountEntry.method == AccountEntryMethodEnum.CASH) {
                accountEntry.creditAccountId = ChartOfAccounts.CASH.id
            } else if (accountEntry.method == AccountEntryMethodEnum.WRITEOFF) {
                accountEntry.creditAccountId = ChartOfAccounts.BAD_DEBTS.id
            }
            try {
                accountEntry.save()
                accountEntryService.save(accountEntry)
            } catch (ValidationException e) {
//                String errs = ""
//                ia.errors.allErrors.each {
//                    errs += messageSource.getMessage(it, LocaleContextHolder.getLocale()) + "  "
//                }
                flash.message = e.getLocalizedMessage()
                respond paymentCommand, view: 'payRefund'
                return
            }
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'payment.label', default: 'Payment'), paymentCommand.person.id])
                redirect controller: 'person', action: 'show', id: paymentCommand.person.id
            }
            '*' { redirect controller: 'person', action: 'show', id: paymentCommand.person.id,  [status: CREATED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'person.label', default: 'Person'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
