package holder

import grails.plugin.springsecurity.annotation.Secured

import java.time.LocalDate
import java.time.LocalDateTime

@Secured(['ROLE_ADMIN','ROLE_VICE_ADMIN','ROLE_USER'])
class AdminController {
    SmsService smsService

    def index() {
        render view: 'index', model: [
                schedule: Schedule.findAllByCompleteAndCancelledAndDueLessThan(false, false, LocalDateTime.now()),
                todo: ToDo.findAllByComplete(false, [sort: [room: 'asc', priority: 'asc']]),
                moneyOwing: Person.list().findAll { it.amountOwing > 0.0 }.sort {a, b -> b.amountOwing <=> a.amountOwing },
                requestList: Booking.findAllByDateRespondedIsNullAndDateDecidedIsNull( [sort: 'dateCreated']),
                inspectionList: Booking.findAllByDateInspectionGreaterThanAndDateInspectionIsNotNullAndDateRespondedIsNotNullAndDateDecidedIsNull(LocalDateTime.now(), [sort: 'dateInspection']),
                bookingList: Booking.findAllByDateDecidedIsNullAndDateRespondedIsNotNull(false, [sort: 'dateCreated']),
                contractList: Contract.findAllByClosedAndPerson(false, null, [sort: 'dateCreated']),
                roomList: Room.findAllByAvailable(true).sort { a, b ->
                    a.parent?.parent?.name <=> b.parent?.parent?.name ?:
                            a.parent?.name <=> b.parent?.name ?:
                                    a.name <=> b.name
                },
//                rented: Room.findAllByLesseeIsNotNull(),
//                rented: Room.findAll().findAll {it.lessee }.sort { a, b ->
//                    a.premises.name <=> b.premises.name ?:
//                            a.section.name <=> b.section.name ?:
//                                    a.name <=> b.name
//                },
                queryList: Comment.findAllByParentAndClosed(null,false, [sort: 'dateCreated'])
        ]
    }

    def foo() {
        try {
//            def result = smsService.send([message: 'hello', to: ['+61416245269', '+61420523138']])
//            def result = smsService.send([message: 'hello', to: '+61416245269', send_at: LocalDate.of(2020, 2, 3)])
            def result = smsService.send([text: 'hello', to: '+61416245269', sendAt: LocalDateTime.of(2020, 2, 3, 14, 5, 6)])
            log.debug "sms sent: $result"
        } catch (SmsException x) {
            log.debug "sms error: $x $x.result"
        }
        flash.message = 'tried'
        redirect view: 'index'
    }

    @Secured(['ROLE_ANONYMOUS','ROLE_USER', 'ROLE_ADMIN'])
    def testsms() {
        log.debug "testsms header: ${request.getHeader('Authorization')}"
        log.debug "testsms received: ${params.toString()}"
        render groovy.json.JsonOutput.toJson([message_id: 'abc123', send_at: '2021-03-04 15:06:07', recipients: params.to, cost: 0.067])
    }

    def generateInvoices() {
        GenerateInvoiceJob.triggerNow([:])
        flash.message = "invoice job triggered"
        redirect view: 'index'
    }

    def generateSchedule() {
        GenerateScheduleJob.triggerNow([:])
        flash.message = "generate schedule job triggered"
        redirect view: 'index'
    }

    def generateReminder() {
        RemindScheduleJob.triggerNow([:])
        flash.message = "remind schedule job triggered"
        redirect view: 'index'
    }

    def diagnostics() {
        render view: 'diagnostics'
    }

    def createTestData() {
        BootStrap.createDefaultData()
        render view: 'diagnostics'
    }
}
