package holder

import grails.plugin.springsecurity.annotation.Secured

import java.time.LocalDateTime

@Secured('ROLE_ADMIN')
class ScheduleController {
    static scaffold = Schedule
    ScheduleService scheduleService

    @Secured(['ROLE_ADMIN', 'ROLE_GUEST', 'ROLE_USER'])
    def display(Schedule schedule) {
        LocalDateTime earliest = schedule.due - schedule.task.remindEarlyInterval
        if (LocalDateTime.now() < earliest) {
            flash.message = "It's too early to complete this task. Try after $earliest"
            render view: 'display', model: [schedule: schedule]
            return
        }
        if (schedule.complete) {
//            redirect controller: 'schedule', action: 'complete', id: schedule.id
            redirect action: 'complete', id: schedule.id
        } else {
            respond schedule
        }
    }

    def sendReminder(Schedule schedule) {
        scheduleService.sendReminder(schedule, 'TASK_REMIND_LATE')
        flash.message = 'email sent'
        redirect action: 'show', id: schedule.id
    }

    @Secured(['ROLE_ADMIN', 'ROLE_GUEST', 'ROLE_USER'])
    def complete(Schedule schedule) {
        LocalDateTime earliest = schedule.due - schedule.task.remindEarlyInterval
        if (LocalDateTime.now() < earliest) {
            flash.message = "It's too early to complete this task. Try after $earliest"
            render view: 'display', model: [schedule: schedule]
            return
        }
        boolean hasError = false
        List<Boolean> errors = new ArrayList<>(schedule.task.subtasks.size())
        for (int i = 0; !schedule.complete && i < schedule.task.subtasks.size(); i++) {
            errors[i] = false
            if (params.getProperty("subtask[$i]") == 'on') {

            } else {
                hasError = true
                errors[i] = true
            }
        }
        if (hasError) {
            flash.message = 'Not all subtasks are complete'
            render view: 'display', model: [schedule: schedule, errors: errors]
        } else {
            if (!schedule.complete) {
                EmailTemplate.findTemplate('ADMIN_TASK_COMPLETE')?.sendMail([schedule: schedule, task: schedule.task])
                def pc = EmailTemplate.findTemplate('TASK_COMPLETE')
                schedule.task.taskPeople.each {
                    pc?.sendMail([person: it.person, task: schedule.task, schedule: schedule],
                            it.person.emailEnabled,
                            [
                                    [id: it.person.id, name: 'person'],
                                    [id: it.person.user?.id, name: 'user']
                            ],
                            it.person.smsEnabled
                    )
                }
            }
            schedule.complete = true
            scheduleService.save(schedule)
            respond schedule
        }
    }
}
