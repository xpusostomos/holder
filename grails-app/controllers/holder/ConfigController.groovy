package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class ConfigController {
    static scaffold = Config
//    ConfigService configService
//
//    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
//
//    def index(Integer max) {
//        params.max = Math.min(max ?: 10, 100)
////        params.sort = [lastUpdated: 'desc']
//        params.sort = 'lastUpdated'
//        params.order = 'desc'
//        def foo = configService.list(params)
//        respond configService.list(params), model:[configCount: configService.count()]
//    }
//
//    def show(Long id) {
//        respond configService.get(id)
//    }
//
//    def create() {
//        respond new Config(params)
//    }
//
//    def save(Config config) {
//        if (config == null) {
//            notFound()
//            return
//        }
//
//        try {
//            configService.save(config)
//        } catch (ValidationException e) {
//            respond config.errors, view:'create'
//            return
//        }
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'config.label', default: 'Config'), config.id])
//                redirect config
//            }
//            '*' { respond config, [status: CREATED] }
//        }
//    }
//
//    def edit(Long id) {
//        respond configService.get(id)
//    }
//
//    def update(Config config) {
//        if (config == null) {
//            notFound()
//            return
//        }
//
//        try {
//            configService.save(config)
//        } catch (ValidationException e) {
//            respond config.errors, view:'edit'
//            return
//        }
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'config.label', default: 'Config'), config.id])
//                redirect config
//            }
//            '*'{ respond config, [status: OK] }
//        }
//    }
//
//    def delete(Long id) {
//        if (id == null) {
//            notFound()
//            return
//        }
//
//        configService.delete(id)
//
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'config.label', default: 'Config'), id])
//                redirect action:"index", method:"GET"
//            }
//            '*'{ render status: NO_CONTENT }
//        }
//    }
//
//    protected void notFound() {
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.not.found.message', args: [message(code: 'config.label', default: 'Config'), params.id])
//                redirect action: "index", method: "GET"
//            }
//            '*'{ render status: NOT_FOUND }
//        }
//    }
}
