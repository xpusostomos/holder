package holder

import grails.plugin.springsecurity.annotation.Secured

import java.time.LocalDate
@Secured('ROLE_ADMIN')
class FinanceController {

//    def index(def fromDate, def toDate) {
    def index(FinanceCommand c) {
//        def fromDatex = params.froMDate
        def toDatex = params.toDate
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
//        Set<AccountEntry> aes = AccountEntry.findAllByDateGreaterThanEqualsAndDateLessThanEquals(c.fromDate, c.toDate)
        Set<AccountEntry> aes = AccountEntry.findByDates(c.fromDate, c.toDate, c.premises)
//        Set<AccountEntry> aes = AccountEntry.findAll()
        Set<Account> accounts = new HashSet<>()
        Map<Account, BigDecimal> totals = new HashMap<>()
        Map<LocalDate, Map<Account,BigDecimal>> summary = new HashMap<>()
        aes.forEach {
            LocalDate pdate
            if (c.byYear) {
                pdate = Contract.financialYearFor(it.date)
            } else if (c.calendarYear) {
                pdate = Contract.calendarYearFor(it.date)
            } else {
                pdate = Contract.stdPeriodEndFor(it.date)
            }
            Map<Account, BigDecimal> period = summary.get(pdate)
            if (!period) {
                period = new HashMap<>()
                summary.put(pdate, period)
            }
            BigDecimal total = period.get(it.debitAccount)
            if (!total) {
                total = new BigDecimal(0.0)
            }
            total = total.add(it.amount)
            period.put(it.debitAccount, total)

            BigDecimal grandTotal = totals.get(it.debitAccount)
            if (!grandTotal) {
                grandTotal = new BigDecimal(0.0)
            }
            grandTotal = grandTotal.add(it.amount)
            totals.put(it.debitAccount, grandTotal)

            accounts.add(it.debitAccount)

            total = period.get(it.creditAccount)
            if (!total) {
                total = new BigDecimal(0.0)
            }
            total = total.subtract(it.amount)
            period.put(it.creditAccount, total)

            grandTotal = totals.get(it.creditAccount)
            if (!grandTotal) {
                grandTotal = new BigDecimal(0.0)
            }
            grandTotal = grandTotal.subtract(it.amount)
            totals.put(it.creditAccount, grandTotal)

            accounts.add(it.creditAccount)
        }
        render view: 'index', model: [ accounts: accounts, summary: summary, c: c, totals: totals ]
    }

    def businessSummary(FinanceCommand c) {
//        def fromDatex = params.froMDate
        def toDatex = params.toDate
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        def incomeAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(40000, 60000).sort {a, b -> a.id <=> b.id}
        def expenseAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(60000, 70000).sort {a, b -> a.id <=> b.id}
        render view: 'businessSummary', model: [ incomeAccounts: incomeAccounts, expenseAccounts: expenseAccounts, c: c]
    }


    def trialBalance(FinanceCommand c) {
        bindData(c, params)
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        def accounts = Account.list().sort {a, b -> a.id <=> b.id}
        render view: 'trialBalance', model: [ accounts: accounts, c: c]
    }

    def profitAndLoss(FinanceCommand c) {
        bindData(c, params)
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        def incomeAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(40000, 60000).sort {a, b -> a.id <=> b.id}
        def expenseAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(60000, 70000).sort {a, b -> a.id <=> b.id}
        render view: 'profitAndLoss', model: [ incomeAccounts: incomeAccounts, expenseAccounts: expenseAccounts, c: c]
    }

    def balanceSheet(FinanceCommand c) {
        bindData(c, params)
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        def assetAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(10000, 20000).sort {a, b -> a.id <=> b.id}
        def liabilityAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(20000, 30000).sort {a, b -> a.id <=> b.id}
        def equityAccounts = Account.findAllByIdGreaterThanEqualsAndIdLessThan(30000, 40000).sort {a, b -> a.id <=> b.id}
        render view: 'balanceSheet', model: [ assetAccounts: assetAccounts, liabilityAccounts: liabilityAccounts, equityAccounts: equityAccounts, c: c]
    }


    def ledger(FinanceCommand c) {
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        def accounts = Account.list().sort {a, b -> a.id <=> b.id}
        render view: 'ledger', model: [ accounts: accounts, c: c]
    }


    def journal(FinanceCommand c) {
        LocalDate now = LocalDate.now()
        if (!c.fromDate) {
            int yr = now.year
            if (now.monthValue <= 6) {
                yr--
            }
            c.fromDate = LocalDate.of(yr, 7, 1)
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        Set<AccountEntry> accountEntries = AccountEntry.findByDates(c.fromDate, c.toDate, c.premises)
        render view: 'journal', model: [ accountEntries: accountEntries, c: c]
    }

    def generateOpeningEntries() {
        List<Account> accounts = Account.findAllByIdNotEqualAndIdLessThan(ChartOfAccounts.EQUITY.id, 40000, [sort: 'id'])
//        List<Account> accounts = [Account.get(17160)]
        List<AccountEntry> accountEntries = accounts.collectMany { it.generateOpeningEntries() }
//        List<AccountEntry> xxx = accounts.collect { it.generateOpeningEntries() }
//        List<AccountEntry> accountEntries = xxx.flatten()w

        List<AccountEntry> oldOpening = AccountEntry.findAllByType(AccountEntryTypeEnum.OPENING, [sort: 'date'])
//        accountEntries.sort {a, b -> a.date <=> b.date ?: a.debitAccount.id <=> b.debitAccount.id ?: a.creditAccount.id <=> b.creditAccount.id}.each {
//            log.warn "$it.date $it.debitAccount $it.creditAccount $it.amount"
//        }
//        log.warn "$accountEntries"
        AccountEntry.withTransaction {
            new HashSet<AccountEntry>(oldOpening).minus(accountEntries).each {it.delete() }
            accountEntries.each { it.save() }
        }
        render view: 'openingEntries', model: [ accountEntries: accountEntries ]
    }

    def generateClosingEntries() {
        List<Account> accounts = Account.findAllByIdGreaterThanEquals(40000, [sort: 'id'])
        List<AccountEntry> accountEntries1 = accounts.collectMany { it.generateClosingEntries(Account.get(ChartOfAccounts.CURRENT_YEAR_SURPLUS.id), AccountEntryTypeEnum.CLOSING) }
        List<AccountEntry> accountEntries2 = Account.get(ChartOfAccounts.CURRENT_YEAR_SURPLUS.id).generateClosingEntries(Account.get(ChartOfAccounts.RETAINED_SURPLUS.id), AccountEntryTypeEnum.ROLLOVER)
        List<AccountEntry> accountEntries3 = Account.get(ChartOfAccounts.DIVIDENDS_PAID.id).generateClosingEntries(Account.get(ChartOfAccounts.RETAINED_SURPLUS.id), AccountEntryTypeEnum.ROLLOVER)
        List<AccountEntry> accountEntries = [ *accountEntries1, *accountEntries2, *accountEntries3 ]
        List<AccountEntry> oldClosing = AccountEntry.findAllByType(AccountEntryTypeEnum.CLOSING, [sort: 'date'])
        AccountEntry.withTransaction {
            new HashSet<AccountEntry>(oldClosing).minus(accountEntries).each {
                it.debitAccount.removeFromDebitAccountEntries(it)
                it.creditAccount.removeFromCreditAccountEntries(it)
                it.delete()
            }
            accountEntries.each { it.save() }
        }
        render view: 'openingEntries', model: [ accountEntries: accountEntries ]
    }
}
