package holder

import grails.plugin.springsecurity.annotation.Secured

import javax.xml.bind.ValidationException
import java.text.DecimalFormat
import java.time.LocalDate

@Secured(['ROLE_GUEST', 'ROLE_ADMIN'])
class GuestController {
    def myValidateService
    def springSecurityService
    CommentService commentService
    GuestService guestService

    def index() {
        def currentPrincipal = springSecurityService.principal
        // should the above be currentUser instead of principal?
        if (currentPrincipal.authorities.collect{it.role}.contains('ROLE_ADMIN')) {
            respond Person.list()
        } else {
            redirect action: 'home', id: Person.findByUser(User.get(currentPrincipal.id)).id
        }
    }

    // TODO provide a downloadable google calender file.
    def home(Person person) {
        def currentPrincipal = springSecurityService.principal
//        if (!person) {
//            flash.message = 'no user specified'
//            redirect action: 'home', id: Person.findByUser(User.get(currentPrincipal.id)).id
//            return
//        }
        if (person?.user?.id != currentPrincipal.id && !currentPrincipal.authorities.collect{it.authority}.contains('ROLE_ADMIN')) {
            if (person) {
                flash.message = 'wrong user'
            }
            redirect action: 'home', id: Person.findByUser(User.get(currentPrincipal.id)).id
            return
        }
        if (!person) {
            redirect action: 'index'
            return
        }
//        if (person == null & session.user != null) {
//            redirect action: 'home', id: session.user.id
//            return
//        }
        def f = new DecimalFormat('$#0.00;($#0.00)')
        LocalDate today = LocalDate.now()
        def dates = [today]
        List<Map<String,String>> events
        if (person) {
            def beginningOfLastMonth = LocalDate.of(today.month.value == 1 ? today.year-1 : today.year, today.month.value == 1 ? 12 : today.month.value-1, 1)
            def beginningOfThisMonth = LocalDate.of(today.year, today.month, 1)
            def endOfNextMonth = LocalDate.of(today.month.value == 12 ? today.year+1 : today.year, today.month.value == 12 ? 1 : today.month.value+1, 1)
            endOfNextMonth = endOfNextMonth.withDayOfMonth(endOfNextMonth.month.length(endOfNextMonth.isLeapYear()))
            List<AccountEntry> accountEntries = new ArrayList<AccountEntry>(person.accountEntries)
            accountEntries.addAll person.contracts.findAll {it.active }.collect { it.generateAccountEntriesUntil(endOfNextMonth)  }.flatten()
            events = accountEntries.collect { [key: it.periodBegin, value: it.description + ' '+ f.format(it.amount), color: 'DarkViolet' ]}
            events
            events.addAll(person.contracts.findAll{it.active}.collect {[
                    [key: it.checkIn?.toLocalDate(), value: 'Contract start', color: 'SeaGreen'],
                    [key: it.checkOut?.toLocalDate(), value: 'Contract end', color: 'SeaGreen'],
                    [key: it.moveOut?.toLocalDate(), value: 'Notified move out date', color: 'SeaGreen']
            ]}.flatten())
            def taskPeople = TaskPerson.findAllByPerson(person)
            def taskEvents = taskPeople.collect { it.task }
                    .collectMany {
                        it.schedules.findAll { it.person == person }.collect{
                            [
                                    value: it.task.name + (it.complete ? ' (done)' : ''),
                                    key: it.due.toLocalDate(),
                                    color: (!it.complete && it.due.isBefore(java.time.LocalDateTime.now()) ? 'red' : 'black'),
                                    controller: 'schedule',
                                    action: 'display',
                                    id: it.id
                            ]}}
            events.addAll(taskEvents)
            events = events.findAll { it.key >= beginningOfLastMonth && it.key <= endOfNextMonth }
            if (person.amountOwing < 1.0) { // a dollar
                events = events.findAll { it.key >= beginningOfThisMonth }
            }
            events.add([key: LocalDate.now(), value: 'Today', color: 'red'])
            dates = [today]
        } else {
            flash.errors = ["No person specified"]
        }
        render view: 'home', model: [
                person: person,
                events: events,
                dates: dates,
                lastPayment: person.lastPayment
        ]
    }

    def tips(Person person) {
        render view: 'tips', model: [
                person: person,
                tips: Tip.getApplicableTipsByCategory(TipTypeEnum.TIP, person)
        ]
    }

    def account(Person person) {

    }

    def submitQuery(Comment query) {
        Person person = Person.findByUser(query.from)
        query.to = User.principal
        try {
            commentService.save(query)
            flash.message = "Query submitted to Chris. He'll get back to you shortly!"
        } catch (ValidationException) {
            // TODO check that this works
            render action: 'home', model: [query: comment, person: person, errors: comment.errors]
            return
        }
        redirect action: 'home', id: person.id
    }

    def closeQuery(Comment comment) {
        Person person = Person.findByUser(comment.from)
        comment.parent.closed = true
        commentService.save(comment.parent)
        if (comment.text) {
            if (comment.validate()) {
                Comment.withTransaction {
                    comment.save()
                }
                flash.message = "Query closed"
            } else {
                flash.message = comment.errors
            }
        }
        redirect action: 'home', id: person.id
    }

    def addComment(Comment comment) {
        Person person = Person.findByUser(comment.parent.from)
        if (comment.validate()) {
            Comment.withTransaction {
                comment.save()
            }
            flash.message = "Comment sent to Chris. He'll get back to you shortly!"
        } else {
            flash.message = comment.errors
        }
        redirect action: 'home', id: person.id
    }
}
