package holder

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

@Secured('ROLE_ADMIN')
class TokenController {
    static scaffold = Token
    TokenService tokenService

}
