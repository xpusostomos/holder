package holder

import grails.gsp.PageRenderer
import grails.plugin.springsecurity.SpringSecurityService
import org.grails.gsp.GroovyPagesTemplateEngine
import grails.plugin.asyncmail.AsynchronousMailService
import org.springframework.security.access.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ANONYMOUS','ROLE_USER'])
class ApplyController {
    AsynchronousMailService asynchronousMailService
    MailIndexService mailIndexService
    SpringSecurityService springSecurityService
    def myValidateService
//    PageRenderer groovyPageRenderer
    BookingService bookingService
    ContractService contractService
    PersonService personService

    def index() {
        respond new Booking()
    }
    def index2() {
        respond new Booking()
    }

    def save(Booking booking) {
        if (booking == null) {
            notFound()
            return
        }
        try {
            if (!booking.album.photos.isEmpty()) {
                booking.album.save()
                booking.album.photos.forEach { it.save() }
            } else {
                booking.album = null
            }
            booking.save()
            bookingService.save(booking)
        } catch (ValidationException) {
            respond booking.errors, view:'index'
            return
        }
        // TODO should this apply to any logged in user.
        boolean isAdmin = springSecurityService.principal.authorities.collect{it.role}.contains('ROLE_ADMIN');
        if (!isAdmin) {
//            def application = groovyPageRenderer.render(view: '/apply/applicationEmail', model: [booking: booking])
            EmailTemplate.findByName('APPLICATION')?.sendMail([booking: booking])
//            if (applicationEmail) {
//                asyncMailService.sendMail {
//                    to "$booking.name <$booking.email>"
//                    subject applicationEmail.makeSubject([booking: booking])
//                    text applicationEmail.makeBody([booking: booking])
//                }
//            }
//            def applicationAdmin = groovyPageRenderer.render(view: '/apply/applicationAdminEmail', model: [booking: booking])
            EmailTemplate.findByName('APPLICATION_ADMIN')?.sendMail([booking: booking])
//            def applicationAdminEmail = EmailTemplate.findByName('APPLICATION_ADMIN');
//            if (applicationAdminEmail) {
//                asyncMailService.sendMail {
//                    to User.usersWithRole('ROLE_ADMIN').collect { it.email }
//                    subject applicationAdminEmail.makeSubject([booking: booking])
//                    text applicationAdminEmail.makeBody([booking: booking])
////                subject 'New room Application received'
////                text applicationAdmin
//                }
//            }
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'booking.label', default: 'Booking'), booking.id])
                redirect action: 'done'
            }
            '*' { respond booking, [status: CREATED] }
        }

//        if (booking.validate()) {
//            Booking.withTransaction {
//                if (!booking.album.photos.isEmpty()) {
//                    booking.album.save()
//                    booking.album.photos.forEach { it.save() }
//                } else {
//                    booking.album = null;
//                }
//                booking.save()
//            }
//            def application = groovyPageRenderer.render(view: '/apply/applicationEmail', model: [booking: booking])
//            asyncMailService.sendMail {
//                to "$booking.name <$booking.email>"
//                subject 'Room Application'
//                text application
//            }
//            def applicationAdmin = groovyPageRenderer.render(view: '/apply/applicationAdminEmail', model: [booking: booking])
//            asyncMailService.sendMail {
//                to user.usersWithRole('ROLE_ADMIN').collect { it.email }
//                subject 'New room Application received'
//                text applicationAdmin
//            }
//            request.getServerPort()
//
//            redirect action: 'done'
//        } else {
////            flash.errors = myValidateService.checkErrors([booking])
//            respond booking.errors, view: 'index'
//        }
    }

    def host() {
        return request.getScheme() + "://" + request.getServerName() + (request.getServerPort() > 0 ? ':' + request.getServerPort() : '')
    }

    def done() {
    }

    // should stop people signing up twice.
    def signUp(Contract contract, String passKey) {
        if (!contract) {
            flash.message = "contract has been deleted"
            flash.error = true
            redirect controller: 'root'
            return
        }
        if (passKey != contract.passKey) {
            flash.message = "Permission Denied"
            flash.error = true
            redirect controller: 'root'
            return
        }
        if (contract.agreed) {
            flash.message = "Contract already finalized"
            flash.error = true
            redirect controller: 'root'
            return
        }

        // At this point these objects are just transients
        // that are sent to the front end. Later on, they'll be
        // persisted... if the user gets that far.
        Person person = personService.newPersonFromContract(contract)
        render view: 'signUp', model: [ contract: contract, user: person.user, person: person, passKey: passKey ]
    }

    //  TODO We can't assume a new User, what if they're an old user reapplying?
    //  TODO present the contract for physical printing.
    // TODO photos must be mandatory
    def signed(Contract contract, User user, Person person, String passKey) {
        if (passKey != contract.passKey) {
            flash.message = "Permission Denied"
            flash.error = true
            redirect controller: 'root'
            return
        }
        if (contract.agreed) {
            flash.message = "Contract already finalized"
            flash.error = true
            redirect controller: 'root'
            return
        }
        contract.agreed = true
        contract.person = person
        person.user = user
        person.clearErrors()
        if (contract.validate() & user.validate() & person.validate()) {
            personService.signup(contract)
            // TODO should this be reinstated? contract.generateEmailForAccountEntries(accountEntries) // TODO does this handle bond correctly?
//            def signedAdmin = groovyPageRenderer.render(view: '/apply/signedAdminEmail', model: [contract: contract])

            EmailTemplate.findByName('CONTRACT_SIGNED_ADMIN')?.sendMail([contract: contract])
//            def contractSignedEmail = EmailTemplate.findByName('CONTRACT_SIGNED_ADMIN');
//            if (Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool) {
//                if (contractSignedEmail) {
//                    asyncMailService.sendMail {
//                        to User.usersWithRole('ROLE_ADMIN').collect { it.email }
//                        subject contractSignedEmail.makeSubject([contract: contract])
//                        text contractSignedEmail.makeBody([contract: contract])
//                    }
//                }
//            }
            render view: 'signed', model: [ contract: contract, user: user, person: person, passKey: passKey ]
        } else {
//            flash.errors = myValidateService.checkErrors([contract, person]) // don't put user here.
            respond person.errors, view: 'signUp', model: [ contract: contract, user: user, person: person, passKey: passKey ]
        }
    }

    def rejected(Contract contract, String passKey) {
        if (contract == null) {
            notFound()
            return
        }
        if (passKey != contract.passKey) {
            flash.message = "Permission Denied"
            flash.error = true
            redirect controller: 'root'
            return
        }
        if (contract.agreed) {
            flash.message = "Contract already finalized"
            flash.error = true
            redirect controller: 'root'
            return
        }
        contract.agreed = false
        contract.closed = true
        try {
            contract.save()
            contractService.save(contract)
        } catch (ValidationException) {
            respond contract.errors, view: 'signUp'
            return
        }
//        def rejectedAdmin = groovyPageRenderer.render(view: '/apply/rejectedAdminEmail', model: [contract: contract])
        EmailTemplate.findByName('CONTRACT_REJECTED_EMAIL')?.sendMail([contract: contract])
//        def rejectedAdminEmail = EmailTemplate.findByName('CONTRACT_REJECTED_EMAIL');
//        if (Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool) {
//            if (rejectedAdminEmail) {
//                asyncMailService.sendMail {
//                    to User.usersWithRole('ROLE_ADMIN').collect { it.email }
//                    subject rejectedAdminEmail.makeSubject([contract: contract])
//                    text rejectedAdminEmail.makeBody([contract: contract])
//                }
//            }
//        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contract.label', default: 'Contract'), contract.id])
                respond contract, view: 'rejected'
            }
            '*' { respond contract, [status: OK] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'booking.label', default: 'Booking'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}




