package holder
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class TipController {
    static scaffold = Tip
}
