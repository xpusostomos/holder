package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class TaskController {
    static scaffold = Task

}
