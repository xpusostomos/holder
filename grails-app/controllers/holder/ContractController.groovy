package holder



import static org.springframework.http.HttpStatus.*
import grails.validation.ValidationException
import org.apache.commons.lang3.RandomStringUtils
import org.grails.gsp.GroovyPagesTemplateEngine
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.AuthenticationException

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import grails.plugin.springsecurity.annotation.Secured

import java.time.Period
import java.time.temporal.ChronoUnit

@Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN'])
class ContractController {
    static scaffold = Contract
    def springSecurityService
    GroovyPagesTemplateEngine groovyPagesTemplateEngine
    ContractService contractService
    PersonService personService
    AccountEntryService accountEntryService
    static OK = 200
    static CREATED = 201

    def myValidateService
    def asynchronousMailService

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def index() {
        def allContracts = Contract.list([sort: 'checkIn'])
        if (request.isUserInRole('ROLE_ADMIN')) {
            respond allContracts
        } else {
            respond allContracts.findAll { springSecurityService.currentUser.rooms.contains(it.room) }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def indexShortTerm(boolean all) {
        def allContracts = Contract.list([sort: 'checkOut'])
        def aes = []
        Person person = Person.findByUser(springSecurityService.currentUser)
        if (request.isUserInRole('ROLE_ADMIN')) {
            respond allContracts, model: [all: all]
        } else {
            BigDecimal balance = 0.0
            BigDecimal openingBalance = 0.0
            LocalDate startDate = LocalDate.EPOCH
            def allaes = person.accountEntries.sort { a, b -> a.date <=> b.date ?: a.id <=> b.id }
            if (all) {
                aes = allaes
            } else {
                allaes.each { ae ->
                    def prevBalance = balance
                    balance = balance + ae.personAmount
                    aes = aes + ae
                    if (balance > prevBalance) {
                        aes = []
                        startDate = ae.date
                        openingBalance = balance
                    }
                }
            }
            respond allContracts.findAll { springSecurityService.currentUser.rooms.contains(it.room) && it.checkOut.toLocalDate() > startDate }, model: [all: all, accountEntries: aes, openingBalance: openingBalance]
        }
    }

    protected load(Contract contract) {
        contract.accountEntries.each {it.id}
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def createShortTerm(boolean all) {
        respond new Contract(params), model: [all: all]
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN'])
    def show(Long id) {
        Contract c = contractService.get(id)
        // For Julia
        if (request.isUserInRole('ROLE_ADMIN') || request.isUserInRole('ROLE_VICE_ADMIN') || springSecurityService.currentUser.rooms.contains(c.room)) {
            respond c
        } else {
            throw new BadCredentialsException("Permission denied")
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def showShortTerm(Long id) {
        Contract c = contractService.get(id)
        if (request.isUserInRole('ROLE_ADMIN') || springSecurityService.currentUser.rooms.contains(c.room)) {
            respond c
        } else {
            throw new BadCredentialsException("Permission denied")
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def editShortTerm(Long id) {
        respond contractService.get(id)
    }

//
//    def edit(Long id) {
//        if (id) {
//            respond Contract.get(id)
//        } else {
//            respond new Contract(
//                    start: LocalDateTime.of(Config.findByName(Config.names.NEXT_RENT_DATE).date, LocalTime.of(12, 0)),
//                    rentPeriod: Config.findByName(Config.names.RENT_PERIOD).number.longValue()
//            )
//        }
//    }
//
//    def add() {
//        redirect action: 'edit'
//    }
//
//    def save(Contract contract) {
//        if (contract.validate()) {
//            Contract.withTransaction {
//                contract.save()
//                contract.generateFees()
//            }
//            redirect action: 'index'
//        } else {
//            flash.errors = myValidateService.checkErrors([contract])
//            respond view: 'edit', contract
//        }
//    }

//    def delete(Contract contract) {
//        Contract.withTransaction {
//            contract.delete()
//        }
//        redirect action: 'index'
//    }

    def save(Contract contract) {
        if (contract == null) {
            notFound()
            return
        }

        load(contract)
        try {
            contractService.save(contract)
        } catch (ValidationException e) {
            respond contract.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contract.label', default: 'Contract'), contract.id])
                redirect contract
            }
            '*' { respond contract, [status: CREATED] }
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def updateShortTerm(Contract contract) {
        if (contract == null) {
            notFound()
            return
        }

        load(contract)
        try {
            personService.saveShortTermContract(contract.person, contract)
        } catch (ValidationException e) {
            respond contract.errors, view:'editShortTerm'
            return
        }
        redirect action: 'indexShortTerm'
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN', 'ROLE_USER'])
    def saveShortTerm(Contract contract, boolean all) {
        if (contract == null) {
            notFound()
            return
        }

        load(contract)
        try {
            personService.saveShortTermContract(contract.person, contract)
        } catch (ValidationException e) {
            respond contract.errors, view:'editShortTerm', model: [all: all]
            flash.message = contract.errors
            return
        }
        redirect action: 'indexShortTerm', params: [all: all]
    }

    def update(Contract contract) {
        if (contract == null) {
            notFound()
            return
        }
        load(contract)
        try {
            contractService.save(contract)
        } catch (ValidationException e) {
            respond contract.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'contract.label', default: 'Contract'), contract.id])
                redirect contract
            }
        '*'{ respond contract, [view: 'show', status: OK] }
        }
    }


def redraw(Contract contract) {
        render view: 'drawUp', model: [booking: contract.booking, contract: contract]
    }

    // TODO Need to allow a definite move out date, if we decide to.
    def drawUp(Booking booking) {
//        def foo = booking.class.constrainedProperties['agreed']?.appliedConstraints?.find { it instanceof org.grails.datastore.gorm.validation.constraints.NullableConstraint }?.nullable

        Contract contract = new Contract(
                booking: booking,
                checkIn: LocalDateTime.of(booking.checkIn, LocalTime.of(12, 0)),
                checkOut: LocalDateTime.of(booking.checkOut, LocalTime.of(12, 0)),
                rentPeriod: Config.findByName(ConfigNameEnum.RENT_PERIOD).long
        )
        render view: 'drawUp', model: [booking: booking, contract: contract]
    }

    def drawUp2(Contract contract) {
        if (contract.validate()) {
//            contract.contractTerms = groovyPageRenderer.render(template: '/contract/contractTerms', model: [booking: booking, contract: contract, fees: contract.generateAccountEntriesForEntireContract()])
            contract.contractTerms = HtmlTemplate.findTemplate('CONTRACT', contract.room)?.make([principal: User.principal, booking: contract.booking, contract: contract, fees: contract.generateAccountEntriesForEntireContract()])
            render view: 'drawUp2', model: [contract: contract]
        } else {
            render view: 'drawUp', model: [contract: contract]
        }
    }

    // TODO do we really need this or is it just a special case of edit contract
    def resume(Contract contract) {
        render view: 'drawUp2', model: [contract: contract]
    }

    def preview(Booking booking, Contract contract) {
        render view: 'drawUp2', model: [contract: contract]
    }

//    def foo() {
//        def output = new StringWriter()
//        groovyPagesTemplateEngine.createTemplate(templateText, 'sample').make([show: true, items: ['Grails','Groovy']]).writeTo(output)
//        render output.toString()
//    }

    def manualSignup(Contract contract) {
        load(contract)
        try {
            personService.signup(contract)
        } catch (ValidationException e) {
            flash.message = "signup failed " + e.localizedMessage
            flash.error = true
            respond contract.errors, view: 'show'
            return
        }
        respond contract, view: 'show'
    }

    def manualCancel(Contract contract) {
        contract.agreed = false
        contract.closed = true
        try {
            contractService.save(contract)
        } catch (ValidationException e) {
            flash.message = "cancel failed " + e.localizedMessage
            flash.error = true
            respond contract.errors, view: 'show'
            return
        }
        respond contract, view: 'show'
    }

    def send(Booking booking, Contract contract) {
        // https://www.baeldung.com/java-random-string
        contract.passKey = RandomStringUtils.randomAlphabetic(16)
        booking.positiveDecision = true
        booking.dateDecided = LocalDate.now()
        if (contract.validate() && booking.validate()) {
            Contract.withTransaction {
                contract.save()
            }
            EmailTemplate.findByName('APPLICATION_APPROVED')?.sendMail([contract: contract])
            flash.messages = ["Contract sent"]
            redirect controller: 'admin'
        } else {
            respond contract.errors, view: 'drawUp2'
        }
    }

    def saveToResume(Contract contract) {
        if (contract == null) {
            notFound()
            return
        }

        try {
            contract.booking.dateDecided = LocalDate.now()
            contract.booking.positiveDecision = true
            // We drew or redrew the contract, start from scratch
            new ArrayList(contract.accountEntries).findAll { it.type == AccountEntryTypeEnum.RENT }.forEach { ae ->
                ae.delete()
                contract.removeFromAccountEntries(ae)
            }
            contractService.save(contract)
            contract.generateSignupAccountEntries().forEach { it.save() }
            contractService.save(contract)
        } catch (ValidationException e) {
            respond contract.errors, view: 'create'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'contract.label', default: 'Contract'), contract.id])
                redirect contract
            }
            '*' { respond contract, [status: CREATED] }
        }
    }

    def extendContract(Contract contract) {
//        if (contract.successor) {
//            flash.message = "Cannot extend contract that already has a successor"
//            flash.error = true
//            respond contract, view: 'show'
//            return
//        }
        contract = contract.finalContract
        respond new Contract(
                room: contract.room,
                carSpot: contract.carSpot,
                checkIn: contract.checkOut,
                person: contract.person,
                rent: contract.rent,
                bond: 0.0,
                rentPeriod: contract.rentPeriod,
                predecessor: contract,
                useStdPayDates: contract.useStdPayDates
        ), view: 'create'
    }

    def host() {
        return request.getScheme() + "://" + request.getServerName() + (request.getServerPort() > 0 ? ':' + request.getServerPort() : '')
    }

    def moveOut(Contract contract) {
        contract.noticeGiven = LocalDateTime.now()
        render view: 'moveOut', model: [ contract: contract ]
    }

    def saveMoveOut(Contract contract) {
        if (contract.payUntil.isBefore(contract.checkOut.toLocalDate()) & !params.override) {
            flash.message = "Warning: trying to move out without completing contract. Tick to override"
            render view: 'moveOut', model: [ contract: contract ]
        } else {
            LocalDate roomEmpty = contract.payUntil.plusDays(1)
            List<AccountEntry> refundable = contract.guestAccountEntries.sort { a, b -> a.periodBegin <=> b.periodBegin }.findAll { it.periodBegin <= roomEmpty && roomEmpty <= it.periodEnd }
            AccountEntry refund = contract.guestAccountEntries.find { it.type == AccountEntryTypeEnum.BOND_REFUND }
            if (refundable.size() > 0) {
                LocalDate end = refundable.last().periodEnd
                long days = ChronoUnit.DAYS.between(roomEmpty, end) + 1
                if (!refund) {
                    refund = new AccountEntry(
                            person: contract.person,
                            contract: contract,
                            room: contract.room,
                            debitAccountId: ChartOfAccounts.RENT_INCOME.id,
                            creditAccountId: ChartOfAccounts.RENT_DEBTORS.id,
                            type: AccountEntryTypeEnum.BOND_REFUND
                    )
                }
                refund.periodBegin = roomEmpty
                refund.periodEnd = end
                refund.description = "Rent refund for period $roomEmpty to $end for ${days} days"
                refund.date = roomEmpty.minusDays(1)
                refund.amount = contract.rentForNumberOfDays(days as int)
                refund.save()
            } else {
                if (refund) {
                    contract.removeFromAccountEntries(refund)
                    refund.delete()
                }
            }
            contractService.save(contract)
            render view: 'show', model: [ contract: contract ]
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: '${propertyName}.label', default: '${className}'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
