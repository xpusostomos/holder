package holder


import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

import java.time.LocalDate

@Secured('ROLE_ADMIN')
class AssetController {
    static scaffold = AccountEntry
    AssetService assetService
    AccountEntryService accountEntryService
    def messageSource

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
//        params.max = Math.min(max ?: 10, 100)
        params.sort = 'id'
        params.order = 'asc'
        respond assetService.findByDepreciationMethodIsNotNull(params)
    }

    def depSchedule(FinanceCommand c) {
        LocalDate now = LocalDate.now()
        int yr = now.year
        if (now.monthValue <= 6) {
            yr--
        }
        if (!c.fromDate) {
            c.fromDate = LocalDate.of(yr, 7, 1)
        }
        if (!c.toDate) {
            c.toDate = LocalDate.of(yr+1, 6, 30)
        }
        params.sort = 'date'
        params.order = 'desc'
        Set<AccountEntry> entries
        if (c.premises) {
            entries = assetService.findByDepreciationMethodIsNotNullAndPremises(c.premises, params)
        } else {
            entries = assetService.findByDepreciationMethodIsNotNull(params)
        }
        def schedule = entries.collect {
            [
                    source: it,
                    accumulatedDepreciation: it.accumulatedDepreciation(c.fromDate),
                    openingValue: it.openingValue(c.fromDate),
                    depreciation: it.depreciation(c.fromDate, c.toDate),
                    depreciationRate: it.getDepreciationRate(c.fromDate, c.toDate),
                    closingValue: it.openingValue(c.toDate.plusDays(1)),
                    accountEntry: it.depreciation.find { it.periodBegin == c.fromDate }
            ]
        }
        render view: 'depSchedule', model:[schedule: schedule, c: c ]
    }

    def generate(FinanceCommand c) {
        Set<AccountEntry> entries = assetService.findByDepreciationMethodIsNotNull(params).sort { a, b -> b.date <=> a.date }
        // this won't give a nice message if validation fails....
//        AccountEntry.withTransaction {
        Set<AccountEntry> depEntries = new HashSet<>()
        try {
            entries.each {
                def dep = it.generateDepreciation(c.fromDate, c.toDate)
                if (!dep.validate()) {
                    log.error "dep: $dep"
                }
                dep.save()
                depEntries.add(dep)
            }
            accountEntryService.save(depEntries)
        } catch (ValidationException x) {
//            messageSource.getMessage(e, LocaleContextHolder.getLocale())
            flash.message = x.localizedMessage
            x.printStackTrace()
        }
        redirect action: 'depSchedule', model: [c: c, depEntries: depEntries]
    }

    def lowValuePool(FinanceCommand c) {
        Set<AccountEntry> entries = assetService.findByDepreciationMethodIsNotNull(params)
        entries.each {
            if ((it.depreciationMethod == DepreciationMethod.DIMINISHING
                    || it.depreciationMethod == DepreciationMethod.LOW_VALUE_POOL)
                    && it.openingValue(c.getFromDate()) < 1000.00) {
                it.depreciationMethod = DepreciationMethod.LOW_VALUE_POOL
//                it.depreciationRate = 0.375
                it.usefulLife = 2.666666
                assetService.save(it)
            }
        }
        redirect action: 'depSchedule', model: [c: c]
    }

    def show(Long id) {
        respond assetService.get(id)
    }

    def create() {
        respond new AccountEntry(params)
    }

    def save(AccountEntry asset) {
        if (asset == null) {
            notFound()
            return
        }
        try {
            // Kind of a hack because of grails issue #430
            asset.depreciation.each { it.validate() }
            assetService.save(asset)
        } catch (ValidationException e) {
            respond asset.errors, view: 'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'asset.label', default: 'AccountEntry'), asset.id])
                redirect action: 'show', id: asset.id
            }
            '*' { respond asset, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond assetService.get(id)
    }

    def update(AccountEntry asset) {
        if (asset == null) {
            notFound()
            return
        }

        try {
            // Kind of a hack because of grails issue #430
            asset.depreciation.each { it.validate() }
            assetService.save(asset)
        } catch (ValidationException e) {
            respond asset.errors, view: 'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'asset.label', default: 'AccountEntry'), asset.id])
                redirect action: 'show', id: asset.id
            }
            '*' { respond asset, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        assetService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'asset.label', default: 'AccountEntry'), id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'asset.label', default: 'AccountEntry'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
