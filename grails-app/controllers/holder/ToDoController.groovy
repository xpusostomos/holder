package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class ToDoController {
    static scaffold = ToDo
}
