package holder

import grails.plugin.springsecurity.annotation.Secured

//
//@Secured('ROLE_ADMIN')
//class CalenderController {
//    def index() {}
//}
//import ch.silviowangler.grails.icalender.CalendarExporter
import grails.plugin.springsecurity.annotation.Secured
import net.fortuna.ical4j.model.Date



@Secured('ROLE_ADMIN')
class CalendarController {
//        implements CalendarExporter {
    def index = {
        renderCalendar {
            calendar {
                events {
//                    event(start: Date.parse('dd.MM.yyyy HH:mm', '31.10.2009 14:00'), end: Date.parse('dd.MM.yyyy HH:mm', '31.10.2009 15:00'), description: 'Events description', summary: 'Short info1') {
//                        organizer(name: 'Silvio Wangler', email: 'silvio.wangler@gmail.com')
//                    }
//                    event(start: Date.parse('dd.MM.yyyy HH:mm', '01.11.2009 14:00'), end: Date.parse('dd.MM.yyyy HH:mm', '01.11.2009 15:00'), description: 'hell yes', summary: 'Short info2', location: '@home', classification: 'private')
//                    event(start: new Date(), end: new Date(), description: 'Events description', summary: 'Short info1') {
//                        organizer(name: 'Silvio Wangler', email: 'silvio.wangler@gmail.com')
//                    }
//                    event(start: new Date(), end: new Date(), description: 'hell yes', summary: 'Short info2', location: '@home', classification: 'private')
                    event start: new Date(), end: new Date(), description: 'hell yes', summary: 'Short info2', location: '@home', classification: 'private'
                }
            }
        }
    }
}
