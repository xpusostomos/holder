package holder


import static org.springframework.http.HttpStatus.*
import grails.compiler.GrailsCompileStatic
import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException

@Secured('ROLE_ADMIN')
class AccountController {
    static scaffold = Account
    AccountService accountService

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 1000)
        params.sort = 'id'
//        def foo = accountService.list(params).find { it.id == null}
        respond accountService.list(params), model:[accountCount: accountService.count()]
    }

    def save() {
        // Grails data binding hates self-assigned ids
        //TODO instead of this crap, let's try bindable: true in the domain object
        Account account = new Account()
        bindData(account, params)
        bindData(account, params, [include: ['id']])
    if (account == null) {
        notFound()
        return
    }

    try {
        accountService.save(account)
    } catch (ValidationException e) {
        respond account.errors, view:'create'
        return
    }

    request.withFormat {
        form multipartForm {
            flash.message = message(code: 'default.created.message', args: [message(code: 'account.label', default: 'Account'), account.id])
            redirect account
        }
        '*' { respond account, [status: CREATED] }
    }
}
    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'account.label', default: 'Account'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
