package holder

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

@Secured(['ROLE_ADMIN', 'ROLE_ACCOUNT_ADD', 'ROLE_ACCOUNT_EDIT'])
class AccountEntryController {
    static scaffold = AccountEntry
    AccountEntryService accountEntryService
    PersonService personService
    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        params.sort = 'id'
        params.order = 'desc'
//        def user = User.get(springSecurityService.principal.id)
        User user = springSecurityService.currentUser
        def roles = springSecurityService.principal.authorities.collect{it.role}
        if (roles.contains('ROLE_ADMIN') || roles.contains('ROLE_ACCOUNT_EDIT')) {
            respond accountEntryService.list(params), model: [accountEntryCount: accountEntryService.count()]
        } else {
            respond accountEntryService.findAllByUserCreated(user, params), model: [accountEntryCount: accountEntryService.countByUserCreated(user)]
        }
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN'])
    def acceptPayment(Person person) {
        AccountEntry accountEntry = new AccountEntry(
                person: person,
                room: person.currentRoom
        )
        respond accountEntry, view: 'acceptPayment'
    }

    def recordDebt(Person person) {
        AccountEntry accountEntry = new AccountEntry(
                person: person,
                room: person.currentRoom
        )

        respond accountEntry, view: 'recordDebt'
    }

    @Secured(['ROLE_ADMIN', 'ROLE_VICE_ADMIN'])
    def savePayment(AccountEntry accountEntry) {
        if (accountEntry == null) {
            notFound()
            return
        }
        accountEntry.description = 'Payment Received'
        accountEntry.creditAccountId = ChartOfAccounts.RENT_DEBTORS.id
        accountEntry.type = AccountEntryTypeEnum.PAYMENT
//        if (accountEntry.amount.signum() > 0) {
//            accountEntry.amount = accountEntry.amount.negate()
//        }
        if (accountEntry.method == AccountEntryMethodEnum.BANK) {
            accountEntry.debitAccountId = ChartOfAccounts.BANK.id
        } else if (accountEntry.method == AccountEntryMethodEnum.CASH) {
            accountEntry.debitAccountId = ChartOfAccounts.CASH.id
        } else if (accountEntry.method == AccountEntryMethodEnum.WRITEOFF) {
            accountEntry.debitAccountId = ChartOfAccounts.BAD_DEBTS.id
        }
        try {
            accountEntry.save()
            accountEntryService.save(accountEntry)
            accountEntry.person.paymentNotified = false
            accountEntry.person.waitingOnPayment = false
            personService.save(accountEntry.person)
        } catch (ValidationException e) {
            respond accountEntry.errors, view: 'acceptPayment'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), accountEntry.id])
                redirect controller: 'person', action: 'show', id: accountEntry.person.id
            }
            '*' { respond accountEntry, [status: CREATED] }
        }
    }

    def refundBond(Person person) {
        AccountEntry accountEntry = new AccountEntry(
                person: person,
                amount: person.bondHeld,
                room: person.currentRoom
        )
        respond accountEntry, view: 'refundBond'
    }

    def saveRefundBond(AccountEntry accountEntry) {
        if (accountEntry == null) {
            notFound()
            return
        }
        accountEntry.description = 'Bond Refund Due'
        accountEntry.debitAccountId = ChartOfAccounts.BOND_HELD.id
        accountEntry.type = AccountEntryTypeEnum.BOND_REFUND
//        if (accountEntry.amount.signum() > 0) {
//            accountEntry.amount = accountEntry.amount.negate()
//        }
        accountEntry.creditAccountId = ChartOfAccounts.RENT_DEBTORS.id
//        AccountEntry accountEntry2 = new AccountEntry(
//                date: accountEntry.date,
//                person: accountEntry.person,
//                room: accountEntry.room,
//                premises: accountEntry.premises,
//                method: accountEntry.method,
//                amount: accountEntry.amount,
//                type: AccountEntryTypeEnum.BOND_REFUND,
//                debitAccount: ChartOfAccounts.RENT_DEBTORS.id,
//                description: 'Bond refunded'
//        )
//        if (accountEntry.method == AccountEntryMethodEnum.BANK) {
//            accountEntry2.creditAccountId = ChartOfAccounts.BANK.id
//        } else if (accountEntry.method == AccountEntryMethodEnum.CASH) {
//            accountEntry2.creditAccountId = ChartOfAccounts.CASH.id
//        } else if (accountEntry.method == AccountEntryMethodEnum.WRITEOFF) {
//            accountEntry2.creditAccountId = ChartOfAccounts.WRITEOFF.id
//        }
        accountEntry.method = null
        try {
            accountEntry.save()
//            accountEntry2.save()
            accountEntryService.save(accountEntry)
//            accountEntryService.save(accountEntry2)
        } catch (ValidationException e) {
            respond accountEntry.errors, view: 'refundBond'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), accountEntry.id])
                redirect controller: 'person', action: 'show', id: accountEntry.person.id
            }
            '*' { redirect controller: 'person', action: 'show', id: accountEntry.person.id,  [status: CREATED] }
        }
    }

//    def payRefund(Person person) {
//        AccountEntry accountEntry = new AccountEntry(
//                person: person,
//                amount: -person.amountOwing,
//                room: person.currentRoom
//        )
//        respond accountEntry, view: 'payRefund'
//    }

//    def savePayRefund(AccountEntry accountEntry) {
//        if (accountEntry == null) {
//            notFound()
//            return
//        }
//        accountEntry.description = 'Refund paid'
//        accountEntry.room = accountEntry.room
//        accountEntry.type = AccountEntryTypeEnum.PAY_REFUND
//        accountEntry.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
////        AccountEntry accountEntry2 = new AccountEntry(
////                date: accountEntry.date,
////                person: accountEntry.person,
////                room: accountEntry.room,
////                premises: accountEntry.premises,
////                method: accountEntry.method,
////                amount: accountEntry.amount,
////                type: AccountEntryTypeEnum.BOND_REFUND,
////                debitAccount: ChartOfAccounts.RENT_DEBTORS.id,
////                description: 'Bond refunded'
////        )
//        if (accountEntry.method == AccountEntryMethodEnum.BANK) {
//            accountEntry.creditAccountId = ChartOfAccounts.BANK.id
//        } else if (accountEntry.method == AccountEntryMethodEnum.CASH) {
//            accountEntry.creditAccountId = ChartOfAccounts.CASH.id
//        } else if (accountEntry.method == AccountEntryMethodEnum.WRITEOFF) {
//            accountEntry.creditAccountId = ChartOfAccounts.BAD_DEBTS.id
//        }
//        try {
//            accountEntry.save()
////            accountEntry2.save()
//            accountEntryService.save(accountEntry)
////            accountEntryService.save(accountEntry2)
//        } catch (ValidationException e) {
//            respond accountEntry.errors, view: 'payRefund'
//            return
//        }
//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.created.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), accountEntry.id])
//                redirect controller: 'person', action: 'show', id: accountEntry.person.id
//            }
//            '*' { redirect controller: 'person', action: 'show', id: accountEntry.person.id,  [status: CREATED] }
//        }
//    }

    def keepBond(Person person) {
        AccountEntry accountEntry = new AccountEntry(
                person: person,
                amount: person.bondHeld,
                room: person.currentRoom
        )
        respond accountEntry, view: 'keepBond'
    }


    def saveKeepBond(AccountEntry accountEntry) {
        if (accountEntry == null) {
            notFound()
            return
        }
        BigDecimal amount = accountEntry.amount;
        BigDecimal owing = accountEntry.person.amountOwing
        AccountEntry accountEntry2
        if (amount > owing) {
             accountEntry2 = new AccountEntry(
                    person: accountEntry.person,
                    room: accountEntry.room,
                    amount: amount - owing,
                    description: 'Fee requiring forfeiture of bond',
                    debitAccountId: ChartOfAccounts.RENT_DEBTORS.id,
                    creditAccountId: ChartOfAccounts.RENT_INCOME.id,
                    type: AccountEntryTypeEnum.FEE
            )
        }
        accountEntry.description = 'Bond Kept'
        accountEntry.debitAccountId = ChartOfAccounts.BOND_HELD.id
        accountEntry.type = AccountEntryTypeEnum.BOND_KEPT
        accountEntry.creditAccountId = ChartOfAccounts.RENT_DEBTORS.id
        try {
            if (accountEntry2) {
                accountEntry2.save()
                accountEntryService.save(accountEntry2)
            }
            accountEntry.save()
            accountEntryService.save(accountEntry)
        } catch (ValidationException e) {
            respond accountEntry.errors, view: 'keepBond'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), accountEntry.id])
                redirect controller: 'person', action: 'show', id: accountEntry.person.id
            }
            '*' { redirect controller: 'person', action: 'show', id: accountEntry.person.id,  [status: CREATED] }
        }
    }

    def saveDebt(AccountEntry accountEntry) {
        if (accountEntry == null) {
            notFound()
            return
        }
        accountEntry.creditAccountId = ChartOfAccounts.FEES.id
        accountEntry.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
        try {
            accountEntry.save()
            accountEntryService.save(accountEntry)
        } catch (ValidationException e) {
            respond accountEntry.errors, view: 'recordDebt'
            return
        }
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), accountEntry.id])
                redirect controller: 'person', action: 'show', id: accountEntry.person.id
            }
            '*' { respond accountEntry, [status: CREATED] }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'accountEntry.label', default: 'AccountEntry'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def bulkEntry(BulkEntryCommand bulk) {
//        if (!bulk) {
//            bulk = new BulkEntryCommand()
//            foreach (i; 0 .. 10) {
//                bulk.entries.add(new BulkEntrySubCommand())
//            }
//        }
//        bulk.doValidate()
        render view: 'bulkEntry', model: [ bulk: bulk ]
    }

    def bulkAddTen(BulkEntryCommand bulk) {
        bulk.addTen()
        bulk.validate()
        render view: 'bulkEntry', model: [ bulk: bulk ]
    }

    def bulkSave(BulkEntryCommand bulk) {
        if (bulk.validate()) {
            List<AccountEntry> entries = new ArrayList<>()
            bulk.entries.each {
                if (it.hasEntry()) {
                    entries.add(
                            new AccountEntry(
                                    periodBegin: bulk.periodBegin,
                                    periodEnd: bulk.periodBegin,
                                    debitAccount: bulk.debitSingle ? bulk.singleAccount : it.account,
                                    creditAccount: bulk.debitSingle ? it.account : bulk.singleAccount,
                                    amount: it.amount,
                                    date: it.date,
                                    description: it.description,
                                    premises: it.premises,
                                    provisional: it.provisional,
                                    usefulLife: it.usefulLife,
                                    depreciationMethod: it.depreciationMethod,
                                    depreciationAccount: it.account.depreciation,
                                    notes: it.notes))
                }
            }
            accountEntryService.save(entries)
            flash.message = "${entries.size()} Account Entries saved"
            bulk.initEntries()
        }
        render view: 'bulkEntry', model: [ bulk: bulk ]
    }
}

