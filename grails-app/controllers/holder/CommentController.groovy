package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class CommentController {
    static scaffold = Comment
//    def index() { }
}
