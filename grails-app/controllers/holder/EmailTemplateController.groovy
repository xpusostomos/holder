package holder

import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class EmailTemplateController {
    static scaffold = EmailTemplate

    def email(BulkEmailCommand bulkEmailCommand) {
        render view: 'email', model: [bulkEmailCommand: new BulkEmailCommand()]
    }

    def analyseEmail(EmailTemplate command) {
        // TODO what is this garbage?
        def emailTemplateCommandList =
                Perosn.currentGuests.collect { EmailTemplate.findTemplate(command.name, command.tags, command.room) }
        render view: 'analyseEmail', model: [personList: Person.currentGuests]
    }

    def copy(EmailTemplate emailTemplate) {
        EmailTemplate et = new EmailTemplate(
                name: emailTemplate.name,
                room: emailTemplate.room,
                toAddress: emailTemplate.toAddress,
                ccAddress: emailTemplate.ccAddress,
                bccAddress: emailTemplate.bccAddress,
                tags: emailTemplate.tags,
                subject: emailTemplate.subject,
                body: emailTemplate.body,
                attachments: emailTemplate.attachments
        )
        render view: 'create', model: [emailTemplate: et]
    }
}
