package holder

class UrlMappings {
    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
//        "/"(controller:'auth', action:'index')
//        "/"(view:"/index")
        "/" (controller: 'root')
        "/$action/$id?(.$format)?" (controller: 'root')
        "/robots.txt" (view: "/robots")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
