package holder
import grails.plugin.springsecurity.annotation.Secured

@Secured('ROLE_ADMIN')
class HtmlTemplateController {
    static scaffold = HtmlTemplate

}
