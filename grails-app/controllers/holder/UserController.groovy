package holder

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileStatic

@Secured('ROLE_ADMIN')
class UserController extends grails.plugin.springsecurity.ui.UserController {
    def show(User u) {
        redirect action: 'edit', id: u.id
    }

    protected Map buildUserModel(user) {
        Set userRoleNames = user[authoritiesPropertyName].collect { it[authorityNameField] }
        Map roleMap = buildRoleMap(userRoleNames, sortedRoles())

        Set userRoomNames = user.rooms.collect { it.name }
        Map roomMap = buildRoomMap(userRoomNames, sortedRooms())

        [roleMap: roleMap, roomMap: roomMap, tabData: tabData, user: user]
    }

    protected List sortedRooms() {
        Room.findAllByLevel(holder.RoomLevelEnum.PREMISES, [sort: 'name'])
    }

    @CompileStatic
    protected Map buildRoomMap(Set<String> userRoomNames, List<Room> sortedRooms) {
        Map granted = [:]
        Map notGranted = [:]
        for (room in sortedRooms) {
            if (userRoomNames?.contains(room.name)) {
                granted[(room)] = true
            } else {
                notGranted[(room)] = false
            }
        }
        granted + notGranted
    }
}
