package holder

import java.time.LocalDate
import java.time.LocalDateTime

enum SexEnum {
    MALE, FEMALE, COUPLE, COMPANY
}

enum PersonType {
    GUEST, AGENT, SITE
}

class  Person {
    User user
    String email
    String firstName
    String lastName
    PersonType type
    String billingEmail
    String billingFirstName
    String billingLastName
    String phone
    String notes
    SexEnum sex
    String abn
    String employment
    Boolean emailEnabled = true
    Boolean smsEnabled = false
    Boolean paymentNotified = false
    Boolean waitingOnPayment = false
    String tags
    Photo photo
    Photo passport
    Photo avatar
    Album album
    Set<AccountEntry> accountEntries = []
    Set<Contract> contracts = []
//    Set<Query> queries = []
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static transients = [ 'amountOwing', 'bondHeld', 'home', 'rooms', 'lastPayment', 'emailDisplay', 'billingEmailDisplay', 'tagList', 'currentGuests', 'currentRoom', 'paymentDue']

    static hasMany = [accountEntries: AccountEntry,
//                      queries: Query,
                      contracts: Contract
    ]

//    static fetchMode = [accountEntries: 'eager'] // used for the person edit standard gui. Probably should change the gui


    static constraints = {
        user nullable: true
        firstName nullable: false
        lastName nullable: true
        email nullable: true, size: 6..320, email: true
        billingEmail nullable: true, size: 6..320, email: true
        billingFirstName nullable: true
        billingLastName nullable: true
        phone nullable: true, maxSize: 15
        sex nullable: false
        abn nullable: true
        employment nullable: false
        emailEnabled nullable: false
        smsEnabled nullable: false
        paymentNotified nullable: false
        waitingOnPayment nullable: false
        type nullable: false
        tags nullable: true, maxSize: 512
        notes nullable: true, widget: 'textarea'
        photo nullable: true
        avatar nullable: true
        passport nullable: true
        album nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        user lazy: false
        emailEnabled defaultValue: 'true'
        smsEnabled defaultValue: 'false'
        paymentNotified defaultValue: 'false'
        waitingOnPayment defaultValue: 'false'
        accountEntries sort: 'date', cascade: 'all-delete-orphan'
//        queries cascade: 'all-delete-orphan'
        contracts sort: 'checkIn', cascade: 'all-delete-orphan'
//        bookings cascade: 'all-delete-orphan'
        photo cascade: 'all-delete-orphan'
        passport cascade: 'all-delete-orphan'
        avatar cascade: 'all-delete-orphan'
        album cascade: 'all-delete-orphan'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (!photo?.data || photo?.data?.length == 0) {
            photo = null
        }
        if (!avatar?.data  || avatar?.data?.length == 0) {
            avatar = null
        }
        if (!passport?.data || passport?.data?.length == 0) {
            passport = null
        }
        if (album?.empty) {
            album = null
        }
    }

    List<String> getTagList() {
        return Arrays.asList((tags?:'').split('\\|')).findAll{it}.sort().unique()
    }

    List<String> getSpecialTagList() {
        List<String> rtn = new ArrayList<>()
        if (amountOwing > 0.0) {
            rtn.add('owing')
        }
        return rtn.sort().unique()
    }

    List<String> getAllTagList() {
        return [*tagList, *specialTagList].unique().sort()
    }

    void setTagList(Collection<String> v) {
        this.tags = v.join('|')
        if (tags) {
            tags = '|' | tags | '|'
        }
    }

    String getEmailDisplay() {
        return email ? "$name <$email>" : null
    }

    String getBillingName() {
        return (billingFirstName ?: '') + ' ' + (billingLastName ?: '')
    }

    String getBillingEmailDisplay() {
        return billingEmail ? "$billingName <$billingEmail>" : null
    }

    Link getHome() {
        new Link(name: 'HOME', controller: 'guest', action: 'home', id: id)
    }

    static getCurrentGuests() {
        LocalDateTime now = LocalDateTime.now()
        return Room.findAll().collect { it.getLesseeByDateTime(now)}.findAll { it}.sort {a, b -> a.name <=> b.name }
    }

    static List<String> getAllTags() {
        Room.list().findAll { it.lessee }.collectMany { it.lessee.allTagList }.unique().sort()
    }

    Collection<Room> getRooms() {
        LocalDateTime now = LocalDateTime.now()
        contracts.findAll{ !it.predecessor && !it.closed && it.agreed && it.moveOut < now }.collect { it.room }
    }

    /**
     * getCurrentRoom... not perfect because one could rent more than one room, but good enough
     * @return
     */
    Room getCurrentRoom() {
        contracts.findAll{ !it.predecessor && !it.closed && it.agreed }.sort { a, b -> b.checkOut <=> a.checkOut}.first().room
    }

    BigDecimal getAmountOwing() {
        return (BigDecimal)accountEntries*.personAmount.sum(0.0)
    }

    Map<Room,BigDecimal> getBalances() {
        return accountEntries.collect {it.premises}.
                collectEntries{ Room premises -> [premises, accountEntries.findAll { it.premises == premises}*.personAmount.sum(0.0)] }
    }

    Map<Room,BigDecimal> getPotentialTransferByPremises(BigDecimal x) {
        Map<Room,BigDecimal> rtn = new HashMap<>()
        def obals = accountEntries.findAll { it.personAmount > 0.0 }.groupBy{it.premises}
                .collectEntries{k, v -> [k, v*.amount.sum(0.0)]}
        accountEntries.findAll { it.personAmount < 0.0}.sort {a, b -> a.date <=> b.date}.each {
            BigDecimal pa = it.amount
            BigDecimal obal = obals.get(it.premises) ?: BigDecimal.ZERO
            if (obal > pa) {
                obal -= pa
                pa = 0.0
            } else {
                pa -= obal
                obal = 0.0
            }
            obals.put(it.premises, obal)
            if (pa > x) {
                pa = x
            }
            BigDecimal bal = rtn.get(it.premises)
            if (!bal) {
                bal = BigDecimal.ZERO
            }
            bal += pa
            x = x - pa
            rtn.put(it.premises, bal)
        }
        if (x != 0.0) {
            rtn = null
        }
        return rtn?.findAll { it.value != 0.0 }
    }

    BigDecimal getBondHeld() {
        return (BigDecimal)accountEntries*.personBondAmount.sum(0.0).negate()
    }

    AccountEntry getLastPayment() {
        accountEntries.findAll {it.type == AccountEntryTypeEnum.PAYMENT}.sort {a, b -> b.date <=> a.date}.getAt(0)
    }

    LocalDate getPaymentDue() {
        accountEntries.periodBegin.max()
    }

//    String checkCredentials(String password) {
//        String message = null
//        if (disabled) {
//            message = 'au.gov.environment.ibis.auth.User.password.disabled'
//        } else if (!passwordMatches(password)) {
//            message = "au.gov.environment.ibis.auth.User.password.passwordIncorrect"
//        }
//        return message
//    }

    void loadIntoMemory() {
    }

    String getName() {
        return (firstName ?:'') + ' ' + (lastName ?: '')
    }

    String toString() {
        return "$name"
    }
}
