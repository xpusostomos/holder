package holder

import grails.plugin.asyncmail.AsynchronousMailMessage
import java.time.LocalDateTime

class MailIndex {
    AsynchronousMailMessage mail
    Long entityId
    String entityName
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static constraints = {
        mail unique: ['entityId', 'entityName']
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }
}
