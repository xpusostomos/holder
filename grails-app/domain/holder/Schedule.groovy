package holder

import java.time.Duration
import java.time.LocalDateTime

class Schedule {
    TokenService tokenService
    Task task
    Person person
    LocalDateTime due
    LocalDateTime lastReminder
    boolean complete = false
    boolean cancelled = false
    boolean sendMail = true
    int remindersSent = 0
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static transients = ['tokenService', 'late', 'lateHours']

    static constraints = {
        task nullable: false
        person nullable: false
        due nullable: false
        lastReminder nullable: false
        complete attributes: [:]
        cancelled attributes: [:]
        sendMail attributes: [:]
        lastReminder nullable: true
        remindersSent attributes: [:]
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        autowire true
        id defaultValue: "nextval('hibernate_sequence')"
        complete defaultValue: 'false'
        cancelled defaultValue: 'false'
        sendMail defaultValue: 'true'
        remindersSent defaultValue: '0'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    Duration getLate() {
        LocalDateTime now = LocalDateTime.now()
        if (due && due < now) {
            return Duration.between(due, now)
        }
        return null
    }

    Long getLateHours() {
        return late?.toHours()
    }

    LocalDateTime getNextReminder() {
        if (!lastReminder) {
            return due - task.remindEarlyInterval
        } else {
            LocalDateTime d = due
            while (d <= lastReminder) {
                d += task.remindLateInterval
            }
            return d
        }
    }

    String toString() {
        return "$person $due complete: $complete"
    }
}
