package holder

import java.math.MathContext
import java.math.RoundingMode
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period

class Contract {
    def groovyPageRenderer
    AccountEntryService accountEntryService

    Room room
    Room carSpot
    LocalDateTime checkIn
    LocalDateTime checkOut
    Person person
    BigDecimal rent
    BigDecimal bond
    Boolean checkoutDayIsPaid = true
    Long rentPeriod = 28
    Boolean agreed = false
    LocalDateTime noticeGiven
    LocalDateTime moveIn
    LocalDateTime moveOut
    LocalDate payUntil
    Booking booking
    Contract predecessor
    Boolean closed = false
    Boolean useStdPayDates = true
    Person agent
    String notes
    Set<AccountEntry> accountEntries = []
    String contractTerms
    Album album
    String passKey
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated


    static transients = [ 'accountEntryService', 'mailIndexService', 'groovyPageRenderer', 'checkoutOrMoveOut',
                          'currentRent', 'name',
                          'lastRentAccountEntry', 'active', 'successor', 'finalContract']

    static hasMany = [accountEntries: AccountEntry ]

    static belongsTo = [person: Person, room: Room, booking: Booking, predecessor: Contract]

    static constraints = {
        booking nullable: true
        predecessor nullable: true
        room nullable: false, validator: {
            val , obj ->
                if (val?.roomType != RoomTypeEnum.ROOM) return ['holder.Contract.room.invalid']
        }
        carSpot nullable: true, validator: {
            val , obj ->
                if (val && val.roomType != RoomTypeEnum.CARSPOT) return ['holder.Contract.carSpot.invalid']
        }
        moveIn nullable: true
        checkIn nullable: false
        checkOut nullable: false
        bond nullable: false
        rent nullable: false
        checkoutDayIsPaid nullable: false
        rentPeriod nullable: false
        person nullable: true
        agreed nullable: true
        noticeGiven nullable: true
        moveOut nullable: true
        payUntil nullable: true
        closed nullable: false
        useStdPayDates nullable: false
        agent nullable: true
        notes nullable: true, widget: 'textarea'
        person nullable: true
        album nullable: true
        contractTerms nullable: true, maxSize: 32672, widget: 'textarea'
        passKey nullable: true, display: false
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        autowire true
        useStdPayDates defaultValue: 'true'
        rentPeriod defaultValue: 28
        checkoutDayIsPaid defaultValue: 'true'
        agreed defaultValue: 'false'
        closed defaultValue: 'false'
        predecessor index: 'predecessor_idx'
        accountEntries sort: 'date', cascade: 'all-delete-orphan'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }

    void setCheckIn(LocalDateTime v) {
        this.checkIn = v
        if (moveIn == null) {
            this.moveIn = checkIn
        }
    }

    Collection<AccountEntry> getGuestAccountEntries() {
        return accountEntries.findAll { it.person = person }
    }

    Collection<AccountEntry> getAgentAccountEntries() {
        return accountEntries.findAll { it.person = agent }
    }

    boolean getActive() {
        return !predecessor && !closed && agreed && !(moveOut && moveOut < LocalDateTime.now())
    }

    String getName() {
        return person?.name ?: booking?.name
    }

    Contract getSuccessor() {
        //TODO what a mess this crashes on not being saved.
//        return Contract.findByPredecessor(this)
        return this
    }

    Contract getFinalContract() {
        return successor ?: this
    }

    BigDecimal getCurrentRent() {
        return finalContract.rent
    }

    LocalDateTime getCheckoutOrMoveOut() {
        if (moveOut && moveOut < checkOut) {
            return moveOut
        }
        return checkOut
    }

    AccountEntry getLastRentAccountEntry() {
        Set<AccountEntry> rentAccountEntries = this.accountEntries.findAll { it.type == AccountEntryTypeEnum.RENT }
        if (rentAccountEntries.isEmpty()) {
            return null
        } else {
            return rentAccountEntries.sort { a, b -> a.periodEnd <=> b.periodEnd }.last()
        }
    }

    LocalDate nextPeriodBegin(AccountEntry after) {
        if (!after) {
            return this.checkIn.toLocalDate()
        } else {
            after.periodEnd.plusDays(checkoutDayIsPaid ? 1 : 0)
        }
    }

    static LocalDate calendarYearFor(LocalDate aDate) {
        return LocalDate.of(aDate.year, 12, 31)
    }

    static LocalDate financialYearFor(LocalDate aDate) {
        if (aDate.monthValue <= 6) {
            return LocalDate.of(aDate.year, 6, 30)
        } else {
            return LocalDate.of(aDate.year+1, 6, 30)
        }
    }

    LocalDate periodEndFor(LocalDate aDate) {
        LocalDate stdPeriodEndDate = Config.findByName(ConfigNameEnum.STD_PERIOD_END_DATE).date
        while (stdPeriodEndDate <= aDate) {
            stdPeriodEndDate = stdPeriodEndDate.plusDays(rentPeriod)
        }
        return stdPeriodEndDate
    }

    static LocalDate stdPeriodEndFor(LocalDate aDate) {
        LocalDate stdPeriodEndDate = Config.findByName(ConfigNameEnum.STD_PERIOD_END_DATE).date
        int stdRentPeriod = Config.findByName(ConfigNameEnum.RENT_PERIOD).integer
        while (stdPeriodEndDate < aDate) {
            stdPeriodEndDate = stdPeriodEndDate.plusDays(stdRentPeriod)
        }
        return stdPeriodEndDate
    }

    static LocalDate stdPeriodStartFor(LocalDate aDate, boolean checkoutDayIsPaid) {
        LocalDate end = stdPeriodEndFor(aDate)
        int stdRentPeriod = Config.findByName(ConfigNameEnum.RENT_PERIOD).integer
        return end.minusDays(stdRentPeriod - (checkoutDayIsPaid ? 1 : 0))
    }


    LocalDate nextPeriodEnd(AccountEntry after) {
        LocalDate periodBegin = nextPeriodBegin(after)
        if (useStdPayDates) {
            return periodEndFor(periodBegin)
        } else {
            return periodBegin.plusDays(rentPeriod-(checkoutDayIsPaid ? 1 : 0))
        }
    }

    LocalDate nextPeriodEndCheckingMoveOut(AccountEntry after, LocalDate move) {
        LocalDate periodBegin = nextPeriodBegin(after)
        LocalDate periodEnd = nextPeriodEnd(after)
        if (move && move >= periodBegin && move <= periodEnd) {
            return move
        } else if (move && move < periodBegin) {
            return null
        }
        return periodEnd
    }

    int nextPeriodNumberOfDays(AccountEntry after, LocalDate move) {
        LocalDate end = nextPeriodEndCheckingMoveOut(after, move)
        int rtn = nextPeriodBegin(after).until(end).getDays() + (checkoutDayIsPaid ? 1 : 0)
        return rtn
    }

//    int getEffectiveRentPeriod() {
////        if (useStdPayDates) {
////            return Config.findByName(ConfigNameEnum.RENT_PERIOD).integer
////        } else {
//            return rentPeriod
////        }
//    }

    BigDecimal rentForNumberOfDays(int days) {
        MathContext mc = new MathContext(8, RoundingMode.HALF_UP)
        return new BigDecimal(days).divide(new BigDecimal(rentPeriod), mc).multiply(currentRent).setScale(2, RoundingMode.HALF_DOWN)
    }

    BigDecimal nextRentalFee(AccountEntry after, LocalDate move) {
        // TODO Instead of using currentRent, try and aportion to different contracts.
        return rentForNumberOfDays(nextPeriodNumberOfDays(after, move))
    }

    AccountEntry  generateAccountEntry(AccountEntry after, LocalDate move, boolean findSamePeriod) {
        LocalDate end = nextPeriodEndCheckingMoveOut(after, move)
        if (end) {
            LocalDate begin = nextPeriodBegin(after)
            int days = nextPeriodNumberOfDays(after, move)
            AccountEntry ae
            if (findSamePeriod) {
                ae = accountEntries.find {
                    it.periodBegin == begin &&
                            it.debitAccountId == ChartOfAccounts.RENT_DEBTORS.id &&
                            it.creditAccountId == ChartOfAccounts.RENT_INCOME.id }
            } else {
                ae = accountEntries.find {
                    it.debitAccountId == ChartOfAccounts.RENT_DEBTORS.id &&
                            it.creditAccountId == ChartOfAccounts.RENT_INCOME.id }
            }
            if (!ae) {
                ae = new AccountEntry(
                        contract: this,
                        periodBegin: begin
                )
            }
            ae.person = person
            ae.room = this.room
            ae.date = begin
            ae.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
            ae.creditAccountId = ChartOfAccounts.RENT_INCOME.id
            ae.periodEnd = end
            ae.amount = nextRentalFee(after, move)
            ae.type = AccountEntryTypeEnum.RENT
            ae.description = "Rent due for period $begin to $end for ${days} days"
            return ae
        }
        return null
    }

    AccountEntry generateAutoPaymentDue(AccountEntry after, LocalDate move) {
        LocalDate end = nextPeriodEndCheckingMoveOut(after, move)
        if (end) {
            LocalDate begin = nextPeriodBegin(after)
            int days = nextPeriodNumberOfDays(after, move)
            AccountEntry ae = accountEntries.find {
                it.periodBegin == begin &&
                        it.debitAccountId == ChartOfAccounts.BANK.id &&
                        it.creditAccountId == ChartOfAccounts.ACCOUNTS_PAYABLE.id
            }
            if (!ae) {
                ae = new AccountEntry(
                        contract: this,
                        periodBegin: begin
                )
            }
            ae.person = person
            ae.room = this.room
            ae.date = begin
            ae.debitAccountId = ChartOfAccounts.RENT_DEBTORS.id
            ae.creditAccountId = ChartOfAccounts.RENT_INCOME.id
            ae.periodEnd = end
            ae.amount = nextRentalFee(after, move)
            ae.type = AccountEntryTypeEnum.RENT
            ae.description = "Rent due for period $begin to $end for ${days} days"
            return ae
        }
        return null
    }

    List<AccountEntry> generateAccountEntriesUntil(LocalDate date, LocalDate payUntil, boolean restartFromBeginning = false, int max = Integer.MAX_VALUE) {
        List<AccountEntry> rtn = new ArrayList<>()
        AccountEntry latest
        if (!restartFromBeginning) {
            latest = lastRentAccountEntry
            if (payUntil && latest && latest.periodEnd >= payUntil) {
                this.closed = true
            }
        }
        while (!latest || (latest.periodEnd < date && (!payUntil || latest.periodEnd < payUntil))) {
            latest = generateAccountEntry(latest, payUntil, true)
            if (latest) {
                rtn.add(latest)
                if (rtn.size() == max) {
                    break
                }
            } else {
                break
            }
        }
        return rtn
    }

    List<AccountEntry> generateAccountEntriesUntil(LocalDate date, boolean restartFromBeginning = false, int max = Integer.MAX_VALUE) {
        generateAccountEntriesUntil(date, payUntil, restartFromBeginning, max)
    }

    AccountEntry generateBondAccountEntry() {
        if (bond > 0) {
            AccountEntry ae
            ae = accountEntries.find {
                    it.debitAccountId == ChartOfAccounts.RENT_DEBTORS.id &&
                            it.creditAccountId == ChartOfAccounts.BOND_HELD.id }
            if (!ae) {
                ae = new AccountEntry(
                        person: person,
                        contract: this,
                        debitAccountId: ChartOfAccounts.RENT_DEBTORS.id,
                        creditAccountId: ChartOfAccounts.BOND_HELD.id,
                )
            }
            ae.room = this.room
            ae.date = LocalDate.now().isBefore(checkIn.toLocalDate()) ? LocalDate.now() : checkIn.toLocalDate()
            ae.amount = bond
            ae.type = AccountEntryTypeEnum.BOND
            ae.description = "Bond deposit due"
            return ae
        }
    }

    int getDaysInContract() {
        return Period.between(checkIn.toLocalDate(), checkOut.toLocalDate()).days + (checkoutDayIsPaid ? 1 : 0)
    }

    // TODO why do we add it to person.accountEntries here, but not in other generateXXX methods?
    def generateSignupAccountEntries() {
        List<AccountEntry> aes
        // We must generate fees after person is saved.
        aes = [generateBondAccountEntry()]
        aes.addAll(generateAccountEntriesWithinReminderPeriodOrAtLeastTheFirstOne(true))
        aes = aes.findAll { it }
        aes.forEach {
            if (person) {
                // seems like person isn't actually created until the contract is confirmed, which is kind of an issue.
                // May need to correct account entries later in that case.
                person.addToAccountEntries(it)
            }
            addToAccountEntries(it)
        }
        return aes
    }

    def generateAgencyAccountEntries() {
        List<AccountEntry> aes = []
        def that = this
        if (agent) {
            accountEntries.findAll { it.creditAccountId == ChartOfAccounts.RENT_INCOME.id }.each {income ->
                BigDecimal commission = Eval.x([daysInContract: daysInContract, amount: income.amount], Config.getValue(ConfigNameEnum.AGENT_FEE))
                BigDecimal cleaningFee = Eval.me('contract', this, Config.getValue(ConfigNameEnum.CLEANING_FEE))
                def ae = accountEntries.find {
                    it.debitAccountId == ChartOfAccounts.PROP_MANAGE_FEES.id && it.source == income
                }
                if (!ae) {
                    ae = new AccountEntry(
                            contract: this,
                            source: income
                    )
                }
                ae.person = agent
                ae.room = this.room
                ae.date = income.date
                ae.debitAccountId = ChartOfAccounts.PROP_MANAGE_FEES.id
                ae.creditAccountId = ChartOfAccounts.ACCOUNTS_PAYABLE.id
                ae.periodBegin = income.periodBegin
                ae.periodEnd = income.periodEnd
                ae.amount = commission
                ae.type = AccountEntryTypeEnum.AGENT_FEE
                ae.description = "Agent Fee Earned for ${person.name} for contract ${id} for ${checkOut.toLocalDate()} of ${income.amount}"
                aes.add(ae)
                ae = accountEntries.find {
                    it.debitAccountId == ChartOfAccounts.CLEANING.id && it.source == income
                }
                if (!ae) {
                    ae = new AccountEntry(
                            contract: this,
                            source: income
                    )
                }
                ae.person = agent
                ae.room = this.room
                ae.date = income.date
                ae.debitAccountId = ChartOfAccounts.CLEANING.id
                ae.creditAccountId = ChartOfAccounts.ACCOUNTS_PAYABLE.id
                ae.periodBegin = income.periodBegin
                ae.periodEnd = income.periodEnd
                ae.amount = cleaningFee
                ae.type = AccountEntryTypeEnum.CLEANING_FEE
                ae.description = "Cleaning fee for ${person.name} for contract ${id} for ${checkOut.toLocalDate()}"
                aes.add(ae)
            }
        }
        return aes
    }

    List<AccountEntry> generateAccountEntriesWithinReminderPeriodOrAtLeastTheFirstOne(boolean restartFromBeginning = false) {
        LocalDate until = LocalDate.now().plusDays(Config.findByName(ConfigNameEnum.REMINDER_PERIOD).integer)
        List<AccountEntry> entries = generateAccountEntriesUntil(until, restartFromBeginning)
        if (entries.isEmpty()) {
            entries.addAll(generateAccountEntriesUntil(checkIn.toLocalDate()))
        }
        return entries
    }

    List<AccountEntry> generateAccountEntriesForEntireContract() {
        return generateAccountEntriesUntil(checkOut.toLocalDate(), checkOut.toLocalDate(), true)
    }

//    void persistFees(List<AccountEntry> fees) {
//        fees.each {fee ->
//            this.person.addToAccountEntries(fee)
//            this.addToAccountEntries(fee)
//            fee.save()
//            accountEntryService.save(fee)
//            fee.generateEmail() // I don't think we want to generate the email here do we?
//        }
//    }

//    void generateAndPersistFees() {
//        persistFees(generateAccountEntriesWithinReminderPeriodOrAtLeastTheFirstOne())
//    }

//    void generateAndPersistFeesAndSendEmails() {
//        def accountEntries = generateAccountEntriesWithinReminderPeriodOrAtLeastTheFirstOne()
//        persistFees(accountEntries)
//        generateEmailForAccountEntries(accountEntries)
//    }

    String toString() {
        person?.toString() + ":" + room?.toString() + ":" + checkIn?.toLocalDate()
    }

//    List<AccountEntry> calculateFees() {
//        if (!rent) { // do we need this to save contracts that aren't signed yet?
//            return []
//        }
//        List<AccountEntry> rtn = []
//        LocalDate firstDate = nextRentEnd
//        LocalDate endDate =  nextPeriodEnd
//
//        long reminderPeriod = Config.findByName(Config.names.REMINDER_PERIOD).number.longValue()
//        LocalDate today = LocalDate.now()
//        LocalDate reminderThreshold = today.plusDays(reminderPeriod)
//        while (nextDateConfig.date <= reminderThreshold) {
//            nextDateConfig.date = nextDateConfig.date.plusDays(rentPeriod)
//            Config.withTransaction {
//                nextDateConfig.save()
//            }
//        }
//        LocalDate dateMoveOut = moveOut?.toLocalDate()
//        MathContext mc = new MathContext(8, RoundingMode.HALF_UP) ;
//        while (firstDate <= reminderThreshold && (!dateMoveOut || firstDate <= dateMoveOut)) {
//            BigDecimal amount = this.rent
//            if (dateMoveOut && dateMoveOut < endDate) {
//                endDate = dateMoveOut.plusDays(1)
//            }
//            BigDecimal days = new BigDecimal(firstDate.until(endDate).getDays())
//            BigDecimal fraction = days.divide(new BigDecimal(rentPeriod), mc)
//            amount = amount.multiply(fraction)
//            amount = amount.setScale(2, RoundingMode.HALF_DOWN)
//            if (!dateMoveOut || firstDate <= dateMoveOut) {
//                LocalDate end = endDate.minusDays(1)
//                AccountEntry fee = new AccountEntry(
//                        date: firstDate,
//                        periodBegin: firstDate,
//                        periodEnd: end,
//                        amount: amount,
//                        type: AccountEntry.types.RENT,
//                        description: "Rent due for period $firstDate to $end"
//                )
//                rtn.add(fee)
//            }
//            firstDate = endDate
//            endDate = firstDate.plusDays(rentPeriod)
//        }
//        return rtn
//    }
}
