package holder

import groovy.transform.CompileStatic

import java.time.LocalDateTime

@CompileStatic
enum SmsStatus {
    CREATED, ATTEMPTED, SENT, ERROR, EXPIRED, ABORT
}

class Sms {
    Long id
    String messageId
    SmsStatus status = SmsStatus.CREATED
    String error
    String fromAddress
    String toAddress
    String text
    int attemptsCount = 0
    int maxAttemptsCount = 1
    LocalDateTime lastAttemptDate
    long attemptInterval = 300000l
    boolean markDelete = false
    LocalDateTime dateCreated

    static constraints = {
        messageId nullable: true
        from nullable: true
        error nullable: true
        lastAttemptDate nullable: true
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        status defaultValue: SmsStatus.CREATED.toString()
        attemptsCount defaultValue: '0'
        maxAttemptsCount defaultValue: '1'
        attemptInterval defaultValue: '300000'
        markDelete defaultValue: 'false'
        dateCreated defaultValue: "current_timestamp"
        version defaultValue: '0'
    }
}
