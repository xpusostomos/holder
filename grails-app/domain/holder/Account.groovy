package holder

import grails.compiler.GrailsTypeChecked

import java.time.LocalDate
import java.time.LocalDateTime

class Account {
    Long id
    Account parent
    String name
    Account offset
    Account depreciation
    Set<AccountEntry> debitAccountEntries = []
    Set<AccountEntry> creditAccountEntries = []
    Boolean hasPersonalUse = false
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated


    static hasMany = [ debitAccountEntries: AccountEntry, creditAccountEntries: AccountEntry ]

    static mappedBy = [ debitAccountEntries: 'debitAccount', creditAccountEntries: 'creditAccount']

    static transients = ['balance', 'balances']

    static constraints = {
        parent nullable: true
        offset nullable: true
        depreciation nullable: true
        hasPersonalUse nullable: false
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id generator: 'assigned'
        debitAccountEntries sort: 'date'
        creditAccountEntries sort: 'date'
        hasPersonalUse defaultValue: 'false'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
//        debitAccountEntries column: 'debit_account'
//        creditAccountEntries column: 'credit_account'
    }

    String toString() {
        return "$id $name"
    }

    BigDecimal getBalance() {
        balance(Account.openingDate(LocalDate.now()), Account.closingDate(LocalDate.now()), null)
    }

    Map<Room,BigDecimal> getBalances() {
        [*debitAccountEntries, *creditAccountEntries].collect{ it.premises }.collectEntries {[it, balance(null, null, it)]}
    }

    Set<AccountEntry> entries(LocalDate from, LocalDate to, Room premises) {
        return AccountEntry.findByDates(this, from, to, premises)
    }

    Set<AccountEntry> trialEntries(LocalDate from, LocalDate to, Room premises) {
        return AccountEntry.findTrialByDates(this, from, to, premises)
    }

    BigDecimal balance(LocalDate from, LocalDate to, Room premises) {
//        BigDecimal dbal = AccountEntry.findAllByDebitAccountAndDateGreaterThanEqualsAndDateLessThanEquals(this, from, to).sum { it.amount } ?: BigDecimal.ZERO
//        BigDecimal cbal = AccountEntry.findAllByCreditAccountAndDateGreaterThanEqualsAndDateLessThanEquals(this, from, to).sum { it.amount } ?: BigDecimal.ZERO
        BigDecimal bal = entries(from, to, premises).collect { it.signedAmount(this) }.sum() ?: BigDecimal.ZERO
        return bal
    }

    BigDecimal trialBalance(LocalDate from, LocalDate to, Room premises, boolean exPersonal = false) {
//        BigDecimal dbal = AccountEntry.findAllByDebitAccountAndDateGreaterThanEqualsAndDateLessThanEquals(this, from, to).sum { it.amount } ?: BigDecimal.ZERO
//        BigDecimal cbal = AccountEntry.findAllByCreditAccountAndDateGreaterThanEqualsAndDateLessThanEquals(this, from, to).sum { it.amount } ?: BigDecimal.ZERO
        BigDecimal bal = trialEntries(from, to, premises).collect { it.signedAmount(this, exPersonal) }.sum() ?: BigDecimal.ZERO
        return bal
    }


    static openingDate(LocalDate date) {
        int yr = date.year
        if (date.monthValue <= 6) {
            yr--
        }
        return LocalDate.of(yr, 7, 1)
    }

    static closingDate(LocalDate date) {
        int yr = date.year
        if (date.monthValue >= 7) {
            yr++
        }
        return LocalDate.of(yr, 6, 30)
    }

    List<AccountEntry> generateOpeningEntries() {
        List<AccountEntry> allEnts = AccountEntry.findAllByDebitAccountOrCreditAccount(this, this, [sort: 'date'])
        List<AccountEntry> ents = allEnts.findAll { it.type != AccountEntryTypeEnum.OPENING}
        List<AccountEntry> opening = allEnts.findAll { it.type == AccountEntryTypeEnum.OPENING}
        List<AccountEntry> rtn = new ArrayList<>()
        Account equity = Account.get(ChartOfAccounts.BALANCING_ACCOUNT.id)
        if (ents.size() > 0) {
            LocalDate earliest = ents[0].date
            Map<Room,BigDecimal> balances = new HashMap<>()
            LocalDate currentPeriodStart = openingDate(LocalDate.now())
            while (openingDate(earliest) <= currentPeriodStart) {
                LocalDate openingDate = openingDate(earliest)
                LocalDate closingDate = closingDate(earliest)
                List<AccountEntry> periodEntries = ents.findAll { it.date >= openingDate && it.date <= closingDate }
                for (Room premises in [*periodEntries.collect { it.premises}, *new ArrayList(balances.keySet())].unique()) {
                    List<AccountEntry> periodPropEntries = periodEntries.findAll { it.premises == premises }
                    BigDecimal periodAmount = periodPropEntries.collect { it.signedAmount(this) }.sum() ?: BigDecimal.ZERO
                    BigDecimal balance = balances.get(premises)
                    if (!balance) {
                        balance = BigDecimal.ZERO
                        balances.put(premises, balance)
                    }
                    AccountEntry openingEnt = opening.find { it.premises == premises && it.date == openingDate }
                    if (balance != BigDecimal.ZERO) {
                        if (!openingEnt) {
                            openingEnt = new AccountEntry(type: AccountEntryTypeEnum.OPENING)
                            opening.add(openingEnt)
                        }
                        rtn.add(openingEnt)
                        openingEnt.date = openingDate
                        openingEnt.description = 'Opening Entry'
                        openingEnt.periodBegin = openingDate
                        openingEnt.periodEnd = closingDate
                        openingEnt.premises = premises
                        if (balance > 0) {
                            openingEnt.debitAccount = this
                            openingEnt.creditAccount = equity
                            openingEnt.amount = balance
                        } else if (balance < 0) {
                            openingEnt.debitAccount = equity
                            openingEnt.creditAccount = this
                            openingEnt.amount = balance.negate()
                        }
                    }
                    balances.put(premises, balance.add(periodAmount))
                }
                earliest = closingDate.plusDays(1)
            }
        }
        return rtn
    }

    List<AccountEntry> generateClosingEntries(Account destination, AccountEntryTypeEnum exclude) {
//        List<AccountEntry> allEnts = AccountEntry.findAllByDebitAccountOrCreditAccount(this, this, [sort: 'date'])
        List<AccountEntry> allEnts = [ *new ArrayList(debitAccountEntries), *new ArrayList(creditAccountEntries) ].sort { a, b -> a.date <=> b.date }
        List<AccountEntry> ents = allEnts.findAll { it.type != exclude}
        List<AccountEntry> existing = allEnts.findAll { it.type == exclude}
        List<AccountEntry> rtn = new ArrayList<>()
        if (ents.size() > 0) {
            LocalDate earliest = ents[0].date
            LocalDate currentPeriodStart = openingDate(LocalDate.now())
            while (openingDate(earliest) <= currentPeriodStart) {
                LocalDate openingDate = openingDate(earliest)
                LocalDate closingDate = closingDate(earliest)
                List<AccountEntry> periodEntries = ents.findAll {it.date >= openingDate && it.date <= closingDate }
                for (Room premises in [*periodEntries.collect { it.premises}].unique()) {
                    List<AccountEntry> periodPropEntries = periodEntries.findAll { it.premises == premises }
                    BigDecimal balance = periodPropEntries.collect { it.signedAmount(this) }.sum() ?: BigDecimal.ZERO
                    AccountEntry closingEnt = existing.find {it.premises == premises && it.date == closingDate }
                    if (balance != BigDecimal.ZERO) {
                        log.warn "prop: $premises.name"
                        if (!closingEnt) {
                            closingEnt = new AccountEntry(type: exclude)
                            existing.add(closingEnt)
                        }
                        rtn.add(closingEnt)
                        closingEnt.date = closingDate
                        closingEnt.description = 'Closing Entry'
                        closingEnt.periodBegin = openingDate
                        closingEnt.periodEnd = closingDate
                        closingEnt.premises = premises
                        if (balance > 0) {
                            closingEnt.debitAccount = destination
                            closingEnt.creditAccount = this
                            closingEnt.amount = balance
                        } else if (balance < 0) {
                            closingEnt.debitAccount = this
                            closingEnt.creditAccount = destination
                            closingEnt.amount = balance.negate()
                        }
                    }
                }
                earliest = closingDate.plusDays(1)
            }
        }
        return rtn
    }
}


enum ChartOfAccounts {
    // Assets
    ASSETS(10000),
    RENT_DEBTORS(11230),
    BANK(11120),
    CASH(11160),
    LAND(16000),
    BUILDINGS(17100),
    BUILDINGS_ACC_DEP(17110),
    PLANT(17120),
    PLANT_ACC_DEP(17130),
    FURN_FIT(17140),
    FURN_FIT_ACC_DEP(17150),
    VEHICLE(17160),
    VEHICLE_ACC_DEP(17170),
    // Liabilities
    LIABILITIES(20000),
    ACCOUNTS_PAYABLE(21110),
    BOND_HELD(21140),
    EQUITY(30000),
    MEMBER_FUNDS(30500),
    RETAINED_SURPLUS(31000),
    CURRENT_YEAR_SURPLUS(32000),
    BALANCING_ACCOUNT(35000),
    DIVIDENDS_PAID(36000),
    // Income
    INCOME(40000),
    RENT_INCOME(45030),
    FEES(44040),
    // Expenses
    EXPENSES(60000),
    ACCOUNTING_FEES(60010),
    ADVERTISING(60020),
    MINOR_ASSETS(60040),
    BANK_FEES(60070),
    BAD_DEBTS(60080),
    CLEANING(60100),
    COMPUTER(60220),
    WRITEOFF(60740),
    DEP_BUILDING(60250),
    DEP_VEHICLE(60260),
    DEP_PLANT(60270),
    DEP_RENTAL_PROP(60280),
    DEP_FURN_FIT(60290),
    ENTERTAINMENT(60315),
    EQUIPMENT_HIRE(60320),
    FEES_PERMITS(60330),
    INSURANCE_GENERAL(60400),
    INSURANCE_PUBLIC_LIAB(60410),
    INSURANCE_PROF_INDEM(60420),
    INSURANCE_RENT_PROP(60430),
    INTEREST_PAID(60445),
    LEGAL_FEES(60450),
    LOSS_ON_SALE(60460),
    MANAGEMENT_FEES(60470),
    VEHICLE_FUEL(60501),
    VEHICLE_REPAIRS(60502),
    VEHICLE_INSURANCE(60503),
    VEHICLE_REGO(60504),
    VEHICLE_OTHER(60506),
    POSTAGE(60510),
    STATIONARY(60520),
    PROP_MANAGE_FEES(60530),
    RATES_RENTAL(60560),
    REPAIR_EQUIP(60590),
    REPAIRS_RENTAL(60595),
    SALARIES(60611),
    SUNDRY(60670),
    PHONE(60680),
    TRAVEL(60710),
    UTILTIES(60720)

    Long id
    ChartOfAccounts(Long a) {
        this.id = a
    }
}
