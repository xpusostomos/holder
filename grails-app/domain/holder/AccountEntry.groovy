package holder

import grails.compiler.GrailsCompileStatic
import grails.compiler.GrailsTypeChecked

import java.math.RoundingMode
import java.time.LocalDate
import java.time.LocalDateTime

enum AccountEntryTypeEnum {
    BOND, RENT, ADJUSTMENT, FEE, PAYMENT, BOND_REFUND, PAY_REFUND, BOND_KEPT, PURCHASE, SALE, DEPRECIATION, OPENING, CLOSING, ROLLOVER, AGENT_FEE, CLEANING_FEE
}

enum AccountEntryMethodEnum {
    CASH, BANK, WRITEOFF, NONCASH
}

enum DepreciationMethod {
    STRAIGHT, DIMINISHING, LOW_VALUE_POOL
}

class AccountEntry {
    AccountEntryTypeEnum type
    AccountEntryMethodEnum method
    LocalDate periodBegin
    LocalDate periodEnd
    LocalDate date = LocalDate.now()
    Account debitAccount
    Account creditAccount
    BigDecimal amount
    String description
    String notes
    Person person
    Contract contract
    Room room
    Room premises
    Boolean provisional = false // revisit this entry before finalising accounts
    BigDecimal usefulLife
//    BigDecimal depreciationRate
    DepreciationMethod depreciationMethod
    Account depreciationAccount
    AccountEntry source
    Set<AccountEntry> depreciation = []
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [ person: Person, contract: Contract, room: Room, source: AccountEntry ]

    static hasMany = [ depreciation: AccountEntry ]

    static transients = [ 'creditAmount', 'personAmount', 'personBondAmount', 'debitAccountId', 'creditAccountId', 'initialAccumulatedDepreciation']

    static constraints = {
        date nullable: false
        premises nullable: false
        room nullable: true
        description nullable: true
        amount scale: 2, min: 0.0
        debitAccount nullable: false
        creditAccount nullable: false
        type nullable: true
        method nullable: true
        periodBegin nullable: true
        periodEnd nullable: true
        notes nullable: true, widget: 'textarea'
        person nullable: true
        contract nullable: true
//        , validator: {
//            val , obj ->
//                if (val && val.person?.id != obj.person?.id) return ['contract', 'contractPerson']
//        }
        provisional nullable: false
        usefulLife nullable: true, min: 0.0
//        depreciationRate nullable: true, min: 0.0
        depreciationMethod nullable: true
        depreciationAccount nullable: true
        source nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        debitAccount enumType: 'identity'
        creditAccount enumType: 'identity'
        depreciation sort: 'date'
        provisional defaultValue: 'false'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    void setRoom(Room rm) {
        room = rm
        if (rm) {
            premises = rm.parentOfLevel(RoomLevelEnum.PREMISES)
        }
    }

    BigDecimal getPersonAmount() {
        if (debitAccount.id == ChartOfAccounts.RENT_DEBTORS.id || debitAccount.id == ChartOfAccounts.ACCOUNTS_PAYABLE.id) {
            return amount
        } else if (creditAccount.id == ChartOfAccounts.RENT_DEBTORS.id || creditAccount.id == ChartOfAccounts.ACCOUNTS_PAYABLE.id) {
            return amount.negate()
        } else {
            return BigDecimal.ZERO
        }
    }

    BigDecimal getPersonBondAmount() {
        if (debitAccountId == ChartOfAccounts.BOND_HELD.id) {
            return amount
        } else if (creditAccountId == ChartOfAccounts.BOND_HELD.id) {
            return amount.negate()
        } else {
            return BigDecimal.ZERO
        }
    }

//    def beforeValidate() {
//        depreciation.each { it.save() }
//    }

    void setInitialAccumulatedDepreciation(BigDecimal amount) {
        AccountEntry initialDepreciation = depreciation.find { it.date == date }
        if (amount != 0.0 && !initialDepreciation) {
            AccountEntry dep = new AccountEntry(
                    date: date,
                    type: AccountEntryTypeEnum.DEPRECIATION,
                    amount: amount,
                    room: this.room,
                    premises: this.premises,
                    source: this,
                    debitAccount: this.depreciationAccount,
                    creditAccount: this.debitAccount.offset)
            this.addToDepreciation(dep)
        } else if (amount == 0.0 && initialDepreciation) {
            this.removeFromDepreciation(initialDepreciation)
            initialDepreciation.delete()
        } else if (initialDepreciation) {
            initialDepreciation.amount = amount
            initialDepreciation.date = date
        }
        this.validate() // why?
    }

    BigDecimal getInitialAccumulatedDepreciation() {
        depreciation.find { it.date == date}?.amount ?: 0.0
    }

    Long getDebitAccountId() {
        return debitAccount.id
    }

    void setDebitAccountId(Long e) {
        debitAccount = Account.get(e)
    }

    Long getCreditAccountId() {
        creditAccount.id
    }

    BigDecimal accumulatedDepreciation(LocalDate date) {
        depreciation.findAll {it.periodEnd < date }.sum { it.amount } ?: BigDecimal.ZERO
    }

    BigDecimal openingValue(LocalDate date) {
        amount.minus(accumulatedDepreciation(date))
    }

    BigDecimal depreciation(LocalDate from, LocalDate to) {
        depreciation.findAll {it.periodBegin <= from && to <= it.periodEnd }.sum { it.amount } ?: BigDecimal.ZERO
    }

    BigDecimal getDepreciationRate(LocalDate from, LocalDate to) {
        BigDecimal rate = 1 / usefulLife
        (depreciationMethod == DepreciationMethod.LOW_VALUE_POOL && from <= date && date <= to)
                ? rate / 2 : rate
    }

    BigDecimal nextDepreciationAmount(LocalDate from, LocalDate to) {
        BigDecimal depreciableAmount = depreciationMethod == DepreciationMethod.STRAIGHT ? amount : openingValue(from)
        BigDecimal depRateToUse = getDepreciationRate(from, to)
        BigDecimal depFullYear = depreciableAmount.multiply(depRateToUse)
        BigDecimal opening = openingValue(from)
        LocalDate endYear = from.plusYears(1)
        long daysInYear = java.time.temporal.ChronoUnit.DAYS.between(from, endYear)
        long daysOfDepreciation = depreciationMethod == DepreciationMethod.LOW_VALUE_POOL ? daysInYear : java.time.temporal.ChronoUnit.DAYS.between(from, to)
        BigDecimal proportionOfYear = new BigDecimal(daysOfDepreciation).divide(daysInYear, 2, RoundingMode.HALF_DOWN )
        BigDecimal dep = depFullYear.multiply(proportionOfYear)
        dep <= opening ? dep : opening
    }

    AccountEntry generateDepreciation(LocalDate start, LocalDate end) {
        AccountEntry dep = depreciation.find { it.periodBegin == start }
        if (dep) {
            dep.amount = nextDepreciationAmount(start, end)
        } else {
            dep = new AccountEntry(
                    type: AccountEntryTypeEnum.DEPRECIATION,
                    amount: nextDepreciationAmount(start, end),
                    source: this,
            )
        }
        dep.date = end
        dep.periodBegin = start
        dep.periodEnd = end
        dep.room = this.room
        dep.premises = this.premises
        dep.debitAccount = this.depreciationAccount
        dep.creditAccount = this.debitAccount.offset
        if (!dep.creditAccount) {
            throw new Exception("generateDepreciation: invalid debit account on purchase: $this.id")
        }
        return dep
    }

    void setCreditAccountId(Long e) {
        creditAccount = Account.get(e)
    }



//    static types = [
//            RENT: "RENT",
//            PAYMENT: "PAYMENT"
//    ]
//
//    static methods = [
//            CASH: "CASH",
//            BANK: "BANK",
//            WRITEOFF: "WRITEOFF"
//    ]

    def generateEmail() {

    }

    def getDays() {
        periodBegin.until(periodEnd).getDays() + 1
    }

    def setCreditAmount(BigDecimal v) {
        amount = v ? v.negate() : null
    }

    BigDecimal getCreditAmount() {
        return amount ? amount.negate() : null
    }

    BigDecimal signedAmount(Account acc, boolean exPersonal = false) {
        BigDecimal rtn
        if (debitAccount.id == acc.id) {
            rtn = amount
        } else if (creditAccount.id == acc.id) {
            rtn = amount.negate()
        } else {
            rtn = BigDecimal.ZERO
        }
        if (exPersonal && acc.hasPersonalUse) {
            rtn = rtn * (1 - premises.personalUse)
        }
        return rtn
    }

    static def findByDates(LocalDate from, LocalDate to, Room premises) {
//        Set<AccountEntry> aes = AccountEntry.findAllByDateGreaterThanEqualsAndDateLessThanEquals(c.fromDate, c.toDate)
        return AccountEntry.createCriteria().list() {
//            or {
//                and {
//                    lt('id', 40000L)
//                    le('date', to)
//                }
//
//                and {
//                    ge('id', 40000L)
                    ge('date', from)
                    le('date', to)
//                }
//            }
            if (premises) {
                eq 'premises', premises
            }
            order 'date', 'asc'
        }
    }

    static Set<AccountEntry> findByDates(Account account, LocalDate from, LocalDate to, Room premises) {
        return AccountEntry.createCriteria().list() {
            // This nested matching of id is due to balance() some weird reason account.id is sometimes null
            // and gives weird error
            or {
                'debitAccount' {
                    eq 'id', account.id
                }
                'creditAccount' {
                    eq 'id', account.id
                }
            }
            if (from) {
                ge('date', from)
            }
            if (to) {
                le('date', to)
            }
            if (premises) {
                eq 'premises', premises
            }
            order 'date', 'asc'
        }
    }

    static Set<AccountEntry> findTrialByDates(Account account, LocalDate from, LocalDate to, Room premises) {
        return AccountEntry.createCriteria().list(date: 'asc', id: 'asc') {
            or {
                'debitAccount' {
                    eq 'id', account.id
                }
                'creditAccount' {
                    eq 'id', account.id
                }
            }
            if (from) {
                ge('date', from)
            }
            if (to) {
                le('date', to)
            }
            or {
                and {
                    ne('type', AccountEntryTypeEnum.CLOSING)
                    ne('type', AccountEntryTypeEnum.ROLLOVER)
                }
                isNull 'type'
            }
            if (premises) {
                eq 'premises', premises
            }
        }
    }

//    @Override
//    int compareTo(AccountEntry o) {
//        return Comparator.comparing(AccountEntry::date).thenComparing(AccountEntry::id).compare(this, o)
//    }
}
