package holder

import java.time.LocalDateTime

class ToDo {
    String summary
    String text
    Person person
    Room room
    LocalDateTime deadline
    Boolean complete = false
    Album album
    BigDecimal priority = 1
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [room: Room]

    static constraints = {
        id defaultValue: "nextval('hibernate_sequence')"
        summary maxSize: 256
        text maxSize: 4000, widget: 'textarea'
        person nullable: true
        room nullable: true
        complete nullable: false
        album nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        summary nullable: false
        text nullable: true
        deadline nullable: true
        complete defaultValue: 'false'
        priority defaultValue: '1.0'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }
}
