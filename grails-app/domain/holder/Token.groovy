package holder

import org.apache.commons.lang3.RandomStringUtils
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.web.util.matcher.AntPathRequestMatcher
import org.springframework.security.web.util.matcher.RequestMatcher

import java.time.LocalDateTime

class Token {
    User user
    String token = RandomStringUtils.randomAlphanumeric(16)
    LocalDateTime expiry = LocalDateTime.now().plusDays(1)
    boolean oneTime = true

    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static constraints = {
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        token index: 'token_token_idx', defaultValue: "MD5(random()::text)"
        expiry index: 'token_expiry_idx', defaultValue: "'tomorrow'::TIMESTAMP"
        oneTime defaultValue: 'true'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    TokenAuthenticationToken checkAuthentication(String username, String key) {
        if (oneTime) {
            delete()
        }
        return LocalDateTime.now() < expiry &&
                (username == null || username == user.username) &&
                key == token ? new TokenAuthenticationToken(this) : null
    }
}
