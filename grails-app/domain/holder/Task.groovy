package holder

import au.com.tech.RingBuffer
import grails.compiler.GrailsTypeChecked

import java.time.Duration
import java.time.LocalDateTime

//enum ScheduleStyle {
//    ASSIGN_ALL, ROTATION
//}

@GrailsTypeChecked
class Task {
    Room room
    String name
    String description
//    ScheduleStyle style
    LocalDateTime due
    Duration scheduleInterval = Duration.ofDays(7)
    Duration remindEarlyInterval = Duration.ofDays(1)
    Duration remindLateInterval = Duration.ofDays(1)
    int calculateAhead = 4
    Set<TaskPerson> taskPeople = []
    Set<Schedule> schedules = []
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static hasMany = [schedules: Schedule, taskPeople: TaskPerson, subtasks: Subtask]

    static transients = ['people', 'sortedTasks', 'complete', 'incomplete', 'active', 'inactive', 'astActive', 'nextDateTime' ]

    static constraints = {
        room nullable: true
        name nullable: false
//        style nullable: false
        description nullable: true, widget: 'textarea'
        due nullable: false
//        calculateAhead nullable: false
        scheduleInterval nullable: false
        remindEarlyInterval nullable: false
        remindLateInterval nullable: false
        subtasks attributes: [:]
        taskPeople attributes: [:]
        schedules attributes: [:]
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        dateCreated defaultValue: "current_timestamp"
        subtasks sort: 'orderBy'
        taskPeople sort: 'orderBy'
        schedules sort: 'due'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }


    List<Person> getPeople() {
        taskPeople.sort{ a, b -> a.orderBy <=> b.orderBy }.collect { it.person}
    }

    List<Schedule> getSortedTasks() {
        return schedules.sort{ a, b -> a.due <=> b.due }
    }

    List<Schedule> getComplete() {
        sortedTasks.findAll{!it.complete}
    }

    List<Schedule> getIncomplete() {
        sortedTasks.findAll{ it.complete }
    }

    List<Schedule> getActive() {
        LocalDateTime now = LocalDateTime.now()
        sortedTasks.findAll {it.due <= now }
    }

    List<Schedule> getInactive() {
        LocalDateTime now = LocalDateTime.now()
        sortedTasks.findAll {it.due > now }
    }

    Schedule getLastActive() {
        active.reverse().find()
    }

    LocalDateTime getNextDateTime() {
        Schedule la = lastActive
        if (la) {
            return la.due + scheduleInterval
        } else {
            return due
        }
    }

    List<Schedule> generateSchedule() {
        Schedule la = lastActive
        RingBuffer<Person> ring = new RingBuffer<>(people)
        Iterator<Person> iring = ring.iterator(la?.person)
        List<Schedule> tasks = inactive
        int sz = calculateAhead - tasks.size()
        sz.times {
            tasks.add(new Schedule(task: this))
        }
        LocalDateTime nextD = nextDateTime
        if (iring.hasNext()) {
            for (Schedule t in tasks) {
                t.person = iring.next()
                t.due = nextD
                nextD = nextD + scheduleInterval
            }
        } else {
            tasks.clear()
        }
        return tasks
    }

    String toString() {
        return name
    }
}
