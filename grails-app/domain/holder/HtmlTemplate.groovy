package holder

import groovy.text.GStringTemplateEngine

import java.time.LocalDateTime

class HtmlTemplate {
    def grailsApplication
//    def groovyPagesTemplateEngine
    static engine = new GStringTemplateEngine()

    HtmlTemplate parent
    String name
    Room room
    String body
    Album attachments
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [parent: HtmlTemplate, room: Room]

    static constraints = {
        parent nullable: true
        name nullable: false
        room nullable: true
        body widget: 'textarea', maxSize: 32672
        attachments nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }
    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        autowire true
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        attachments?.beforeValidate()
        if (attachments?.photos?.size() == 0) {
            attachments = null
        }
    }

    static HtmlTemplate findTemplate(String name, Room room) {
        HtmlTemplate t = HtmlTemplate.findByNameAndRoom(name, room)
        if (room && !t) {
            t = HtmlTemplate.findTemplate(name, room.parent)
        }
        return t
    }

    String make(Map binding) {
        StringWriter sw = new StringWriter()
        engine.createTemplate(body).make(binding).toString()
//        groovyPagesTemplateEngine.createTemplate(body, name).make(binding).writeTo(sw)
//        return sw.toString()
    }
}

