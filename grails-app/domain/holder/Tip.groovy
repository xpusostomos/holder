package holder

import java.time.LocalDateTime

enum TipTypeEnum {
    TIP,  // Helpful Tips
    CLEAN, // Cleaning list when checking out
    ADMIN  // Things for me to remember
}

enum TipCategory {
    CLEANING, SECURITY, GARBAGE, LIGHTING, SHOPPING, APPLIANCES, DELIVERIES, DAMAGE
}

class Tip {
    TipTypeEnum type
    TipCategory category
    String code
    String brief
    String text
    Person person
    Album album
    Room room
    BigDecimal weighting = 1
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [ room: Room]

    static constraints = {
        id defaultValue: "nextval('hibernate_sequence')"
        type nullable: false
        category nullable: false
        code nullable: false
        brief nullable: false
        text maxSize: 4000, widget: 'textarea'
        person nullable: true
        album nullable: true
        room nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        weighting defaultValue: '1.0'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }

    static List<Tip> getApplicableTips(TipTypeEnum type, Person person) {
        List<Tip> tips = Tip.findAllByType(type)
        if (person) {
            Collection<Room> occupiedRooms = person.contracts.findAll{it.active}*.room
            tips = tips.findAll { it.room.hasAny(occupiedRooms) }
        }
        return tips
    }

    static Map<TipCategory,List<Tip>> getApplicableTipsByCategory(TipTypeEnum type, Person person) {
        List<Tip> tips = getApplicableTips(type, person)
        return tips.groupBy{it.category}
    }

    static Tip getRandomTip(TipTypeEnum type, Person person) {
        List<Tip> applicableTips = getApplicableTips(type, person)
        return applicableTips.get(new Random().nextInt(applicableTips.size()));
    }
}
