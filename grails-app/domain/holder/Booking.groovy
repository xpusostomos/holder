package holder

import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period


class Booking {
    String email
    String firstName
    String lastName
    SexEnum sex
    LocalDate checkIn
    LocalDate checkOut
    LocalDateTime dateInspection
    LocalDate dateResponded
    LocalDate dateDecided
    Boolean positiveDecision = false
    String confirmationCode
    String phone
    String applyNotes
    String notes
    String employment
    Album album = new Album()
//    Contract contract
    Room room
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [room: Room]

    static transients = [ 'name', 'emailDisplay', 'timeUntilInspection' ]

    static constraints = {
        email nullable: false, blank: false, size: 6..320, email: true
        firstName nullable: false
        lastName nullable: false
        sex nullable: false
        checkIn nullable: false
        checkOut nullable: false
        employment nullable: false
        dateInspection nullable: true
        dateResponded nullable: true
        dateDecided nullable: true
        positiveDecision nullable: true
        applyNotes nullable: true, widget: 'textarea'
        notes nullable: true, widget: 'textarea'
        confirmationCode nullable: true
        phone nullable: true
//        person nullable: true
//        contract nullable: true
        room nullable: true
        album nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        positiveDecision defaultValue: 'false'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }

    String getName() {
        return firstName + ' ' + lastName
    }

    String getEmailDisplay() {
        return email ? "$name <$email>" : null
    }

    Duration getTimeUntilInspection() {
        return dateInspection ? Duration.between(LocalDateTime.now(), dateInspection) : null
    }
}
