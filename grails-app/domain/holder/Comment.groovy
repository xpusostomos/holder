package holder

import java.time.LocalDateTime

class Comment {
//    Query query
    Comment parent
    User from
    User to
    String text
    Album album
    Boolean closed = false
    Set<Comment> comments
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static belongsTo = [parent: Comment, from: User, to: User]

    static constraints = {
        parent nullable: true
        from nullable: false
        to nullable: true
        text nullable: false, widget: 'textarea'
        closed nullable: false
        album nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        closed defaultValue: 'false'
        comments cascade: 'all-delete-orphan'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }
    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }
}
