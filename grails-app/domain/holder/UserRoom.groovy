package holder
import grails.gorm.DetachedCriteria
import groovy.transform.ToString
import org.codehaus.groovy.util.HashCodeHelper
import grails.compiler.GrailsCompileStatic
import java.time.LocalDateTime

@GrailsCompileStatic
@ToString(cache=true, includeNames=true, includePackage=false)
class UserRoom implements Serializable {
    private static final long serialVersionUID = 1
    User user
    Room room
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static constraints = {
        user nullable: false
        room nullable: false, validator: { Room r, UserRoom ur ->
            if (ur.user?.id) {
                if (exists(ur.user.id, r.id)) {
                    return ['UserRoom.exists']
                }
            }
        }
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }
    static mapping = {
        table 'usr_room'
        version false
        id composite: ['user', 'room']
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    @Override
    boolean equals(other) {
        if (other instanceof UserRoom) {
            other.userId == user?.id && other.roomId == room?.id
        }
        return false
    }

    @Override
    int hashCode() {
        int hashCode = HashCodeHelper.initHash()
        if (user) {
            hashCode = HashCodeHelper.updateHash(hashCode, user.id)
        }
        if (room) {
            hashCode = HashCodeHelper.updateHash(hashCode, room.id)
        }
        hashCode
    }

    static UserRoom get(long userId, long roomId) {
        criteriaFor(userId, roomId).get()
    }

    static boolean exists(long userId, long roomId) {
        criteriaFor(userId, roomId).count()
    }

    private static DetachedCriteria<UserRoom> criteriaFor(long userId, long roomId) {
        where {
            user == User.load(userId) &&
                    room == Room.load(roomId)
        }
    }

    static UserRoom create(User user, Room room, boolean flush = false) {
        def instance = new UserRoom(user: user, room: room)
        instance.save(flush: flush)
        instance
    }

    static boolean remove(User u, Room r) {
        if (u != null && r != null) {
            where { user == u && room == r }.deleteAll()
        }
        return false
    }

    static int removeAll(User u) {
        u == null ? 0 : where { user == u }.deleteAll() as int
    }

    static int removeAll(Room r) {
        r == null ? 0 : where { room == r }.deleteAll() as int
    }
}
