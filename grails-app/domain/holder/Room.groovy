package holder

import java.time.LocalDate
import java.time.LocalDateTime

enum RoomLevelEnum {
    PREMISES(0),
    STRUCTURE(1),
    SECTION(2),
    ROOM(3),
    final int id
    private RoomLevelEnum(int id) { this.id = id }
}

enum RoomTypeEnum {
    COLLECTION, ROOM, CARSPOT
}


class Room {
//    Property premises
//    Section section
    RoomLevelEnum level
    RoomTypeEnum roomType
    String name
    Room parent
    String description
    String notes
    Boolean available = false
    BigDecimal cleaningFee = 0.0
    BigDecimal personalUse = 0.0
    Album album
    Set<Contract> contracts
    Set<Tip> tips
    Set<Room> rooms
    Set<ToDo> toDos
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

//    static belongsTo = [ property: Property, section: Section ]
    static belongsTo = [ parent: Room ]

    static hasMany = [ rooms: Room, contracts: Contract, tips: Tip, toDos: ToDo ]

    static transients = [ 'lessee', 'lessees', 'dateAvailable', 'dateContractEnd', 'moveInDate', 'moveOutDate', 'validContracts', 'availableDesc', 'allRooms']

    static constraints = {
        level nullable: false
//        premises nullable: false
//        section nullable: false
        roomType nullable: false
        name nullable: false
        parent nullable: true
        description nullable: false
        notes nullable: true, widget: 'textarea'
        available nullable: false
//        validator: {val, obj ->
//            if (val && obj.premises) {
//                 sections can be null if we only just created Property. Why?
//                if (obj.premises.sections && !obj.premises.sections.find { it.id == val.id }) {
//                    return ['holder.Room.section.invalidProperty']
//                }
//            }
//            null
//        }
        cleaningFee nullable: false
        personalUse nullable: false, scale: 5
        album nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        level  enumType: 'identity'
        available defaultValue: 'false'
        contracts cascade: 'all-delete-orphan'
        rooms cascade: 'all-delete-orphan'
        tips cascade: 'all-delete-orphan'
        cleaningFee defaultValue: '0'
        personalUse defaultValue: '0'
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        if (album?.empty) {
            album = null
        }
    }

    List<Room> getAllRooms() {
        List<Room> rtn = new ArrayList<>()
        rtn.add(this)
        rooms.sort{ a, b -> a.name <=> b.name}.forEach { rtn.addAll(it.allRooms)}
        return rtn
    }

    Room parentOfLevel(RoomLevelEnum lvl) {
        if (level == lvl) {
            return this
        }
        return parent.parentOfLevel(lvl)
    }

    Contract getContractByDateTime(LocalDateTime date) {
        Contract rtn = null
        def set = validContracts
        if (set.size() > 0) {
            rtn = set.find {
                (!it.moveOut || date <= it.moveOut) && it.checkIn <= date
            }
//            if (!rtn) {
//                rtn = set.first()
//            }
        }
        return rtn
    }

    Person getLessee() {
        return getLesseeByDateTime(LocalDateTime.now())
    }

    Collection<Person> getLessees() {
        return allRooms.findAll {it.lessee}.collect {it.lessee}
//        List<Person> rtn = new ArrayList<>()
//        if (lessee) {
//            rtn += lessee
//        }
//        rtn += rooms.collectMany {it.lessees }
//        return rtn
    }

    Person getLesseeByDateTime(LocalDateTime date) {
        return getContractByDateTime(date)?.person
    }

    List<String> emailAddresses() {
        room.lessees.findAll { it.emailEnabled }.collect { "$it.name <$it.email>" }
    }

    List<String> billingEmailAddresses() {
        room.lessees.findAll { it.emailEnabled && it.billingEmail }.collect { "$it.billingName <$it.billingEmail>" }
    }

    List<String> allEmailAddresses() {
        emailAddresses() + billingEmailAddresses()
    }

    LocalDateTime getMoveInDate() {
        Contract c = validContracts.sort { a, b -> a.moveIn <=> b.moveIn }.find { it.moveIn > LocalDateTime.now() }
        if (c) {
            return c.moveIn
        }
        return null
    }

    LocalDateTime getMoveOutDate() {
        def set = validContracts
        if (set.isEmpty()) {
            return null
        } else {
            // There could be a later contract about to start, so returning null isn't right
            return set.first().moveOut
        }
    }

    LocalDateTime getDateAvailable() {
        if (!available) {
            return null
        }
        def set = validContracts
        if (set.isEmpty()) {
            return LocalDateTime.MIN
        } else {
            // There could be a later contract about to start, so returning null isn't right
            return set.collect {it.moveOut ?: LocalDateTime.MAX }.max()
        }
    }

    String getAvailableDesc() {
        LocalDateTime a = dateAvailable
        if (!available) {
            return 'NOT AVAILABLE'
        } else if (a == LocalDateTime.MIN) {
            return 'NOW'
        } else if (a == LocalDateTime.MAX) {
            return 'NOTICE NOT GIVEN'
        } else {
            return DateTimeValueConverter.format(a)
        }
    }

    Set<Contract> getValidContracts() {
        contracts.findAll {it.active }.sort { a, b -> a.checkoutOrMoveOut <=> b.checkoutOrMoveOut }
    }

    static allAvailableOnKnownDate() {
        def rooms = findAllByRoomType(RoomTypeEnum.ROOM)
        rooms = rooms.findAll {
            it.dateAvailable && it.dateAvailable != LocalDateTime.MAX
        }
        return rooms
    }

    static allNotAvailableOnKnownDate() {
        def rooms = findAllByRoomType(RoomTypeEnum.ROOM)
        rooms = rooms.findAll {
            !it.dateAvailable || it.dateAvailable == LocalDateTime.MAX
        }
        return rooms
    }

    static allByDate() {
        [*allAvailableOnKnownDate(), *allNotAvailableOnKnownDate()]
    }


    LocalDateTime getDateContractEnd() {
        def set = validContracts
        if (set.isEmpty()) {
            return null
        } else {
            return set.last().checkoutOrMoveOut
        }
    }

    String getFullName() {
        if (parent) {
            return parent.fullName + '.' + name
        } else {
            return name
        }
    }

    boolean hasParent(Room r) {
        return r == this || parent?.hasParent(r)
    }

    Link getHome() {
        new Link(name: 'HOME', controller: 'root', action: 'showRoom', id: id)
    }

    boolean has(Room r) {
        return this.id == r.id || (r.parent && has(r.parent))
    }

    boolean hasAny(Collection<Room> s) {
        for (Room r in s) {
            if (has(r)) {
                return true
            }
        }
        return false
    }

    String toString() {
        return "$name: $description"
    }
}
