package holder

import grails.core.GrailsApplication
import groovy.text.GStringTemplateEngine
import grails.plugin.asyncmail.AsynchronousMailMessage
import grails.plugin.asyncmail.AsynchronousMailService

import java.time.LocalDateTime

class EmailTemplate {
    AsynchronousMailService asynchronousMailService
    MailIndexService mailIndexService
    GrailsApplication grailsApplication
    SmsService smsService

    String name
    Room room
    String toAddress
    String ccAddress
    String bccAddress
    String tags
    String subject
    String body
    String phone
    String smsText
    Album attachments
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static engine = new GStringTemplateEngine()

    static belongsTo = [room: Room]

    static transients = [ 'tagList', 'mailIndexService', 'smsService', 'grailsApplication', 'asynchronousMailService' ]

    static constraints = {
        name nullable: false
        room nullable: true
        toAddress nullable: true
        ccAddress nullable: true
        bccAddress nullable: true
        tags nullable: true, maxSize: 512
        subject maxSize: 78
        body widget: 'textarea', maxSize: 32672
        phone nullable: true, maxSize: 512
        smsText nullable: true, widget: 'textarea', maxSize: 925
        attachments nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        autowire true
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    def beforeValidate() {
        attachments?.beforeValidate()
        if (attachments?.photos?.size() == 0) {
            attachments = null
        }
    }

    Set<String> getTagList() {
//        def foo = Arrays.asList(*tags.split('\\|').findAll{it})
//        def bar = new HashSet(Arrays.asList(tags.split('\\|').findAll{it}))
//        def baz = new  HashSet(foo)
        return new HashSet(Arrays.asList(*(tags?:'').split('\\|').findAll{it}))
    }

    void setTagList(Collection<String> v) {
        this.tags = v.join('|')
        if (tags) {
            tags = '|' | tags | '|'
        }
    }


    List<String> bindAddresses(Map binding, String address) {
        Arrays.asList(engine.createTemplate(address ?: '').make(binding).toString().split(',')).collect {it.trim() }.findAll { it }
    }

    String bindSubject(Map binding) {
        engine.createTemplate(subject).make(binding).toString()
    }

    String bindBody(Map binding) {
        engine.createTemplate(body).make(binding).toString()
    }

    String bindSmsText(Map binding) {
        engine.createTemplate(smsText).make(binding).toString()
    }

    String bindPhone(Map binding) {
        engine.createTemplate(phone).make(binding).toString()
    }

    AsynchronousMailMessage sendMail(Map bindings, boolean enabled = true, List<Map> indexes = [], boolean smsEnabled = false) {
        def allBindings = [*: bindings, config: grailsApplication.config, principal: User.principal]
        List toAddresses = bindAddresses(allBindings, toAddress)
        List ccAddresses = bindAddresses(allBindings, ccAddress)
        List bccAddresses = bindAddresses(allBindings, bccAddress)
        int maxAttempts = Config.findByName(ConfigNameEnum.EMAIL_ENABLED).bool ? 3 : 0
        String theSubject = bindSubject(allBindings)
        AsynchronousMailMessage mm
        if (enabled) {
            mm = asynchronousMailService.sendMail {
                to toAddresses
                if (ccAddresses.size() > 0) {
                    cc ccAddresses
                }
                if (bccAddresses.size() > 0) {
                    bcc bccAddresses
                }
                subject theSubject
                text bindBody(allBindings)
                maxAttemptsCount maxAttempts
            }
            mailIndexService.create(mm, this, indexes)
        } else {
            log.warn("email disabled: ${toAddresses}: ${subject}")
        }
        if (smsEnabled && Config.findByName(ConfigNameEnum.SMS_ENABLED).bool) {
            def smsResult = smsService.send([to: bindPhone(allBindings), message: bindSmsText(allBindings)])
            log.debug "$smsResult"
        }
        mm
    }

//    static EmailTemplate findTemplate(String name, Room room) {
//        return findTemplate(name, null, room)
//    }

    static Map<EmailTemplate,Set<String>> findTemplates(String name, Collection<String> tagList) {
        return EmailTemplate.findAllByName(name).collectEntries {
            [(it): it.tagList.intersect(tagList)]
        }
    }

    static EmailTemplate findTemplate(String name, Collection<String> tagList = []) {
        return findTemplates(name, tagList).max {
            it.value.size()
        }.key
    }


    static EmailTemplate findTemplate(String name, Collection<String> tagList, Room room) {
//        Map<EmailTemplate,Set<String>> summary = findTemplates(name, tagList).
        //TODO need a better way of reporting template not found
          return findAllByName(name).findAll {tagList.containsAll(it.tagList) }.
                findAll { !it.room || it.room.has(room)}.
                sort { a, b ->
                        b.tagList.size() <=> a.tagList.size() ?: a.room && b.room ? b.room.level.id <=> a.room.level.id : a.room ? -1 : b.room ? 1 : 0
                }.find()
//        int maximumTags = summary.max { it.value.size() }.value.size()
//        for (int i = maximumTags; i >= 0; i--) {
//            for (Map.Entry<EmailTemplate,Set<String>> tmplt in summary.findAll { it.value.size() == i }.
//                    sort {a, b -> a.room && b.room ? b.level.id <=> a.level.id : a.room ? -1 : b.room ? 1 : 0 }) {
//                if (!tmplt.key.room || room.has(tmplt.key.room)) {
//                    return tmplt
//                }
//            }
//        }
//        return null
    }
}

//        static EmailTemplate findTemplate(String name, List<String> tagList, Room room) {
//        EmailTemplate t
//        List<String> ltagList = tagList ? tagList.collect {"|$it|"  } : null
////        String ltag = tag ? '%|' | tag | '|%' : null
//            // Try tag and room
//        Room aRoom = room
//        if (ltagList) {
//            for (String ltag in ltagList) {
//                t = EmailTemplate.findByNameAndTagsLikeAndRoom(name, ltag, aRoom)
//                if (t) {
//                    break;
//                }
//            }
//            while (aRoom && !t) {
//                // try tag and parent room
//                aRoom = aRoom.parent
//                // final time through the loop, room is null
//                for (String ltag in ltagList) {
//                    t = EmailTemplate.findByNameAndTagsLikeAndRoom(name, ltag, aRoom)
//                    if (t) {
//                        break;
//                    }
//                }
//            }
//        }
//        aRoom = room
//        if (!t) {
//            t = EmailTemplate.findByNameAndTagsAndRoom(name, null, aRoom)
//            while (aRoom && !t) {
//                // try just the room
//                aRoom = aRoom.parent
//                // final time through the loop, room is null
//                t = EmailTemplate.findByNameAndTagsAndRoom(name, null, aRoom)
//            }
//        }
//        return t
//    }

