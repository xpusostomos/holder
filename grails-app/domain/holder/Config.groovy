package holder

import java.time.LocalDate
import java.time.LocalDateTime

enum ConfigNameEnum {
    STD_PERIOD_END_DATE,
    RENT_PERIOD,
    REMINDER_PERIOD,
    EMAIL_ENABLED,
    EMAIL_REMINDER_ENABLED,
    SMS_ENABLED,
    MIN_REMIND_AMT,
    CLEANING_FEE,
    AGENT_FEE,
    SMS_FROM,
    GEN_INVOICE_ENABLED,
    GEN_SCHEDULE_ENABLED,
    GEN_REMINDER_ENABLED
}

class Config {
    String name
    String string
    LocalDate date
    BigDecimal number
    Boolean bool
    LocalDateTime dateCreated
    User userCreated
    LocalDateTime lastUpdated
    User userUpdated

    static transients = [ 'value' ]

    static constraints = {
        name()
        string nullable: true
        date nullable: true
        number nullable: true
        bool nullable: true
        dateCreated display: true
        userCreated display: false, editable: false // Think editable does nothing, and display: controls edit
        lastUpdated display: true
        userUpdated display: false, editable: false // Don't really want it nullable, but has to be
    }

    static mapping = {
        id defaultValue: "nextval('hibernate_sequence')"
        dateCreated defaultValue: "current_timestamp"
        userCreated defaultValue: '1'
        lastUpdated defaultValue: "current_timestamp"
        userUpdated defaultValue: '1'
        version defaultValue: '0'
    }

    String getValue() {
        string ?: number ? number.toString() : date ? date.toString() : bool ? bool.toString() : null
    }

    static String getValue(ConfigNameEnum name) {
        return Config.findByName(name).value
    }

    Long getLong() {
        number.toLong()
    }

    static long getLong(ConfigNameEnum name) {
        return Config.findByName(name).long
    }

    Integer getInteger() {
        number.toInteger()
    }

    static int getInteger(ConfigNameEnum name) {
        return Config.findByName(name).integer
    }

    BigDecimal getDecimal() {
        number
    }

    static BigDecimal getDecimal(ConfigNameEnum name) {
        return Config.findByName(name).decimal
    }

    Boolean getBool() {
        return bool
    }

    static Boolean getBool(ConfigNameEnum name) {
        return Config.findByName(name).bool
    }
}
